﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="Regalandia.test" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="css/styles.css">
    <style>

        body{

            background: #333333 !important;

        }

        .modal-producto-container{

            display: none;

        }

        .color-row-modal{

            display: flex;
             flex-direction: row;

             align-items: center;

            margin: 1em;
        }

        .color-row-modal span{ 

            margin-right: 1rem;

        }

        .color-row-modal input{

            display:none;

        }

        .radio-square-modal{

            width: 1.5rem;
            height: 1.5rem;

            border-radius: 50%;

            margin-left: 1rem;
            margin-right: 1rem;

            background: red; /* A cambiar en javascript */

        }
        
        .radio-square-modal:hover{

            transform: scale(1.2);
            cursor: pointer;

        }

      

    </style>

     <script src="js/handlebars.min-v4.7.6.js"></script>

    <script type="text/x-handlebars" id="select-frase-template">
        <div class="productFrase__selectContainer">
                        <select name="frases" class="frases">
                            {{#each frases}}
                            <option value="{{frase}}">{{frase}}</option>
                            {{/each}}
                        </select>
                    </div></script>

    <!-- template para el input number de cantidad -->
    <script type="text/x-handlebars" id="cantidad-template">

         <div id="{{id}}" class="fixedInputNumber fixedInputNumber--md balloonsAmount" >
            <p>{{label}}</p>
            <input type="number" readonly >
            <div class="colorCard__buttons">
                <button></button>
                <button></button>
            </div>
         </div>

    </script>

    <!-- template para el radio de genero -->
    <script type="text/x-handlebars-template" id="radio-genero-template">

        <div class="radio-genero-modal" id="rg-{{id}}">

                    <span>Género</span>

                    <div class="radio-genero-modal__opcion">
                        <label for="nino-{{id}}">Niño</label>
                        <input id="nino-{{id}}" type="radio" name="radio-genero-{{id}}" value="nino" >
                        <div class="radio-square-modal"></div>
                    </div>

                    <div class="radio-genero-modal__opcion">
                        <label for="nina-{{id}}">Niña</label>
                        <input id="nina-{{id}}" type="radio" name="radio-genero-{{id}}" value="nina" >
                        <div class="radio-square-modal"></div>
                    </div>

                </div>

    </script>

    <!-- template para el checbox de combinar colores con envoltura -->
    <script type="text/x-handlebars-template" id="check-combinar-colores-template">

        <div class="check-combinar-colores-producto" id="container-check-combinar-{{id}}">
                    <div class="row-checkbox">
                        <input type="checkbox"  name="check-combinar-{{id}}">
                        <div class="checkbox-big-square"></div>

                    <label for="check-combinar">Combinar con colores de envoltura</label>
                    </div>
                    <p>Al elegir esta opción, nos das la oportunidad de elegir los colores
                        perfectos que combinen con tu envoltura. </p>
                </div>

    </script>

    <!-- template para cada fila del panel de color -->
    <script id="panel-color-template" type="text/x-handlebars-template">

        <div class="color-row-modal" id="color-row-{{id}}"><span>{{nombre}}</span>

            {{#each colores}}
                <div class="radio-genero-modal__opcion">
                    <input type="radio" name="color-radio-{{tempId}}" value="{{nombre}}">
                    <div class="radio-square-modal"></div>
                </div>
            {{/each}}
             
        </div>

    </script>

    <!-- template para container de panel de colores -->
    <script id="panel-color-container-template" type="text/x-handlebars-template">

        <div class="color-panel-modal-producto" id="panel-container-{{id}}">
        </div>

    </script>

    <!-- template para el container de la info basica de la ventana modal -->
    <script id="modal-info-basica-template" type="text/x-handlebars-template">

        <section>
            <article>
                <p class="modal-producto__titulo">{{nombre}}</p>
                <p class="modal-producto__descripcion">
                    {{descripcion}}
                </p>
                <p>Costo <span class="modal-producto__costo">{{precio}}</span> </p>

                <div class="modal-producto__options-container">
                    <!-- Aqui se posicionan los components -->



                </div>
            </article>
            <aside>
                <figure class="main-figure-modal-producto"><img src="img\3b86d30fad758713528e8c7007c8db41.jpg" alt="{{foto1}}"></figure>
                    <div class="row-figures-modal-producto">
                        <figure><img src="img\3b86d30fad758713528e8c7007c8db41.jpg" alt="{{foto2}}"></figure>
                            <figure><img src="img\3b86d30fad758713528e8c7007c8db41.jpg" alt="{{foto3}}"></figure>
                                <figure><img src="img\3b86d30fad758713528e8c7007c8db41.jpg" alt="{{foto4}}"></figure>
                </div>

            </aside>
                            <span class="modal-producto__close-button">&times;</span>
                            <button class="btn-md-purple fit-content btn-add-producto">Agregar</button>
        </section>

    </script>

</head>

<body>

    <div>
        <button id="button-prueba0">Ver modal 0</button>
        <button id="button-prueba1">Ver modal 1</button>
        <button id="button-prueba2">Ver modal 2</button>
    </div>

    <div class="modal-producto-container" id="modal-producto-container">

       
    </div>

    <script type="module" src="js/pages/test.js"></script>
</body>
</html>

