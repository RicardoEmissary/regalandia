﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Regalandia.client.index" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="icon" type="image/png" href="img/favico.png">
    <title>Regalandia</title>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@200;300;500;800&display=swap" rel="stylesheet">

    <script src="js/handlebars.min-v4.7.6.js"></script>

    <script id="scroll-card-container" type="text/x-handlebars-template">

        {{#each regalos}}
           <div class="card card-button card-{{id}}">

                    <div class="card-header"><div class="scroll-button"><figure><img src="app/multimedia/{{foto1}}"></figure></div></div>
                    <div class="card-main">
                        <div class="card-title">
                            <p>{{nombre}}</p>
                        </div>
                        <p>$ {{precio}}</p>
                    </div>
                </div>
        {{/each}}
    </script>

    <script id="modal-product-template" type="text/x-handlebars-template">
        {{#each articulos}}
            <div id="mp-{{id}}" class="modal modal-product">

                <div class="modal-content">
                    <span class="close">&times;</span>
                    <section>
                        <div class="mainTitle mainTitle--balloons">
                            <h3>{{nombre}}</h3>
                        </div>
                        <article class="productCard">
                            <div class="productCard__images">
                                <figure class="productCard__images__mainFigure">
                                    <img src="{{foto1}}" alt="Globos">
                                </figure>
                                <figure class="productCard__images__rowFigure">
                                    <img src="{{foto2}}" alt="Globos">
                                    <img src="{{foto3}}" alt="Globos">
                                    <img src="{{foto4}}" alt="Globos">
                                </figure>
                            </div>

                            <div class="productCard__info">
                                <p class="productCard__info__text">{{nombre}}</p>
                                <p class="productCard__info__productDesc">
                                    {{descripcion}}
                                </p>
                                <p class="productCard__info__text product__costo">Costo <span class="costo-product"></span></p>
                                <div class="colorCardGrid">
                                    <div class="colorCard ">
                                        <div class="fixedInputNumber fixedInputNumber--md balloonsAmount">
                                            <p>Cantidad</p>
                                            <input type="number" readonly>
                                            <div class="colorCard__buttons">
                                                <button></button>
                                                <button></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="productCard__info__productColor">
                                    <div class="productColor__rowColor">
                                        <p>Selecciona color</p>
                                        <div class="colorCardGrid">
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--violet"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color1">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--blue"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color2">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--orange"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color3">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--red"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color4">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="productCard__info__productFrase">
                                    <p>Selecciona frase</p>
                                    <div class="productFrase__selectContainer">
                                        <select name="frases" id="frases">
                                            <option value="0">Buen día</option>
                                            <option value="1">Feliz cumpleaños</option>
                                            <option value="2">Felices fiestas</option>
                                        </select>
                                    </div>
                                </div>
                                <button id="modal-product-btn-agregar"  class="btn-md-purple fit-content ">Agregar</button>
                            </div>
                        </article>
                    </section>
                </div>
            </div>
        {{/each}}
    </script>

</head>

<body>

    <div id="container-modals"></div>
<!-- Modal de zonas de envio -->
<div id="modalZonas" class="modal-primary">
    <!-- Modal content -->
    <div class="modal-content-blue">
        <span class="close-purple">&times;</span>
        <h1>Zonas de envíos</h1>

        <section class="carousel" aria-label="Gallery">
            <ol class="carousel__viewport" id="vport">
                <li id="carousel__slide1" tabindex="0" class="carousel__slide">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d28764.20738501578!2d-100.31131896663439!3d25.68700552363942!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2smx!4v1601673037723!5m2!1ses!2smx" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    <div class="carousel__snapper">
                        <a  class="carousel__prev">Go to last slide</a>
                        <a  class="carousel__next">Go to next slide</a>
                    </div>
                </li>
                <li id="carousel__slide2" tabindex="0" class="carousel__slide">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d28776.457459630354!2d-100.27818385970163!3d25.636227607391316!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2smx!4v1601673357751!5m2!1ses!2smx" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    <div class="carousel__snapper"></div>
                    <a  class="carousel__prev">Go to previous slide</a>
                    <a  class="carousel__next">Go to next slide</a>
                </li>
                <li id="carousel__slide3" tabindex="0" class="carousel__slide">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d57545.80197881809!2d-100.4025977583956!3d25.650979223765898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662bda0f12f71a9%3A0xe44e63fb073ffae!2sSan%20Pedro%20Garza%20Garc%C3%ADa%2C%20Nuevo%20Leon!5e0!3m2!1sen!2smx!4v1597913975245!5m2!1sen!2smx"
                            width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    <div class="carousel__snapper"></div>
                    <a  class="carousel__prev">Go to previous slide</a>
                    <a  class="carousel__next">Go to next slide</a>
                </li>

            </ol>
            <aside class="carousel__navigation">
                <ol class="carousel__navigation-list">
                    <li class="carousel__navigation-item">
                        <a  class="carousel__navigation-button">Go to slide 1</a>
                    </li>
                    <li class="carousel__navigation-item">
                        <a  class="carousel__navigation-button">Go to slide 2</a>
                    </li>
                    <li class="carousel__navigation-item">
                        <a  class="carousel__navigation-button">Go to slide 3</a>
                    </li>

                </ol>
            </aside>
        </section>
    </div>
</div>

<!-- SCROLL BANNER -->

<section class="banner" id="header">
    <div class="banner-left">
        <div class="media">
            <p>¡Únete!</p>
            <div class="icon">
                <a href="https://www.instagram.com/regalandia.mx/"><img src="img/icon/icon-instagram.svg" alt="Regalandia Instagram"></a>
            </div>
            <div class="icon">
                <a href=""><img src="img/icon/icon-facebook.svg" alt="Regalandia Facebook"></a>
            </div>
        </div>
    </div>
    <div class="logo">
        <a href="index.html"><img src="img/Rmono/6.svg" alt="Regalandia"></a>
    </div>
    <div class="banner-right">
        <button  class="banner-cart">
            <div class="icon cart">
                <img src="img/icon/shopping.svg" alt="carrito">
                <span class="cart-items">0</span>
            </div>
            <p><span>$0.00</span></p>
        </button>

        <div class="banner-right__popup">
            <div class="container-table popup__container-table">
                <table class="resume">
                    <tbody class="tbody-flex">
                    <tr class="table-product-description table-product-description__popup">

                        <td>
                            <img src="./img/3b86d30fad758713528e8c7007c8db41.jpg" alt="product img" width="80" height="80" />
                        </td>

                        <td class="table__description__title">
                            <span class="product-title">Candy Box</span>
                            <span class="text-primary">Cantidad: 01</span>

                        </td>
                        <td>
                            <span>$100.00</span>
                        </td>
                    </tr>
                    <tr class="table-product-description table-product-description__popup">

                        <td>
                            <img src="./img/3b86d30fad758713528e8c7007c8db41.jpg" alt="product img" width="80" height="80" />
                        </td>

                        <td class="table__description__title">
                            <span class="product-title">Candy Box</span>
                            <span class="text-primary">Cantidad: 01</span>

                        </td>
                        <td>
                            <span>$100.00</span>
                        </td>
                    </tr>
                    <tr class="subtotal">
                        <td>Subtotal</td>
                        <td>$100.00</td>
                    </tr>
                    <tr class="subtotal">
                        <td>Total</td>
                        <td>$100.00</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="btnCarrito btnCarrito--popup">
                <a href="carrito.html" class="btn-sm-secundary">Abrir carrito</a>
                <a href="finalizar-compra.html" class="btn-sm-secundary">Finalizar pedido</a>
            </div>
        </div>
    </div>
</section>


<div class="topnav " id="myTopnav">
    <a href="index.html" class="activeNav"></a>
    <a href="tienda.html">Tienda</a>
    <a href="entregas.html">Entregas</a>
    <a href="tracking.html">Rastreo</a>
    <a href="contacto.html">Contacto</a>
    <a href="nosotros.html">Nosotros</a>
    <a id="responsiveNavbar" href="javascript:void(0);" class="icon">&#9776;</a>
</div>

<main>
    <section class="section-index-1">
        <div class="section-index-1__img">

        </div>
        <div class="text">
            <h1>Recolectamos, envolvemos y
                entregamos tus regalos</h1>
            <h1>Recolectamos, envolvemos y
                entregamos tus regalos--texto2</h1>
            <h1>Recolectamos, envolvemos y
                entregamos tus regalos--texto3</h1>
            <a href="nosotros.html" class="btn-xl-primary fit-content">conócenos</a>
            <a href="nosotros.html" class="btn-md-primary fit-content">conócenos</a>
        </div>

        <div class="carButtons">
            <button></button>
            <button></button>
            <button></button>
        </div>
    </section>

    <!-- NAV entregas/globos/envolturas -->

    <section class="section-index-2">

        <nav class="card-title card-title--violet card-title--animated">
            <a href="entregas.html">
                <div class="card-title__header">entregas</div>
                <div class="card-title__main">
                    <div class="card-title__main__image">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 115.6" style="enable-background:new 0 0 100 115.6;" xml:space="preserve">
                            <style type="text/css">
                                .st0{fill:none;stroke:#FEFCFA;stroke-width:2.4403;stroke-linecap:round;stroke-miterlimit:10;}
                                .st1{fill:#C9195C;}
                                .st2{fill:#FEFCFA;}
                            </style>
                            <g id="dashes">
                                <line class="st0" x1="23.6" y1="68.8" x2="39" y2="68.8"/>
                                <line class="st0" x1="23.6" y1="58.2" x2="39" y2="58.2"/>
                                <line class="st0" x1="23.6" y1="47.6" x2="39" y2="47.6"/>
                            </g>
                            <g id="Llantas">
                                <g>
                                    <circle class="st1" cx="37.1" cy="78.8" r="7.5"/>
                                    <path class="st2" d="M37.1,72.6c3.4,0,6.1,2.8,6.1,6.1c0,3.4-2.8,6.1-6.1,6.1s-6.2-2.7-6.2-6C30.9,75.4,33.7,72.6,37.1,72.6
                                        M37.1,69.9c-4.9,0-8.8,3.9-8.8,8.8s3.9,8.8,8.8,8.8s8.8-3.9,8.8-8.8C45.9,73.9,42,69.9,37.1,69.9L37.1,69.9z"/>
                                </g>
                                <g>
                                    <path class="st1" d="M80.5,86.3c-3.7,0-6.9-2.8-7.4-6.5c-0.1-0.3-0.1-0.7-0.1-1c0-0.9,0.2-1.7,0.4-2.4l0,0l0.1-0.2
                                        c0.1-0.2,0.2-0.3,0.2-0.5c0.1-0.2,0.2-0.2,0.2-0.4c0.1-0.1,0.1-0.2,0.1-0.2c0.1-0.2,0.2-0.2,0.2-0.4s0.2-0.2,0.3-0.4l0.1-0.1
                                        c0.1-0.1,0.2-0.2,0.2-0.3c0.2-0.2,0.2-0.2,0.4-0.4c0.3-0.3,0.7-0.6,1-0.8c0.1-0.1,0.2-0.2,0.3-0.2c0.2-0.1,0.3-0.2,0.4-0.2L77,72
                                        c0.2-0.1,0.3-0.2,0.5-0.2c0.2-0.1,0.2-0.1,0.3-0.2c0.1,0,0.2-0.1,0.2-0.1c0.5-0.2,1-0.2,1.5-0.3h0.1h0.1h0.1c0.2,0,0.3,0,0.5,0
                                        s0.3,0,0.5,0s0.3,0,0.5,0.1c0.2,0,0.3,0.1,0.4,0.1c0.2,0,0.2,0.1,0.4,0.1c0.2,0,0.2,0.1,0.3,0.1c0.2,0.1,0.4,0.2,0.6,0.2
                                        c0.1,0.1,0.2,0.1,0.4,0.2h0.1c0,0,0.2,0.1,0.3,0.2c0.2,0.1,0.2,0.2,0.4,0.2c0.7,0.4,1.4,1,2,1.6l0.1,0.1c0.2,0.3,0.5,0.7,0.7,1.1
                                        c0.2,0.3,0.3,0.6,0.4,0.8s0.2,0.4,0.2,0.6c0.2,0.7,0.3,1.5,0.3,2.2c0,0.3,0,0.7-0.1,1C87.4,83.4,84.2,86.3,80.5,86.3z"/>
                                    <g>
                                        <path class="st2" d="M80.5,72.6c0.2,0,0.2,0,0.4,0c0.1,0,0.2,0,0.3,0h0.1h0.1c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.2,0.1h0.1H82
                                            c0.1,0,0.2,0,0.2,0.1l0,0l0,0c0.2,0,0.2,0.1,0.4,0.2c0.1,0,0.2,0.1,0.3,0.1l0.1,0.1l0,0c0.1,0,0.2,0.1,0.2,0.1
                                            c0.1,0.1,0.2,0.1,0.3,0.2l0,0l0,0c0.6,0.3,1.1,0.8,1.5,1.3l0.1,0.1c0.2,0.2,0.4,0.6,0.6,0.8v0.1v0.1c0.1,0.2,0.2,0.4,0.2,0.6v0.1
                                            v0.1c0.1,0.1,0.1,0.2,0.2,0.3c0.2,0.6,0.2,1.2,0.2,1.8c0,0.2,0,0.5-0.1,0.8c-0.4,3-3,5.4-6.1,5.4c-3.1,0-5.7-2.3-6.1-5.4
                                            c0-0.2-0.1-0.5-0.1-0.7c0-0.6,0.1-1.2,0.2-1.7l0,0l0.2-0.5l0,0c0.1-0.1,0.1-0.2,0.2-0.3c0.1-0.1,0.1-0.2,0.2-0.3l0,0v-0.1
                                            l0.1-0.1l0,0l0,0c0.1-0.1,0.2-0.2,0.2-0.3c0.1-0.1,0.1-0.2,0.2-0.2l0,0l0.1-0.1l0.2-0.2l0,0l0.1-0.1l0.1-0.1l0.1-0.1
                                            c0.1-0.1,0.2-0.2,0.2-0.2l0,0l0,0c0.2-0.2,0.5-0.5,0.8-0.7c0.1-0.1,0.2-0.2,0.3-0.2h0.1h0.1c0.1-0.1,0.2-0.1,0.2-0.2h0.1h0.1
                                            c0.1-0.1,0.2-0.1,0.3-0.2l0,0l0,0c0.1,0,0.2-0.1,0.2-0.1l0,0l0,0h0.1h0.1h0.1c0.3-0.1,0.7-0.2,1.1-0.2h0.1l0,0l0,0h0.1
                                            C80.2,72.6,80.3,72.6,80.5,72.6 M80.5,69.9c-0.2,0-0.4,0-0.6,0c-0.1,0-0.2,0-0.2,0c-0.6,0.1-1.2,0.2-1.7,0.3
                                            c-0.1,0-0.2,0.1-0.2,0.1c-0.2,0.1-0.3,0.1-0.4,0.2c-0.2,0.1-0.3,0.2-0.5,0.2c-0.1,0-0.1,0.1-0.2,0.1c-0.2,0.1-0.3,0.2-0.5,0.2
                                            c-0.2,0.1-0.3,0.2-0.4,0.2c-0.4,0.2-0.8,0.6-1.1,0.9c-0.2,0.2-0.3,0.2-0.4,0.4l0,0c-0.2,0.2-0.2,0.3-0.4,0.5l0,0
                                            c-0.2,0.2-0.2,0.3-0.4,0.5c-0.1,0.2-0.2,0.3-0.2,0.5c-0.1,0.1-0.1,0.2-0.2,0.2c-0.1,0.2-0.2,0.3-0.2,0.5
                                            c-0.1,0.2-0.2,0.4-0.2,0.6l0,0c0,0.1-0.1,0.2-0.1,0.2c-0.3,0.9-0.5,2-0.5,3c0,0.4,0,0.7,0.1,1.1c0.6,4.3,4.2,7.7,8.7,7.7
                                            s8.2-3.3,8.8-7.6c0.1-0.4,0.1-0.7,0.1-1.1c0-0.9-0.2-1.8-0.4-2.6c-0.1-0.2-0.2-0.4-0.2-0.7c-0.2-0.3-0.3-0.7-0.5-1
                                            c-0.2-0.4-0.5-0.9-0.8-1.2l-0.1-0.1c-0.7-0.8-1.5-1.5-2.3-2c-0.2-0.1-0.3-0.2-0.4-0.2c-0.2-0.1-0.2-0.2-0.4-0.2c0,0,0,0-0.1,0
                                            c-0.2-0.1-0.3-0.2-0.5-0.2c-0.2-0.1-0.4-0.2-0.7-0.2c-0.2,0-0.2-0.1-0.4-0.1c-0.2-0.1-0.3-0.1-0.6-0.1c-0.2,0-0.2-0.1-0.4-0.1
                                            c-0.2,0-0.4-0.1-0.7-0.1C80.8,69.9,80.6,69.9,80.5,69.9L80.5,69.9z"/>
                                    </g>
                                </g>
                            </g>
                            <g id="entregas">
                                <g>
                                    <path class="st1" d="M95.8,69.2c-0.7,0.1-1.2,0.1-1.9,0c-1.3-0.2-1.7,0.5-1.6,1.7c0.1,1.2,0.2,2,1.6,1.9c0.3,0,0.6-0.1,0.9-0.1
                                        l0,0c2.6,0,2.5,0,2.6-2C97.5,69.5,97,69.1,95.8,69.2z"/>
                                    <path class="st1" d="M96.1,75.3c-1.6,0-3.2-0.1-4.8,0c-1.1,0.1-1.6-0.3-1.6-1.5v-5.7c0-1.1,0.4-1.5,1.5-1.5c1.6,0.1,3.3,0,5,0
                                        c0.9,0,1.3-0.3,1.3-1.3c0-1.7,0.1-3.5-0.1-5.3c-0.1-1.1-0.4-2.3-0.9-3.3c-2.2-3.7-4.5-7.2-6.7-10.7c-0.5-1-1.5-1.5-2.6-1.5
                                        c-5.3,0.1-10.7,0.1-15.9,0c-0.6,0-1.1,0.1-1.4,0.2H67c0-4.1,0-8.1,0-12.1c0-1.5-0.4-2-2-2c-13.9,0.1-27.7,0.1-41.6,0
                                        c-1.5,0-2,0.5-2,2c0,7.2,0,14.2,0,21.4v21.6c0,1.4,0.4,1.9,1.8,1.8c1.2-0.1,2.4-0.1,3.6,0c0.9,0.2,1.7-0.4,1.9-1.3l0,0
                                        c1.5-4.7,6.6-7.2,11.3-5.7c2.6,0.9,4.7,2.8,5.6,5.5c0.2,1.1,1.2,1.7,2.3,1.5c5.8-0.1,11.6-0.1,17.3,0c0.3,0,0.6,0,0.8-0.1h4.1
                                        c0.2,0.1,0.4,0.1,0.7,0H71v-0.2c0.2-0.2,0.3-0.4,0.5-0.8c1.5-4.3,4.6-6.6,8.5-6.6c3,0,5.5,1.1,7.2,3.4l0.1,0.1
                                        c0.3,0.4,0.6,0.8,0.8,1.2c0.2,0.3,0.3,0.7,0.5,1c0.1,0.2,0.2,0.4,0.2,0.7c0,0,0.1,0,0.1,0.1c0.2,0.6,0.8,1,1.4,1.1
                                        c2,0.1,3.9,0.1,5.9,0c0.6-0.1,1-0.5,1.1-1C97.6,75.4,96.8,75.3,96.1,75.3z"/>
                                </g>
                                <g>
                                    <g>
                                        <path class="st2" d="M98.3,54.9c-2.3-3.7-4.6-7.2-6.8-10.9c-0.7-1.4-2.2-2.1-3.7-2H71.4c-2.4,0-2.4,0-2.4-2.4v-8.2
                                            c0.1-2.8-0.2-3.2-3-3.2l-44.5,0.1c-2.3,0-2.8,0.6-2.8,2.9v22.9h0.2v23.1c0,2.2,0.6,2.7,2.8,2.8c1.7,0,3.4,0.1,5.1,0
                                            c0.1,0,0.8,0,1.6,0c0.6,0,1.1,0,1.5,0c-0.1-0.4-0.2-3.6-0.2-3.6s-1.2,0.3-0.8-0.7c0.2-0.4,0.3-0.8,0.6-1.2
                                            c0.1-0.2,0.2-0.4,0.3-0.6s0.2-0.3,0.4-0.6c0.2-0.2,0.2-0.3,0.4-0.5l0,0c0.2-0.2,0.3-0.3,0.4-0.5c0.3-0.3,0.7-0.6,1.1-0.8
                                            c0.2-0.1,0.3-0.2,0.5-0.3c0,0,0,0,0.1,0c0.2-0.1,0.3-0.2,0.6-0.3c0.2-0.2,0.5-0.2,0.7-0.3c0.2-0.1,0.3-0.2,0.5-0.2h0.1
                                            c0.2-0.1,0.4-0.1,0.6-0.2c0.2-0.1,0.4-0.1,0.7-0.2c0.2,0,0.3,0,0.5-0.1c0.1,0,0.2,0,0.3,0c0.2,0,0.4,0,0.6,0c0.2,0,0.4,0,0.7,0
                                            c0.5,0,1,0.1,1.4,0.2c0.2,0,0.2,0.1,0.4,0.1c0.3,0.1,0.6,0.2,0.9,0.3c0.1,0,0.2,0.1,0.3,0.2s0.2,0.1,0.3,0.2
                                            c0.2,0.1,0.4,0.2,0.6,0.3c0.1,0.1,0.2,0.2,0.3,0.2c0.2,0.1,0.3,0.2,0.4,0.3c0.5,0.4,1,0.8,1.3,1.2c0.2,0.2,0.2,0.3,0.4,0.5
                                            c0.4,0.5,0.7,1,1,1.5l0,0c0.2,0.6-0.6,1.7-0.6,1.7s0.3,2.9,0.2,3.3c0.3,0,0.7,0,1.1,0c0.7,0,1.5,0,1.6,0c7.5,0,15,0,22.5,0
                                            c0.1,0,1.1,0,1.8,0c0.3,0,0.7,0,0.7,0l0.1-3.4c0,0-0.5,0.1-0.2-0.7c0-0.1,0-0.2,0.1-0.2l0,0c0.1-0.2,0.2-0.4,0.2-0.6
                                            c0.1-0.2,0.2-0.3,0.2-0.5c0-0.1,0.1-0.2,0.2-0.2c0.1-0.2,0.2-0.3,0.2-0.5c0.1-0.2,0.2-0.3,0.4-0.5l0,0c0.1-0.2,0.2-0.3,0.4-0.5
                                            l0,0c0.2-0.2,0.2-0.3,0.4-0.4c0.3-0.3,0.7-0.7,1.1-0.9c0.2-0.1,0.3-0.2,0.4-0.2c0.2-0.1,0.3-0.2,0.5-0.2c0.1,0,0.1-0.1,0.2-0.1
                                            c0.2-0.1,0.3-0.2,0.5-0.2c0.2-0.1,0.2-0.1,0.4-0.2c0.1,0,0.2-0.1,0.2-0.1c0.6-0.2,1.1-0.3,1.7-0.3c0.1,0,0.2,0,0.2,0
                                            c0.2,0,0.4,0,0.6,0c0.2,0,0.4,0,0.6,0c0.2,0,0.4,0,0.7,0.1c0.2,0,0.3,0,0.4,0.1c0.2,0,0.3,0.1,0.6,0.1c0.2,0,0.2,0.1,0.4,0.1
                                            c0.2,0.1,0.4,0.2,0.7,0.2c0.2,0.1,0.3,0.2,0.5,0.2c0,0,0,0,0.1,0c0.2,0.1,0.3,0.2,0.4,0.2c0.2,0.1,0.3,0.2,0.4,0.2
                                            c1.1,0.7,2,1.5,2.6,2.4c-0.1-0.1-0.2-0.2-0.2-0.3l-0.1-0.1c-1.7-2.3-4.1-3.4-7.2-3.4c-4,0-6.9,2.3-8.5,6.6
                                            c-0.3,0.9-0.9,1.2-1.6,1c-0.5-0.3-0.7-0.8-0.8-1.4V60.9c0-4.8,0.1-9.6,0-14.4c0-1.5,0.5-2,2-2c5.3,0.1,10.7,0.1,15.9,0
                                            c1.1-0.1,2.1,0.5,2.6,1.5c2.2,3.6,4.6,7.1,6.7,10.7c0.6,1.1,0.9,2.2,0.9,3.3c0.2,1.7,0.1,3.5,0.1,5.3c0,1-0.4,1.3-1.3,1.3
                                            c-1.6,0-3.3,0.1-5,0c-1.1,0-1.5,0.4-1.5,1.5v5.7c0,1.1,0.5,1.5,1.6,1.5c1.6-0.1,3.2,0,4.8,0c0.7,0,1.5,0.1,1.2,1.1
                                            c-0.2,0.5-0.7,0.9-1.1,1c-2,0.1-3.9,0.1-5.9,0c-0.6-0.1-1.1-0.5-1.4-1.1c0,0,0,0-0.1-0.1c-0.2-0.1-1.1,0.2-1.1,0.2l0.1,3.5
                                            c0,0,0.7,0,1.4,0c0.7,0,1.2,0,1.3,0H97c2.4,0,3-0.7,3-3V61C100.1,58.8,99.5,56.7,98.3,54.9z M67,75.4c0,1.5-0.4,2-1.9,2
                                            c-5.8-0.1-11.6-0.1-17.3,0c-1.1,0.2-2-0.5-2.3-1.5c-0.9-2.6-3-4.6-5.6-5.5c-4.7-1.5-9.8,1-11.3,5.7l0,0c-0.2,0.9-1.1,1.5-1.9,1.3
                                            c-1.2-0.1-2.4-0.1-3.6,0c-1.3,0.1-1.8-0.4-1.8-1.8V54.1c0-7.2,0-14.2,0-21.4c0-1.5,0.5-2,2-2c13.9,0.1,27.7,0.1,41.6,0
                                            c1.5,0,2,0.5,2,2C67,46.9,67,61.1,67,75.4z M94.9,72.8L94.9,72.8c-0.3,0-0.7,0-0.9,0c-1.5,0.2-1.5-0.7-1.6-1.9s0.3-1.9,1.6-1.7
                                            c0.7,0.1,1.2,0.1,1.9,0c1.1-0.1,1.6,0.3,1.6,1.5C97.5,72.8,97.5,72.8,94.9,72.8z"/>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path class="st2" d="M93.3,58.9c-1.5-3.2-3.3-6.1-5-9.2c-0.6-1.1-1.7-1.8-2.9-1.7c-3.6,0-7.2,0.1-10.8,0c-1.3,0-1.7,0.5-1.7,1.7
                                            c0,4.3,0.1,8.5,0,12.9c0,1.5,0.6,1.9,2,1.9h8.4l0,0c2.8,0,5.7,0.1,8.5-0.1c0.7,0,2-0.7,2-1.1C93.9,61.8,93.8,60.3,93.3,58.9z
                                            M89.4,61.9h-6.2c-2.2,0-4.3,0-6.5,0c-1,0-1.4-0.4-1.4-1.4v-8.6c0-0.9,0.4-1.3,1.3-1.3c2.8,0.1,5.5,0,8.2,0.1
                                            c0.6,0.1,1.1,0.4,1.5,0.9c1.6,2.7,3.1,5.4,4.6,8.1C91.8,61.4,91.4,61.9,89.4,61.9z"/>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path class="st2" d="M55.4,59.4c-2.4-0.1-4.6,2-4.6,4.4c-0.1,2.4,2,4.6,4.4,4.6c0.1,0,0.1,0,0.2,0c2.4,0,4.5-2,4.5-4.4
                                            C59.8,61.5,57.9,59.5,55.4,59.4z M55.3,66.8c-1.5-0.1-2.8-1.3-2.8-2.9c0.1-1.5,1.4-2.8,2.9-2.8c1.5,0.1,2.8,1.3,2.8,2.9
                                            C58.1,65.5,56.8,66.7,55.3,66.8z"/>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path class="st2" d="M58.1,39.3c-1.5,0-2.7,1.1-2.7,2.7c0,1.5,1.1,2.7,2.7,2.7c1.5,0,2.7-1.1,2.7-2.7
                                            C60.8,40.5,59.6,39.3,58.1,39.3z M58.1,43.6c-0.9-0.1-1.5-0.7-1.6-1.6c-0.1-1,0.7-1.8,1.6-1.9c1,0,1.7,0.8,1.7,1.7
                                            C59.7,42.8,59,43.6,58.1,43.6z"/>
                                    </g>
                                </g>
                                <g>
                                    <path class="st2" d="M43.4,45.9c-2.3,0.1-2.5-0.2-2.4-2.4c0.1-2.8-1-3.8-3.8-3.8c-0.7,0-1.5-0.3-2.3-0.3c-0.4,0.1-0.8,0.2-1.1,0.3
                                        c0.2,0.3,0.2,1,0.4,1.1c1,0.3,2,0.6,2.9,0.7c1.8,0,2.4,0.5,2.3,2.2c-0.2,3,0.9,4.2,3.9,4.1c2.2-0.1,2.4,0.2,2.4,2.4
                                        c0.1,0.9,0.2,1.8,0.6,2.7c0.1,0.2,0.7,0.3,1.1,0.5c0.2-0.4,0.2-0.8,0.2-1.2c-0.2-0.7-0.2-1.4-0.3-2.1C47.4,47,46.1,45.8,43.4,45.9
                                        z"/>
                                </g>
                                <g>
                                    <polygon class="st2" points="36.1,55.6 33,58.8 29.9,55.6 29.1,56.5 32.2,59.6 29.1,62.8 29.9,63.7 33,60.5 36.1,63.7 37,62.8
                                        33.9,59.6 37,56.5 		"/>
                                </g>
                                <g>
                                    <path class="st2" d="M87.5,73.6c0.4,0.6,0.7,1.2,1.1,2C88.4,74.8,88,74.2,87.5,73.6z"/>
                                </g>
                            </g>
                        </svg>

                    </div>
                    <p>Programa tu entrega</p>
                </div></a>
        </nav>

        <div class="card-title card-title--yellow card-title--animated">
            <a href="proceso-globos.html">
                <div class="card-title__header">globos</div>
                <div class="card-title__main">
                    <div class="card-title__main__image">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 115.6" style="enable-background:new 0 0 100 115.6;" xml:space="preserve">
                        <style type="text/css">
                            .st4{fill:#f1b144;}
                            .st5{fill:#fff;}
                        </style>
                            <g id="Capa_2_1_">
                                <g id="Capa_1-2">
                                    <g class="globo-2">
                                        <path class="st4" d="M91.4,49.1c-0.2,5-1.9,9.9-4.9,13.9c-4.4,6.1-10.4,10.5-16.9,14.1c-0.6,0.3-1.1,0.5-1.7,0.7
                                            c-0.2,0-0.5,0.2-0.6,0.3c-0.5,0.5-0.5,1.3-0.1,1.8c0.9,1.3,0.6,2.1-1,2.4c-0.3,0-0.6,0-0.9,0c-0.3,0-0.6,0-0.9,0
                                            c-1.5-0.2-1.9-1.1-1-2.4c0.5-0.5,0.5-1.3-0.1-1.8c-0.2-0.2-0.4-0.3-0.6-0.3c-0.6-0.2-1.2-0.4-1.7-0.7c-6.5-3.6-12.5-8-16.9-14.1
                                            c-3-4-4.7-8.8-4.9-13.9c0.1-0.6,0.1-1.5,0.2-2.3c1-5.5,4.1-9.5,9.5-11.4c5.1-1.7,9.4-0.2,13.1,3.6c0.6,0.6,1.4,1.1,2.3,1.3
                                            c0.3,0.1,0.7,0.1,1,0.1h0c0.3,0,0.7,0,1-0.1c0.8-0.3,1.6-0.7,2.3-1.3c3.7-3.8,8-5.3,13.1-3.6c5.4,1.8,8.5,5.8,9.5,11.4
                                            C91.4,47.6,91.4,48.5,91.4,49.1z"/>
                                        <path class="st5" d="M93.1,43.9c-3.3-11-16.9-16.1-25.9-7c-0.7,0.7-1.2,1-1.8,1c-0.5,0-1.1-0.3-1.8-1c-9-9.1-22.7-4-25.9,7
                                            c-1.7,5.7-0.7,11,1.9,16.2c3,6,7.6,10.6,12.9,14.4c2.6,1.9,5.3,3.5,8.1,5.3c-0.3,2.5,0.4,3.8,2.9,4.7c0.7,0.3,0.6,0.7,0.6,1.1v26
                                            c0,0.4-0.1,0.8,0.1,1.1c0.2,0.3,0.8,1,1.1,0.9c0,0,0.1,0,0.2-0.1c0,0,0.1,0,0.2,0.1c0.4,0.1,0.9-0.7,1.1-0.9
                                            c0.2-0.3,0.1-0.7,0.1-1.1v-26c0-0.4-0.1-0.9,0.6-1.1c2.5-0.9,3.2-2.2,2.9-4.7c2.7-1.8,5.5-3.4,8.1-5.3c5.3-3.9,9.9-8.4,12.9-14.4
                                            C93.8,54.9,94.7,49.6,93.1,43.9z M86.5,62.9c-4.4,6.1-10.4,10.5-16.9,14.1c-0.6,0.3-1.1,0.5-1.7,0.7c-0.2,0-0.5,0.2-0.6,0.3
                                            c-0.5,0.5-0.5,1.3-0.1,1.8c0.9,1.3,0.6,2.1-1,2.4c-0.3,0-0.6,0-0.9,0c-0.3,0-0.6,0-0.9,0c-1.5-0.2-1.9-1.1-1-2.4
                                            c0.5-0.5,0.5-1.3-0.1-1.8c-0.2-0.2-0.4-0.3-0.6-0.3c-0.6-0.2-1.2-0.4-1.7-0.7c-6.5-3.6-12.5-8-16.9-14.1c-3-4-4.7-8.8-4.9-13.9
                                            c0.1-0.6,0.1-1.5,0.2-2.3c1-5.5,4.1-9.5,9.5-11.4c5.1-1.7,9.4-0.2,13.1,3.6c0.6,0.6,1.4,1.1,2.3,1.3c0.3,0.1,0.7,0.1,1,0.1h0
                                            c0.3,0,0.7,0,1-0.1c0.8-0.3,1.6-0.7,2.3-1.3c3.7-3.8,8-5.3,13.1-3.6c5.4,1.8,8.5,5.8,9.5,11.4c0.2,0.9,0.2,1.8,0.2,2.3
                                            C91.3,54.1,89.6,58.9,86.5,62.9z"/>
                                        <path class="st5" d="M88.3,48.2c0,1-0.3,1.5-1.1,1.6c-0.7,0.1-1.3-0.4-1.4-1.2c-0.3-2.7-1.7-5.2-4-6.7c-1.1-0.6-2.3-1.1-3.5-1.4
                                            c-1.1-0.3-1.7-0.7-1.5-1.5s0.8-1.2,2-1c4.7,0.7,8.5,4.4,9.3,9.1C88.2,47.5,88.3,47.9,88.3,48.2z"/>
                                    </g>
                                    <g class="globo-1">
                                        <path class="st4" d="M50.3,87.2c-3,3.5-7.2,5.8-11.9,6.3c-1.6,0.2-1.9,0.6-1.7,2.2c0.1,0.7,0.2,1.4,0.3,2.3h-3.4l0.7-2.3
                                            c0.4-1.3,0-1.9-1.3-2C20.6,92.4,13,80.6,15.3,68.9c1.4-7.7,6-13.2,13.4-15.8c8.3-2.9,17.6,0.1,22.7,7.3
                                            C57.2,68.5,56.8,79.6,50.3,87.2z"/>
                                        <path class="st5" d="M52,72.9c0,3.2-0.8,6.3-2.3,9c-0.3,0.5-0.8,0.8-1.3,0.9c-0.3,0-0.9-0.5-1-0.9s0.1-0.9,0.3-1.2
                                            c2.2-4.1,2.5-8.9,0.8-13.3c-1.4-3.8-4.1-6.9-7.8-8.7c-0.3-0.1-0.6-0.3-0.9-0.5c-0.6-0.3-0.8-1-0.5-1.5c0,0,0,0,0-0.1
                                            c0.2-0.6,0.9-0.9,1.4-0.7c0,0,0.1,0,0.1,0c1.1,0.5,2.1,1,3.1,1.7C49,61.1,52,66.8,52,72.9z"/>
                                        <path class="st5" d="M52.4,57.8c-3-3.8-7.2-6.4-11.9-7.6c-0.9-0.2-2.4-0.7-3.6-0.8c-14-0.9-23.4,10.1-24.4,20.8
                                            C11.4,82,18.7,93.8,31.7,96c-0.1,0.4-0.2,0.7-0.2,1.1c-0.3,1.4,0.5,2.8,1.9,3.2c0.8,0.2,0.8,0.6,0.8,1.2v10.2
                                            c0,0.3-0.1,0.6,0,0.8c0.3,0.4,0.7,1,1.1,1.1c0.7,0.2,1.2-0.3,1.3-1c0-0.3,0-0.6,0-0.9v-10.3c0-0.5,0.1-0.9,0.8-1
                                            c0.1,0,0.2,0,0.3-0.1c1.4-0.4,2.3-1.9,1.9-3.3c-0.1-0.4-0.1-0.7-0.1-1.1c10.1-2,15.8-8.3,18.1-18.1c0.4-1.5,0.4-3.4,0.4-3.8
                                            C58.2,68.1,56.2,62.3,52.4,57.8z M50.3,87.2c-3,3.5-7.2,5.8-11.9,6.3c-1.6,0.2-1.9,0.6-1.7,2.2c0.1,0.7,0.2,1.4,0.3,2.3h-3.4
                                            l0.7-2.3c0.4-1.3,0-1.9-1.3-2C20.6,92.4,13,80.6,15.3,68.9c1.4-7.7,6-13.2,13.4-15.8c8.3-2.9,17.6,0.1,22.7,7.3
                                            C57.2,68.5,56.8,79.6,50.3,87.2z"/>
                                    </g>
                                    <g>
                                        <path class="st5 rotacion" d="M88.7,95.8c0,0.3,0,0.6-0.1,0.8c0,1.4,0.5,1.9,1.9,1.9c0.6,0,1.1-0.1,1.7-0.1c2-0.2,3.7,1.3,3.9,3.3
                                            c0,0.2,0,0.4,0,0.6c-0.1,0.4-0.1,0.8-0.1,1.2c0,0.7,0.2,1.3,0.4,1.9c0.3,0.6,0.4,1.1-0.2,1.6c-0.4,0.4-1.1,0.3-1.4-0.1
                                            c0-0.1-0.1-0.1-0.1-0.2c-0.4-0.7-0.7-1.4-0.8-2.2c-0.1-0.7,0-1.4,0.1-2.1c0.1-1.4-0.5-2-1.9-2c-0.5,0-1,0.1-1.4,0.1
                                            c-2,0.2-3.9-1.2-4.1-3.3c0-0.3,0-0.6,0-0.9c0-0.5,0.1-1,0.1-1.4c0-1.4-0.6-2-2-1.9c-0.7,0.1-1.3,0.1-2,0.1
                                            c-0.8-0.2-1.5-0.5-2.2-1c-0.3-0.4-0.3-1-0.1-1.4c0.1-0.2,0.9-0.2,1.3-0.2c0.7,0.2,1.5,0.4,2.2,0.4C87.3,90.7,89,92.3,88.7,95.8z"
                                        />
                                        <path class="st5 rotacion" d="M21.7,110.2c-0.2,3-2.8,5.2-5.8,4.9c-2.6-0.2-4.7-2.3-4.9-4.9c-0.1-3,2.1-5.5,5.1-5.6c3-0.1,5.5,2.1,5.6,5.1
                                            C21.8,109.9,21.8,110.1,21.7,110.2L21.7,110.2z M16.4,107c-1.8,0-3.3,1.5-3.3,3.3c0.1,1.8,1.7,3.2,3.5,3.1c1.7-0.1,3-1.4,3.1-3.1
                                            C19.6,108.4,18.2,107,16.4,107z"/>
                                        <path class="st5 rotacion rotacion" d="M93.8,79.2l-3.4-3.1l1.7-1.6l3,3.5l3.1-3.6l1.6,1.8l-3.4,3l3.5,3.1l-1.7,1.7l-3-3.5L92.2,84l-1.5-1.7
                                            L93.8,79.2z"/>
                                        <path class="st5 rotacion" d="M3.6,102.4c-2,0-3.6-1.6-3.6-3.5c0,0,0,0,0-0.1c0-2,1.6-3.6,3.6-3.6c2,0,3.6,1.6,3.6,3.6
                                            C7.2,100.8,5.6,102.4,3.6,102.4z M5.1,98.8c0-0.8-0.6-1.5-1.4-1.5c0,0,0,0,0,0c-0.8,0-1.5,0.6-1.5,1.4s0.6,1.5,1.4,1.5
                                            C4.3,100.3,5,99.7,5.1,98.8C5.1,98.9,5.1,98.8,5.1,98.8z"/>
                                        <path class="st5 rotacion" d="M23.4,39.3c-2,0-3.6-1.6-3.6-3.6c0,0,0,0,0-0.1c0-2,1.6-3.6,3.6-3.6c2,0.1,3.5,1.7,3.5,3.6
                                            C27,37.7,25.4,39.3,23.4,39.3z M24.9,35.7c0-0.8-0.7-1.5-1.5-1.5c0,0,0,0,0,0c-0.8,0-1.5,0.7-1.5,1.5c0,0.8,0.7,1.5,1.5,1.5
                                            C24.2,37.2,24.9,36.5,24.9,35.7C24.9,35.7,24.9,35.7,24.9,35.7z"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <p>Agrega globos</p>
                </div></a>
        </div>
        <div class="card-title card-title--blue card-title--animated">
            <a href="tienda.html">
                <div class="card-title__header">envolturas</div>
                <div class="card-title__main">
                    <div class="card-title__main__image">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 74 73.4" style="enable-background:new 0 0 74 73.4;" xml:space="preserve">
                            <style type="text/css">
                                .st6{fill:#80ced7;}
                                .st7{fill:#fff;}
                            </style>
                            <g class="gift">
                                <path class="st6" d="M58.8,34.4v9.9c0,0.6-0.2,0.7-0.8,0.7c-1.8,0-1.8,0-1.8,1.8v26.7H18.7c-0.9,0-0.9,0-0.9-0.8V45.9
                                    c0-0.9,0-0.9-0.9-0.9c-1.7,0-1.7,0-1.7-1.7v-8.2c0-0.6,0.2-0.7,0.8-0.7h11.3l0-0.2c-0.3-0.2-0.6-0.4-0.9-0.5c-3.1-1.4-4.5-6-2-9.3
                                    c2.1-2.8,6.1-3.4,8.9-1.3c0.1,0.1,0.3,0.2,0.4,0.3c1,1,1.7,2.2,2,3.6c0.4,1.3,0.7,2.6,1.2,4.1c0.1-0.6,0.2-1,0.3-1.4
                                    c0.6-1.8,1-3.6,1.7-5.4c1.2-2.6,4-4.1,6.8-3.8c3,0.4,5.5,2.7,6.1,5.7c0.6,3.1-1,6.1-3.9,7.4c-0.4,0.2-0.8,0.4-1.2,0.5v0.2H58.8z"/>
                                <path class="st7" d="M27.2,34.2c-0.3-0.2-0.6-0.3-0.9-0.5c-3.1-1.4-4.5-6-2-9.3c2.1-2.8,6.1-3.4,8.9-1.3c0.1,0.1,0.3,0.2,0.4,0.3
                                    c1,1,1.7,2.2,2,3.6c0.4,1.3,0.7,2.6,1.2,4.1c0.1-0.6,0.2-1,0.3-1.4c0.5-1.8,1-3.6,1.7-5.4c1.2-2.6,4-4.2,6.8-3.8
                                    c3,0.4,5.5,2.7,6.1,5.7c0.6,3.1-1,6.1-3.9,7.4c-0.4,0.2-0.8,0.4-1.2,0.5v0.2h12.2c0,0.3,0,0.5,0,0.7c0,3.1,0,6.2,0,9.2
                                    c0,0.6-0.2,0.7-0.8,0.7c-1.8,0-1.8,0-1.8,1.8c0,8.6,0,17.1,0,25.7c0,0.3,0,0.6,0,1H18.6c-0.9,0-0.9,0-0.9-0.8V45.9
                                    c0-0.9,0-0.9-0.9-0.9c-1.7,0-1.7,0-1.7-1.7c0-2.7,0-5.5,0-8.2c0-0.6,0.2-0.7,0.8-0.7c3.5,0,7,0,10.5,0h0.7L27.2,34.2z M41.2,53.9
                                    c0-5.6,0-11.1,0-16.7c0-0.6-0.2-0.8-0.8-0.8c-2.3,0-4.6,0-6.9,0c-0.5,0-0.7,0.1-0.7,0.7c0,11.2,0,22.3,0,33.5
                                    c0,0.5,0.2,0.7,0.7,0.7c2.3,0,4.6,0,6.9,0c0.7,0,0.8-0.2,0.8-0.8C41.2,65,41.2,59.4,41.2,53.9L41.2,53.9z M30.7,58.2
                                    c0-4.2,0-8.3,0-12.5c0-0.5-0.2-0.7-0.7-0.7c-3.2,0-6.3,0-9.5,0c-0.5,0-0.7,0.2-0.7,0.7c0,8.3,0,16.7,0,25c0,0.6,0.1,0.8,0.7,0.8
                                    c3.1,0,6.2,0,9.4,0c0.6,0,0.8-0.2,0.8-0.8C30.7,66.4,30.7,62.3,30.7,58.2z M54.2,58.2c0-4.2,0-8.3,0-12.5c0-0.6-0.2-0.8-0.8-0.8
                                    c-3.1,0-6.2,0-9.4,0c-0.6,0-0.8,0.1-0.8,0.7c0,8.3,0,16.7,0,25c0,0.6,0.2,0.7,0.8,0.7c3.1,0,6.2,0,9.2,0c0.9,0,0.9,0,0.9-0.9
                                    L54.2,58.2z M38.4,33.7c0.1,0,0.3,0,0.5,0c2.6-0.4,5.2-1,7.7-1.9c2.3-0.8,3.6-3.1,3-5.4c-0.4-2.1-2.2-3.7-4.4-3.9
                                    c-2.1-0.2-4,1-4.7,2.9c-0.5,1.7-1,3.4-1.4,5C38.8,31.6,38.6,32.6,38.4,33.7z M17.3,36.5c0,2,0,4,0,6c0,0.5,0.3,0.4,0.6,0.4h12.2
                                    c0.5,0,0.7-0.1,0.6-0.6c0-1.7,0-3.3,0-5c0-0.7-0.2-0.8-0.8-0.8c-4,0-7.9,0-11.9,0L17.3,36.5z M50.1,36.5c-2,0-4,0-6,0
                                    c-0.6,0-0.7,0.2-0.7,0.7c0,1.7,0,3.4,0,5.1c0,0.5,0.1,0.7,0.6,0.7h12.2c0.5,0,0.6-0.2,0.6-0.6c0-1.7,0-3.4,0-5.1
                                    c0-0.6-0.2-0.7-0.8-0.7C54,36.5,52.1,36.5,50.1,36.5L50.1,36.5z M34.9,33.8c0-0.2,0-0.4,0-0.5c-0.4-1.9-0.8-3.9-1.3-5.8
                                    c-0.2-0.8-0.6-1.6-1.1-2.3c-1.1-1.3-2.9-1.7-4.5-1.1c-1.7,0.6-2.8,2.2-2.8,4c0,1.7,1,3.2,2.6,3.8c1.4,0.5,2.8,0.9,4.2,1.2
                                    C32.8,33.5,33.8,33.6,34.9,33.8z"/>
                            </g>
                            <g>
                                <path class="st7 rotacion" d="M70,13.4c0.1-0.6,0.1-1.2,0.2-1.8c0.1-0.8-0.4-1.6-1.2-1.7c-0.1,0-0.3,0-0.4,0c-0.7,0-1.3,0.1-2,0.1
                                    c-1.9,0.1-3.6-1.5-3.6-3.4c0-0.1,0-0.2,0-0.3c0-0.6,0.1-1.2,0.1-1.8c0.1-1.3-0.5-1.9-1.8-1.9c-0.7,0-1.3,0.1-2,0.1
                                    c-0.8,0-1.6-0.3-2.3-0.8c-0.5-0.4-0.7-1.1-0.3-1.6c0,0,0,0,0-0.1c0.4-0.4,1-0.5,1.4-0.1c0,0,0.1,0.1,0.1,0.1
                                    c0.6,0.4,1.4,0.5,2.1,0.3c1.1-0.2,2.2-0.1,3.2,0.4c1.3,0.9,2,2.5,1.7,4C65,5.6,64.9,6,64.9,6.5c-0.1,0.8,0.5,1.5,1.3,1.5
                                    c0.1,0,0.2,0,0.2,0c0.6,0,1.2-0.1,1.8-0.1c2-0.2,3.8,1.2,4.1,3.2c0,0.4,0,0.8-0.1,1.2c-0.1,0.4-0.1,0.8-0.1,1.2
                                    c0.1,0.5,0.2,0.9,0.4,1.3c0.4,0.5,0.3,1.1-0.1,1.5c-0.5,0.3-1.2,0.2-1.5-0.3c0,0,0,0,0,0C70.3,15.4,70,14.4,70,13.4z"/>
                                <path class="st7 rotacion" d="M9.4,1.9c2.9,0,5.3,2.3,5.3,5.2c0,0,0,0,0,0.1c0,2.9-2.3,5.4-5.3,5.4c0,0,0,0,0,0c-2.9,0-5.3-2.4-5.3-5.3
                                    C4.1,4.3,6.5,1.9,9.4,1.9z M12.7,7.2c0-1.8-1.4-3.2-3.1-3.2c0,0,0,0-0.1,0C7.7,4,6.3,5.4,6.2,7.1c0,1.8,1.4,3.3,3.2,3.3
                                    C11.2,10.4,12.7,9,12.7,7.2z"/>
                                <path class="st7 rotacion" d="M41.5,4.4l3.1-3.2l1.3,1.5l-3.1,3l3.3,3.2l-1.6,1.5l-2.9-3.1l-3.2,3.3l-1.6-1.6L40,5.9c-1-0.9-1.8-1.8-2.7-2.6
                                    c-0.5-0.4-0.5-0.7,0-1.1c0.3-0.3,0.6-0.6,0.8-1L41.5,4.4z"/>
                                <path class="st7 rotacion" d="M74,56.4l-1.5,1.5l-3.1-3.1L66.3,58l-1.4-1.6l3.1-3l-3.2-3.1l1.6-1.5l3,3.2l3.1-3.2l1.6,1.6l-3.2,3L74,56.4z"
                                />
                                <path class="st7 rotacion" d="M1.5,66.1c-0.6,0-1.1-0.4-1.1-1c0-0.2,0-0.3,0.1-0.5c0.4-0.8,1.1-1.4,2-1.6c1.8-0.5,1.8-0.5,1-2.2
                                    c-0.8-1.3-0.4-3.1,0.9-3.9c0.3-0.2,0.6-0.3,1-0.4c1.6-0.5,1.7-0.5,1-2.1c-0.4-0.7-0.6-1.6-0.4-2.4c0.1-0.6,0.7-1,1.2-0.9
                                    c0,0,0.1,0,0.1,0c0.6,0.2,0.8,0.7,0.7,1.4c0,0.5,0.1,1,0.4,1.5C9.1,55,9,56.3,8.2,57.3c-0.6,0.5-1.3,0.9-2,1.1
                                    C5,58.8,4.8,59,5.4,60.1c0.5,0.7,0.7,1.5,0.6,2.3c-0.2,1.1-1,2-2,2.3c-0.7,0.3-1.6,0.3-2,1.1C1.9,66,1.6,66,1.5,66.1z"/>
                                <path class="st7 rotacion" d="M70.6,28.1c0,2-1.6,3.6-3.6,3.6c-2,0-3.6-1.6-3.6-3.6c0-2,1.6-3.6,3.6-3.6C69,24.5,70.5,26.1,70.6,28.1z
                                    M67,26.6c-0.8,0-1.5,0.7-1.5,1.5s0.7,1.5,1.5,1.5c0.8,0,1.5-0.7,1.5-1.6c0,0,0,0,0,0C68.4,27.2,67.8,26.6,67,26.6z"/>
                                <path class="st7 rotacion" d="M0,25.6c-0.1-1.9,1.5-3.5,3.4-3.6c0.1,0,0.1,0,0.2,0c2,0,3.5,1.6,3.6,3.5c0,0,0,0,0,0c0,2-1.6,3.5-3.6,3.6
                                    C1.6,29.1,0,27.5,0,25.6z M3.6,24.1c-0.8,0-1.5,0.6-1.5,1.4c0,0,0,0,0,0c0,0.8,0.6,1.5,1.4,1.5c0.8,0,1.5-0.7,1.5-1.5
                                    C5,24.8,4.4,24.1,3.6,24.1z"/>
                            </g>
                        </svg>

                    </div>
                    <p>Envios nacionales</p>
                </div></a>
        </div>

    </section>

    <!-- ENVOLTURAS -->


    <section class="section-index-3">
        <div class="text">
            <h1>¡Las envolturas más bonitas!</h1>
            <a href="tienda.html" class="btn-xl-secundary fit-content">ver</a>
            <a href="tienda.html" class="btn-md-secundary fit-content">ver</a>
        </div>
    </section>

    <!-- Carrusel Lo más nuevo-->
    <section class="section-index-4 carousel-newer">
        <div class="title-white ">
            <h2>Lo más nuevo</h2>
        </div>

        <div class="carousel-layout">
            <button></button>

            <!--containers-->

            <div class="wrapper" id="carousel-small">

            </div>


            <div class="wrapper" id="carousel-big">

                <!--containers-->

                <div class="products" id="big-1">
               
                </div>
                <div class="products" id="big-2">
                   
                </div>
            </div>
            <button></button>
        </div>
    </section>

    <section class="section-index-5">
        <div class="container">
            <div class="image"><img src="img/Fotos/Regalandia_1575.jpg" alt="Candy Box"></div>
            <div class="main">
                <h2>Candy Box</h2>
                <div>
                    <p>Contiene:</p>
                    <ul>
                        <li>Lorem, ipsum.</li>
                        <li>Lorem, ipsum.</li>
                        <li>Lorem, ipsum.</li>
                    </ul>
                </div>
                <p class="add-balloon">Agregar globos</p>
                <a href="envolturas.html" class="btn-md-secundary">comprar</a>
            </div>
        </div>
    </section>

    <section class="section-index-6">
        <div class="image"><img src="img/icon/track.svg" alt="envios"></div>
        <div class="main">
            <h1>Entregamos sorpresas en zona metropolitana</h1>
            <a id="btn-sm-consultar-zonas" class="btn-sm-secundary-2 fit-content">consulta las zonas de
                envío</a>
            <a id="btn-xl-consultar-zonas" class="btn-xl-secundary-2 fit-content">consulta las zonas de
                envío</a>
        </div>
    </section>
</main>
<footer>
    <div class="left">
        <div class="join">
            <p>¡Únete!</p>
            <div class="icon">
                <a href=""><img src="img/icon/icon-instagram.svg" alt="Regalandia Instagram"></a>
            </div>
            <div class="icon">
                <a href=""><img src="img/icon/icon-facebook.svg" alt="Regalandia Facebook"></a>
            </div>
        </div>
        <p> Monterrey, N.L. | correo@electronico.com</p>
        <div class="links">
            <a href="nosotros.html">Nosotros</a>
            <a href="entregas.html">Entregas</a>
            <a href="proceso-globos.html">Globos</a>
            <a href="contacto.html">Contacto</a>
            <a href="tienda.html">Tienda</a>
        </div>
    </div>
    <div class="right">
        <div class="logo">
            <img src="img/Logotipo/morado.svg" alt="">
        </div>
    </div>
</footer>
<!-- Core plugin JavaScript-->
<script src="vendor/jquery/jquery.min.js "></script>

<script src="js/assets/navbar.js"></script>

<script type="module" src="js/pages/index.js"></script>
</body>

</html>
