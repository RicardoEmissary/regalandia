//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Regalandia
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductoSeleccionado
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductoSeleccionado()
        {
            this.Entrega = new HashSet<Entrega>();
            this.GloboColorSeleccionado = new HashSet<GloboColorSeleccionado>();
            this.ProductoColorSeleccionado = new HashSet<ProductoColorSeleccionado>();
        }
    
        public int id_ProductoSeleccionado { get; set; }
        public Nullable<double> precio_ProductoSeleccionado { get; set; }
        public Nullable<int> cantidad_ProductoSeleccionado { get; set; }
        public Nullable<bool> combinarColores_ProductoSeleccionado { get; set; }
        public string comentario_ProductoSeleccionado { get; set; }
        public Nullable<int> id_Producto { get; set; }
        public Nullable<int> id_Frase { get; set; }
        public Nullable<int> id_Orden { get; set; }
        public Nullable<int> id_DireccionDestinatario { get; set; }
        public Nullable<int> id_ProductoStatus { get; set; }
    
        public virtual Direccion Direccion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Entrega> Entrega { get; set; }
        public virtual Frase Frase { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GloboColorSeleccionado> GloboColorSeleccionado { get; set; }
        public virtual Orden Orden { get; set; }
        public virtual Producto Producto { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductoColorSeleccionado> ProductoColorSeleccionado { get; set; }
        public virtual ProductoStatus ProductoStatus { get; set; }
    }
}
