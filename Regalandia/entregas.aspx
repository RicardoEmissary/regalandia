﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entregas.aspx.cs" Inherits="Regalandia.entregas" %>

<!DOCTYPE html>
<html lang="en">

<head>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Servicio</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="icon" type="image/png" href="img/favico.png">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@200;300;500;800&display=swap" rel="stylesheet">

    <style>

        .close-tiendas{

            position:absolute;
            left:93%;

        }

    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


    <script src="js/handlebars.min-v4.7.6.js"></script>

    <!-- Templates para su utilización con el framework de handlebars -->

    <!-- Templates para el paso 2 de eleccion de envoltura -->
    <script id="paso2Template" type="text/x-handlebars-template">

        {{#each regalos}}
        <table class="envoltura">
            <tbody>
            <tr>
                <th>Regalo</th>
                <th>Tamaño</th>
                <th>Tipo envoltura</th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <td>
                    <span>{{nombre}}</span>
                </td>
                <td>
                    <div class="customSelectContainer customSelectContainer--blue customSelectContainer--sm">
                        <select name="tamaño" class="tamano">
                            <option value="Especial">Especial</option>
                            <option value="Divertido">Divertido</option>
                            <option value="En grande">En grande</option>
                        </select>
                    </div>
                </td>
                <td>
                    <div class="envoltura-btn-container">
                        <button class="btn-envoltura-papel">Papel <span
                                class="fa fa-file"></span></button>
                        <button class="btn-envoltura-bolsas">Bolsa <span
                                class="fa fa-shopping-bag"></span></button>
                    </div>
                </td>
                <td>

                </td>
                <td>
                    <div class="round">
                        <input type="checkbox" />
                        <label for="checkbox0"></label>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        {{/each}}
    </script>

    <!--  -->
    <script id="paso2TemplateTienda" type="text/x-handlebars-template">

        {{#each regalos}}
            <table class="envoltura" id="envolturaT-{{tempId}}">
                <tbody>
                <tr>
                    <th>Regalo </th>
                    <th>Tamaño</th>
                    <th>Tipo envoltura</th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td>
                        <span>{{nombre}}</span>
                    </td>
                    <td>
                        <div class="customSelectContainer customSelectContainer--blue customSelectContainer--sm">
                            <select name="tamaño" class="tamano">
                                <option value="0">Especial</option>
                                <option value="1">Divertido</option>
                                <option value="2">En grande</option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="envoltura-btn-container">
                            <button class="btn-envoltura-papel">Papel <span
                                    class="fa fa-file"></span></button>
                            <button class="btn-envoltura-bolsas">Bolsa <span
                                    class="fa fa-shopping-bag"></span></button>
                        </div>
                    </td>
                    <td>
                        <div class="cart-product-img">

                        </div>
                    </td>
                    <td>
                        <div class="round">
                            <input type="checkbox" />
                            <label for="checkbox0"></label>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        {{/each}}
    </script>

    <script id="modalGlobosTemplate" type="text/x-handlebars-template">
        {{#each articulos}}
        <div class="box-product" id="{{buttonId}}">
            <div class="box-product__image">
                <div class="image frame-container">
                    <div class="frame frame--violet"></div>
                    <img src="img/Fotos/Regalandia_1574.jpg" alt="">
                    <img src="img/Fotos/Regalandia_1558.jpg" alt="">
                </div>
            </div>
            <div class="box-product__description text-purple">
                <p>{{titulo}}</p>{{precio}}
                <button class="btn-sm-blue add-to-cart">agregar</button>
                <a href="#" class="cart"><span></span></a>
            </div>
        </div>
            {{/each}}
    </script>

    <script id="modalProductGlobos" type="text/x-handlebars-template">
        {{#each articulos}}
            <div id="{{id}}" class="modal modal-product">

                <div class="modal-content">
                    <span class="close" id="close-product">&times;</span>
                    <section>
                        <div class="mainTitle mainTitle--balloons">
                            <h3>{{titulo}}</h3>
                        </div>
                        <article class="productCard">
                            <div class="productCard__images">
                                <figure class="productCard__images__mainFigure">
                                    <img src="{{foto1}}" alt="Globos">
                                </figure>
                                <figure class="productCard__images__rowFigure">
                                    <img src="{{foto2}}" alt="Globos">
                                    <img src="{{foto3}}" alt="Globos">
                                    <img src="{{foto4}}" alt="Globos">
                                </figure>
                            </div>

                            <div class="productCard__info">
                                <p class="productCard__info__text">{{titulo}}</p>
                                <p class="productCard__info__productDesc">
                                    {{descripcion}}
                                </p>
                                <p class="productCard__info__text product__costo">Costo <span></span></p>
                                <div class="colorCardGrid">
                                    <div class="colorCard ">
                                        <div class="fixedInputNumber fixedInputNumber--md balloonsAmount">
                                            <p>Cantidad</p>
                                            <input type="number" readonly>
                                            <div class="colorCard__buttons">
                                                <button></button>
                                                <button></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="productCard__info__productColor">
                                    <div class="productColor__rowColor">
                                        <p>Selecciona color</p>
                                        <div class="colorCardGrid">
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--violet"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color1">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--blue"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color2">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--orange"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color3">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--red"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color4">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="productCard__info__productFrase">
                                    <p>Selecciona frase</p>
                                    <div class="productFrase__selectContainer">
                                        <select name="frases" id="frases">
                                            <option value="0">Buen día</option>
                                            <option value="1">Feliz cumpleaños</option>
                                            <option value="2">Felices fiestas</option>
                                        </select>
                                    </div>
                                </div>
                                <button id="modal-product-btn-agregar"  class="btn-md-purple fit-content ">Agregar</button>
                            </div>
                        </article>
                    </section>
                </div>
            </div>
        {{/each}}
    </script>

    <script id="paso5Template" type="text/x-handlebars-template">
        <tbody>
            <tr>
                <th></th>
                <th>Concepto</th>
                <th></th>
                <th>Costo</th>
            </tr>
            {{#each regalos}}
            <tr>
                <td>

                </td>
                <td class="table-concept">
                    {{descripcion}}
                </td>
                <td>
                    %comisión
                </td>
                <td>
                    <div>
                        {{#if precio}}
                        <div>{{precio}}</div>
                        <div class="table-element"><span>+ &nbsp &nbsp 12%</span></div>
                        <div class="table-element last"> <span class="resta"> {{precioyComision}} &nbsp</span></div>
                            {{else}}
                            <span>Sin costo</span>
                        {{/if}}
                    </div>
                </td>
            </tr>

            <tr>
                <td>

                </td>
                <td class="table-concept">
                    {{envolturaNombre}}
                </td>
                <td>
                </td>
                <td>
                    $ {{envolturaPrecio}}
                </td>
            </tr>
            {{/each}}
            {{#each globos.articulos}}
            <tr>
                <td>
                    <div class="cart-product-img">
                        <img width="100px" src="{{foto1}}" />
                    </div>
                </td>
                <td class="table-concept">
                    {{titulo}}<br />
                </td>
                <td>
                </td>
                <td>
                    {{precio}}
                </td>
            </tr>
            {{/each}}
            <tr class="table-row-bold">
                <td></td>
                <td>servicio</td>
                <td></td>
                <td>$100.00</td>
            </tr>
            <tr class="table-row-bold">
                <td></td>
                <td></td>
                <td>Subtotal</td>
                <td>$400.00</td>
            </tr>
        </tbody>
    </script>

    <script id="templateRegalosCasa1Direccion" type="text/x-handlebars-template">


        <div class="card-entregas">
            <div class="card-entregas__header">
                <h3 class="h3__sm">Regalo {{indice}}</h3>

            </div>
            <div class="card-entregas__body">
                <p>Nombre del Regalo</p>
                <textarea  cols="30" rows="3" class="primary textarea__sm inputNombre" placeholder="Nombre"></textarea>
                <p>Tamaño del regalo:</p>
                <div class="recoleccion-content--sizes recoleccion-content--sizes__col inputTamano">
                    <div class="recoleccion-content--radios">
                        <div class="rowRecoleccion-content">
                            <label class="radio radio__primary radio__clear fit-content">especial

                                <input type="radio" name="radio{{indice}}" checked>
                                <span class="checkmark"></span>


                            </label>
                            <div class="infoButton infoButton--sm"></div>
                        </div>
                    </div>
                    <div class="recoleccion-content--radios">
                        <div class="rowRecoleccion-content">
                            <label class="radio radio__primary radio__clear fit-content">divertido

                                <input type="radio" name="radio{{indice}}">
                                <span class="checkmark"></span>


                            </label>
                            <div class="infoButton infoButton--sm"></div>
                        </div>
                    </div>
                    <div class="recoleccion-content--radios">
                        <div class="rowRecoleccion-content">
                            <label class="radio radio__primary radio__clear fit-content">en grande

                                <input type="radio" name="radio{{indice}}">
                                <span class="checkmark"></span>


                            </label>
                            <div class="infoButton infoButton--sm"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-entregas__footer card-entregas__footer__sm">

            </div>
        </div>


    </script>

    <script id="templateRegalosCasaVDireccion" type="text/x-handlebars-template">


        <div class="card-entregas">
            <div class="card-entregas__header">
                <h3 class="h3__sm">Regalo {{indice}}</h3>

            </div>
            <div class="card-entregas__body">
                <div>
                    <div class="form-group">
                        <p>Nombre del Regalo</p>
                        <textarea name="" id="" cols="30" class="primary inputNombre" rows="3" placeholder="Nombre"></textarea>
                    </div>
                    <p>Domicilio para la recolección:</p>
                    <div class="form-group">
                        <input class="inputCalle" type="text" class="primary" placeholder="Calle">
                    </div>
                    <div class="form-group">
                        <input class="inputNumero" type="text" class="primary" placeholder="Número">
                    </div>
                    <div class="form-group">
                        <input class="inputColonia" type="text" class="primary" placeholder="Colonia">
                    </div>
                    <div class="form-group">
                        <input class="inputCiudad" type="text" class="primary" placeholder="Ciudad">
                    </div>
                    <div class="form-group">
                        <input class="inputCP" type="text" class="primary" placeholder="Código Postal">
                    </div>
                    <div class="form-group">
                        <input class="inputEstado" type="text" class="primary" placeholder="Estado">
                    </div>
                    <div class="form-group">
                        <input class="inputReferencia" type="text" class="primary" placeholder="Punto de Referencia">
                    </div>
                    <div class="item-form-recoleccion">
                        <p>Día y hora de recolección:</p>
                        <div class="recoleccion-content--form">
                            <div class="container-time container-time__flex">
                                <div class="date-picker">
                                    <div class="input primary">
                                        <div class="result">Fecha <span></span></div>
                                        <button><i class="fa fa-calendar"></i></button>
                                    </div>
                                    <div class="calendar"></div>
                                </div>

                                <div style="width: 100%">
                                    <p>Hora</p>
                                    <div class="recoleccion-content--form--1cols">
                                        <select name="tiendas" id="tiendas-sugeridas" class="horarios1 recoleccion-content--form__color-primary primary input-hora input-hora__md text-primary">
                                            <option value="#">8:00AM - 11:00AM</option>
                                            <option value="#">11:00AM - 2:00PM</option>
                                            <option value="#">2:00PM - 5:00PM</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>Tamaño del regalo:</p>
                    <form action="" class="recoleccion-content--sizes recoleccion-content--sizes__col">
                        <div class="recoleccion-content--radios inputTamano">
                            <div class="rowRecoleccion-content">
                                <label class="radio radio__primary radio__clear fit-content">especial

                                    <input type="radio" name="radio{{indice}}" checked>
                                    <span class="checkmark"></span>


                                </label>
                                <div class="infoButton infoButton--sm"></div>
                            </div>
                        </div>
                        <div class="recoleccion-content--radios">
                            <div class="rowRecoleccion-content">
                                <label class="radio radio__primary radio__clear fit-content">divertido

                                    <input type="radio" name="radio{{indice}}">
                                    <span class="checkmark"></span>


                                </label>
                                <div class="infoButton infoButton--sm"></div>
                            </div>
                        </div>
                        <div class="recoleccion-content--radios">
                            <div class="rowRecoleccion-content">
                                <label class="radio radio__primary radio__clear fit-content">en grande

                                    <input type="radio" name="radio{{indice}}">
                                    <span class="checkmark"></span>


                                </label>
                                <div class="infoButton infoButton--sm"></div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="card-entregas__footer">

            </div>
        </div>

    </script>


    <script id="templateRegalosTienda1Direccion" type="text/x-handlebars-template">


        <div class="card-entregas">
            <div class="card-entregas__header">
                <h3 class="h3__sm">Regalo {{indice}}</h3>

            </div>
            <div class="card-entregas__body">
                <div>
                    <p>Nombre del Regalo</p>
                    <div class="form-group">

                        <textarea  id="" cols="30" class="primary inputNombre" rows="3" placeholder="Nombre"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Link del producto</label>
                        <input type="text" class="primary inputLink" placeholder="http://">
                    </div>
                    <div class="form-group">
                        <label >Precio</label>
                        <input type="text" class="primary inputPrecio" placeholder="$">
                    </div>
                    <p>Tamaño del regalo:</p>
                    <div class="recoleccion-content--sizes recoleccion-content--sizes__col  inputTamano">
                        <div class="recoleccion-content--radios">
                            <div class="rowRecoleccion-content">
                                <label class="radio radio__primary radio__clear fit-content">especial

                                    <input type="radio" name="radio{{indice}}" checked>
                                    <span class="checkmark"></span>


                                </label>
                                <div class="infoButton infoButton--sm"></div>
                            </div>
                        </div>
                        <div class="recoleccion-content--radios">
                            <div class="rowRecoleccion-content">
                                <label class="radio radio__primary radio__clear fit-content">divertido

                                    <input type="radio" name="radio{{indice}}">
                                    <span class="checkmark"></span>


                                </label>
                                <div class="infoButton infoButton--sm"></div>
                            </div>
                        </div>
                        <div class="recoleccion-content--radios">
                            <div class="rowRecoleccion-content">
                                <label class="radio radio__primary radio__clear fit-content">en grande

                                    <input type="radio" name="radio{{indice}}">
                                    <span class="checkmark"></span>


                                </label>
                                <div class="infoButton infoButton--sm"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="checkbox-container-primary text-sm">Confirma que el producto se encuentra
                            <input type="checkbox" checked="checked">
                            <span class="checkbox-checkmark"></span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="card-entregas__footer">

            </div>
        </div>

    </script>

    <script id="templateRegalosTiendaVDireccion" type="text/x-handlebars-template">


        <div class="card-entregas">
            <div class="card-entregas__header">
                <h3 class="h3__sm">Regalo {{indice}}</h3>

            </div>
            <div class="card-entregas__body">
                <div>
                    <p>Nombre del Regalo</p>
                    <div class="form-group">

                        <textarea name="" id="" cols="30" class="primary inputNombre" rows="3" placeholder="Nombre"></textarea>
                    </div>
                    <div class="form-group">
                        <div  class="recoleccion-content--form">
                            <label for="tiendas">¿Dónde encontrarlo?</label>
                            <div class="recoleccion-content--form--1cols">
                                <select name="tiendas"  class="select__bg select__primary primary text-primary tiendas-sugeridas">
                                    <option value="#">Tiendas sugeridas</option>
                                    <option value="#">Tienda 1</option>
                                    <option value="#">Tienda 2</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <label>Link del producto</label>
                        <input type="text" class="primary inputLink" placeholder="http://">
                    </div>
                    <div class="form-group">
                        <label >Precio</label>
                        <input type="text" class="primary inputPrecio" placeholder="$">
                    </div>
                    <p>Tamaño del regalo:</p>
                    <div class="recoleccion-content--sizes recoleccion-content--sizes__col inputTamano">
                        <div class="recoleccion-content--radios">
                            <div class="rowRecoleccion-content">
                                <label class="radio radio__primary radio__clear fit-content">especial

                                    <input type="radio" name="radio{{indice}}" checked>
                                    <span class="checkmark"></span>


                                </label>
                                <div class="infoButton infoButton--sm"></div>
                            </div>
                        </div>
                        <div class="recoleccion-content--radios">
                            <div class="rowRecoleccion-content">
                                <label class="radio radio__primary radio__clear fit-content">divertido

                                    <input type="radio" name="radio{{indice}}">
                                    <span class="checkmark"></span>


                                </label>
                                <div class="infoButton infoButton--sm"></div>
                            </div>
                        </div>
                        <div class="recoleccion-content--radios">
                            <div class="rowRecoleccion-content">
                                <label class="radio radio__primary radio__clear fit-content">en grande

                                    <input type="radio" name="radio{{indice}}">
                                    <span class="checkmark"></span>


                                </label>
                                <div class="infoButton infoButton--sm"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="checkbox-container-primary text-sm">Confirma que el producto se encuentra
                            <input type="checkbox" checked="checked">
                            <span class="checkbox-checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="card-entregas__footer">

            </div>
        </div>

    </script>

      <!--template para el sidebar-->
    <script id="articulos-envolturas" type="text/x-handlebars-template">



                        <div class="sidebar__title">
                            <div class="image">
                                <img src="img/icon/paper-white.svg" alt="bolsas especiales">
                            </div>
                            bolsas especiales
                        </div>
                        <div class="sidebar__filter">
                            <div class="sidebar-filter sidebar-filter__categoria">
                                <p class="text-purple">Elige por categoría</p>
                                <form>
                                    <label class="radio">ver todo
                                        <input type="radio" value="0" name="sidebar-radio-sub" checked="checked">
                                        <span class="checkmark"></span>
                                    </label>

                                    {{#each subcategorias}}
                                        <label class="radio">{{nombre}}
                                            <input type="radio" value="{{id}}" name="sidebar-radio-sub">
                                            <span class="checkmark"></span>
                                        </label>
                                    {{/each}}
                                </form>
                            </div>

                        </div>
                        <div class="sidebar__filter">
                            <div class="sidebar-filter sidebar-filter__categoria sidebar-tamanos">
                                <p class="text-purple">Elige por tamaño</p>
                                <form>
                                    <label class="radio">ver todo
                                        <input type="radio" value="0" name="sidebar-radio-tamano" checked="checked">
                                        <span class="checkmark"></span>
                                    </label>

                                    <label class="radio">Especial
                                        <input type="radio" value="1" name="sidebar-radio-tamano">
                                        <span class="checkmark"></span>
                                    </label>

                                    <label class="radio">Divertido
                                        <input type="radio" value="2" name="sidebar-radio-tamano">
                                        <span class="checkmark"></span>
                                    </label>

                                    <label class="radio">En grande
                                        <input type="radio" value="3" name="sidebar-radio-tamano">
                                        <span class="checkmark"></span>
                                    </label>

                                </form>
                            </div>

                        </div>






    </script>

    <!--template para el card de eleccion de envoltura-->
    <script id="modalEnvolturasTemplate" type="text/x-handlebars-template">
        {{#each envolturas}}
            <div class="box-product sub-{{categoria}} tam-{{tamano}}" id="bp-{{id}}">
                <div class="box-product__image">
                    <div class="image frame-container">
                        <div class="frame frame--blue"></div>

                        <img src="{{foto1}}" alt="{{nombre}}">
                        <img src="{{foto2}}" alt="{{nombre}}">
                    </div>
                </div>
                <div class="box-product__description text-purple">
                    <p>{{nombre}}</p>$ {{precio}}
                    <button class="btn-sm-blue add-to-cart">agregar</button>
                    <a href="#" class="cart"><span></span></a>
                </div>
            </div>
        {{/each}}
    </script>

    <!--template para el modal-product -->
    <script id="modalProductTemplate" type="text/x-handlebars-template">

        {{#each envolturas}}
            <div id="mp-{{id}}" class="modal modal-product">

                <div class="modal-content">
                    <span class="close">&times;</span>
                    <section>
                        <div class="mainTitle mainTitle--balloons">
                            <h3>{{nombre}}</h3>
                        </div>
                        <article class="productCard">
                            <div class="productCard__images">
                                <figure class="productCard__images__mainFigure">
                                    <img src="{{foto1}}" alt="Globos">
                                </figure>
                                <figure class="productCard__images__rowFigure">
                                    <img src="{{foto2}}" alt="Globos">
                                    <img src="{{foto3}}" alt="Globos">
                                    <img src="{{foto4}}" alt="Globos">
                                </figure>
                            </div>

                            <div class="productCard__info">
                                <p class="productCard__info__text">{{nombre}}</p>
                                <p class="productCard__info__productDesc">
                                    {{descripcion}}
                                </p>
                                <p class="productCard__info__text product__costo">Costo <span class="costo-product"></span></p>
                                <div class="colorCardGrid">
                                    <div class="colorCard ">
                                        <div class="fixedInputNumber fixedInputNumber--md balloonsAmount">
                                            <p>Cantidad</p>
                                            <input type="number" readonly>
                                            <div class="colorCard__buttons">
                                                <button></button>
                                                <button></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="productCard__info__productColor">
                                    <div class="productColor__rowColor">
                                        <p>Selecciona color</p>
                                        <div class="colorCardGrid">
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--violet"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color1">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--blue"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color2" >
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--orange"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color3" >
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--red"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color4" >
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="productCard__info__productFrase">
                                    <p>Selecciona frase</p>
                                    <div class="productFrase__selectContainer">
                                        <select name="frases" id="frases">
                                            <option value="0">Buen día</option>
                                            <option value="1">Feliz cumpleaños</option>
                                            <option value="2">Felices fiestas</option>
                                        </select>
                                    </div>
                                </div>
                                <button id="btn-mp-{{id}}" class="btn-md-purple fit-content ">Agregar</button>
                            </div>
                        </article>
                    </section>
                </div>
            </div>
        {{/each}}

    </script>


</head>
<body>
    <!--Modales para las tiendas de bolsas:-->


    <div id="modal-bolsas-tienda-especial" class="modal-papel">
     <main class="cajas modal-content-papel">
         <span class="close close-tiendas">&times;</span>
        <div class="sidebar"></div>
              
        <div class="right-cajas">
                
        <div class="boxes-grid"></div>
            <div  class="pagination-buttons-container">

            <button class="pag-left-button"><</button>
            <div class=" buttons-container number-buttons-container"></div>
            <button class="pag-right-button">></button>
        </div>

        </div>
    </main>
    </div>



    <div id="modal-bolsas-tienda-divertido" class="modal-papel">
     <main class="cajas modal-content-papel" >
         <span class="close close-tiendas">&times;</span>
        <div class="sidebar"></div>
              
        <div class="right-cajas">
                
        <div class="boxes-grid"></div>
            <div  class="pagination-buttons-container">

            <button class="pag-left-button"><</button>
            <div class=" buttons-container number-buttons-container"></div>
            <button class="pag-right-button">></button>
        </div>

        </div>
    </main>
    </div>



    <div id="modal-bolsas-tienda-enGrande" class="modal-papel">
     <main class="cajas modal-content-papel" >
         <span class="close close-tiendas">&times;</span>
        <div class="sidebar"></div>
              
        <div class="right-cajas">
                
        <div class="boxes-grid"></div>
            <div  class="pagination-buttons-container">

            <button class="pag-left-button"><</button>
            <div class=" buttons-container number-buttons-container"></div>
            <button class="pag-right-button">></button>
        </div>

        </div>
    </main>
    </div>

     <div id="modal-papel-tienda-especial" class="modal-papel">
     <main class="cajas modal-content-papel" >
         <span class="close close-tiendas">&times;</span>
        <div class="sidebar"></div>
              
        <div class="right-cajas">
                
        <div class="boxes-grid"></div>
            <div  class="pagination-buttons-container">

            <button class="pag-left-button"><</button>
            <div class=" buttons-container number-buttons-container"></div>
            <button class="pag-right-button">></button>
        </div>

        </div>
    </main>
    </div>

    <div id="modal-papel-tienda-divertido" class="modal-papel">
     <main class="cajas modal-content-papel" >
         <span class="close close-tiendas">&times;</span>
        <div class="sidebar"></div>
              
        <div class="right-cajas">
                
        <div class="boxes-grid"></div>
            <div  class="pagination-buttons-container">

            <button class="pag-left-button"><</button>
            <div class=" buttons-container number-buttons-container"></div>
            <button class="pag-right-button">></button>
        </div>

        </div>
    </main>
    </div>

    <div id="modal-papel-tienda-enGrande" class="modal-papel">
     <main class="cajas modal-content-papel" >
         <span class="close close-tiendas">&times;</span>
        <div class="sidebar"></div>
              
        <div class="right-cajas">
                
        <div class="boxes-grid"></div>
            <div  class="pagination-buttons-container">

            <button class="pag-left-button"><</button>
            <div class=" buttons-container number-buttons-container"></div>
            <button class="pag-right-button">></button>
        </div>

        </div>
    </main>
    </div>



<div id="modal-sizes" class="modal-primary">
    <div class="modal-content-primary modal-content-primary__bigpd">
        <span class="close-primary close-RCasa" >&times;</span>
        <div class="modal-title">
            <img class="modal-title--img" src="img/icon/house.svg" alt="entregas">
            <h1>Descripción de tamaños</h1>
        </div>
    <div class="desc-tamaños-row">
        <div class="modal-sizes__info" id="info-bolsas">
            <hgroup>
            <h2>Bolsas:</h2>
            <h3><span></span> <label ></label></h3>
            </hgroup>
            <span></span>
            <p></p>
        </div>

        <div class="modal-sizes__info" id="info-papel">
          <hgroup>
            <h2>Papel:</h2>
            <h3><span></span> <label ></label></h3>
          </hgroup>
            <p></p>
        </div>
    </div>
        <div class="modal-primary-btn modal-primary-btn__left">
            <a id="modal-sizes-conf" href="#" class="btn-md-white-secundary fit-content">OK</a>
        </div>
    </div>
</div>


    <div id="articulosEnvolturasContainer"></div>

    <!--Modal papel-->

    <div class="modal-papel" id="modal-bolsa">
        <div class="modal-content-papel" >
            <span class="close">&times;</span>
            <section  class="envoltura envoltura__cajas envoltura--modal">
                <div class="sidebar">
                    <div class="sidebar__title">
                        <div class="image">
                            <img src="img/icon/paper-white.svg" alt="papel decorativo">
                        </div>
                        papel decorativo
                    </div>
                    <div class="sidebar__filter">
                        <div class="sidebar-filter sidebar-filter__categoria">
                            <p class="text-purple">Elige por categoría</p>
                            <form>
                                <label class="radio">ver todo
                                    <input type="radio" name="sidebar-radio-bolsa" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio">mujer
                                    <input type="radio" name="sidebar-radio-bolsa">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio">hombre
                                    <input type="radio" name="sidebar-radio-bolsa">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio">niño
                                    <input type="radio" name="sidebar-radio-bolsa">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio">new born
                                    <input type="radio" name="sidebar-radio-bolsa">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio">botella
                                    <input type="radio" name="sidebar-radio-bolsa">
                                    <span class="checkmark"></span>
                                </label>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="papelBolsa">
                    <div class="boxes-grid">

                    </div>
                </div>


            </section>
        </div>
    </div>


    <div id="envolturas-container">

    </div>

    <!-- Contenedor para el modal de ver productos. Se llena con handlebars(Debe estar vacío)-->
    <div id="modal-product-container-bolsas-especial">

    </div>

     <div id="modal-product-container-bolsas-divertido">

    </div>

     <div id="modal-product-container-bolsas-enGrande">

    </div>

<!-- Contenedor para el modal de ver globos. Se llena con handlebars(Debe estar vacío)-->
    <div id="modal-globos-container">

    </div>

    <!-- Modal de Tipo Recolección Casa-->
    <div id="modalRecoleccionCasa" class="modal-primary">

        <div class="modal-content-primary modal-content-primary__bigpd">
            <span class="close-primary close-RCasa" id="close-ModalCasa">&times;</span>
            <div class="modal-title">
                <img class="modal-title--img" src="img/icon/house.svg" alt="entregas">
                <h1>Recolección en casa</h1>
            </div>
            <div class="form-group">
                <label class="text-white text-left">¿Cuántos regalos vamos a comprar?</label>
                <div class="quantity quantity__primary mt-1">
                    <span class="quantity-signs quantity-signs__white minus-sign">-</span>
                    <input type="number" min="1" max="20" step="1" value="1" id="selectNumRegalosCasa">
                    <span class="quantity-signs quantity-signs__white plus-sign">+</span>
                </div>
            </div>
            <div id="selectAddressHouse" class="form-group selectAddress">
                <label class="text-white text-left">¿Todos los regalos se encuentran en la misma casa?</label>
                <form class="form-group modal-primary--radio" id="MismaDireccionCasa">
                    <div class="form-check form-check-inline" style="width: 10%;">
                        <label class="radio text-secundary">sí
                            <input id="mismaCasa" type="radio" name="radio" value="true" checked="checked">
                            <span class="checkmark-yellow"></span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline" style="width: 10%;">
                        <label class="radio text-secundary">no
                            <input type="radio" name="radio" value="false">
                            <span class="checkmark-yellow"></span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="modal-primary-btn modal-primary-btn__left">
                <a id="modalRCConf" href="#" class="btn-md-white-secundary fit-content confirmar-modal">confirmar</a>
            </div>
        </div>
    </div>

    <!-- Modal de Tipo Recolección Tienda-->
    <div id="modalRecoleccionTienda" class="modal-primary">
        <!-- Modal content -->
        <div class="modal-content-primary modal-content-primary__bigpd">
            <span class="close-primary close-RTienda" id="close-ModalTienda">&times;</span>
            <div class="modal-title">
                <img class="modal-title--img" src="img/icon/store.svg" alt="entregas">
                <h1>Recolección en tienda</h1>
            </div>
            <div class="form-group">
                <label class="text-white text-left">¿Cuántos regalos vamos a recolectar?</label>

                <div class="quantity quantity__primary mt-1">
                    <span class="quantity-signs quantity-signs__white minus-sign">-</span>
                    <input type="number" min="1" max="20" step="1" value="1" id="selectNumRegalosTiendas">
                    <span class="quantity-signs quantity-signs__white plus-sign">+</span>
                </div>
            </div>
            <div id="selectAddressStore" class="form-group selectAddress">
                <label class="text-white text-left">¿Todos los regalos serán recolectados en la misma tienda?</label>
                <form class="form-group modal-primary--radio" id="MismaDireccionTienda">
                    <div class="form-check form-check-inline" style="width: 10%;">
                        <label class="radio text-secundary">sí
                            <input id="mismaTienda" type="radio" name="radio" value="true" checked="checked">
                            <span class="checkmark-yellow"></span>
                        </label>
                    </div>
                    <div class="form-check form-check-inline" style="width: 10%;">
                        <label class="radio text-secundary">no
                            <input type="radio" name="radio" value="false">
                            <span class="checkmark-yellow"></span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="modal-primary-btn">
                <a id="modalRTConf" href="#" class="btn-md-white-secundary fit-content confirmar-modal">confirmar</a>
            </div>
        </div>
    </div>

    <!-- Modal de confirmar pago -->
    <div id="modalPago" class="modal-primary">

        <div class="modal-content-primary">
            <span class="close-primary close-Tienda">&times;</span>
            <h1>¡Ya casi!</h1>
            <p>Por favor confirma que el costo del producto es correcto.</p>
            <div class="modal-primary-btn">
                <a id="modalPConf" href="#" class="btn-md-white-secundary fit-content">confirmar</a>
            </div>
        </div>
    </div>

    <!-- Modal de agregar etiqueta -->
    <div id="modalEtiqueta" class="modal-primary">

        <div class="modal-content-blue">
            <span id="close-modal-etiqueta" class="close-purple">&times;</span>
            <h2>Cantidad de envolturas: </h2>
            <input type="number" class="recoleccion-content--form__big white" placeholder="1" required>
            <h1>Agrega un mensaje a tu tarjeta: </h1>
            <textarea cols="80" rows="6" placeholder="escribe tu mensaje"></textarea>
            <div class="modal-blue-btn">
                <a id="modalPSi" href="#" class="btn-md-white fit-content">confirmar</a>
            </div>
        </div>
    </div>

    <!-- Modal de Globos -->
    <div id="modalGlobos" class="modal-primary">

        <div class="modal-content-secundary">
            <span id="modalGlobos-close" class="close-primary">&times;</span>
            <form>
                <h1>Cantidad de globos: </h1>
                <input type="number" class="recoleccion-content--form__big secundary" placeholder="10 globos" required>

                <h1>Agrega un mensaje en tus globos: </h1>
                <textarea class="secundary" cols="100" rows="6" placeholder="escribe tu mensaje"></textarea>
            </form>
            <div class="modal-blue-btn">
                <a id="modalGconf" href="#" class="btn-md-white-secundary fit-content">confirmar</a>
            </div>
        </div>
    </div>

    <header class="banner" id="header">
        <div class="banner-left">
            <div class="media">
                <p>¡Únete!</p>
                <div class="icon">
                    <a href="https://www.instagram.com/regalandia.mx/"><img src="img/icon/icon-instagram.svg" alt="Regalandia Instagram"></a>
                </div>
                <div class="icon">
                    <a href=""><img src="img/icon/icon-facebook.svg" alt="Regalandia Facebook"></a>
                </div>
            </div>
        </div>
        <div class="logo">
            <a href="index.html"><img src="img/R + moño/6.svg" alt="Regalandia"></a>
        </div>
        <div class="banner-right">
            <a href="carrito.html" class="banner-cart">
                <div class="icon cart">
                    <img src="img/icon/shopping.svg" alt="carrito">
                    <span class="cart-items">0</span>
                </div>
                <p><span>$0.00</span></p>
            </a>
        </div>
    </header>

    <nav class="topnav " id="myTopnav">
        <a href="index.html"></a>
        <a href="tienda.html">Tienda</a>
        <a href="entregas.html" class="activeNav">Entregas</a>
        <a href="tracking.html">Rastreo</a>
        <a href="contacto.html">Contacto</a>
        <a href="nosotros.html">Nosotros</a>
        <a id="responsiveNavbar" href="javascript:void(0);" class="icon">&#9776;</a>
    </nav>

    <!-- Pasos -->
    <div id="menuPasos" class="pasos-menu">
        <div class="pasos-container container ">
            <div class="pasos">
                <div id="barra" class="pasoBarra paso1Barra"></div>
                <div id="step1" class="pasoCirculo paso1 activeStep">1</div>
                <div id="step2" class="pasoCirculo paso2 unactiveStep">2</div>
                <div id="step3" class="pasoCirculo paso3 unactiveStep">3</div>
                <div id="step4" class="pasoCirculo paso4 unactiveStep">4</div>
                <div id="step5" class="pasoCirculo paso5 unactiveStep">5</div>
            </div>


        </div>
    </div>
    <!-- Paso 1 -->
    <div id="paso1">
        <div id="paso1Title" class="title-white">

            <div class="title-step title-step--secundary">
                <div class="title-step__step title-step__step--secundary">1</div>
                Solicita tu recolección
            </div>
        </div>
        <section id="paso1Botones" class="entregas">
            <div class="btn-container">
                <div id="paso1Stopper" class="entregas-card">
                    <div class="entregas-card--img-container">
                        <img class="entregas-card--img" src="img/icon/house.svg" alt="entregas">
                    </div>
                    <div class="entregas-card--btn">
                        <a id="button-casa" href="#" class="btn-md-secundary fit-content">en casa</a>
                    </div>
                </div>
                <div class="entregas-card">
                    <div class="entregas-card--img-container">
                        <img class="entregas-card--img" src="img/icon/store.svg" alt="entregas">
                    </div>
                    <div class="entregas-card--btn">
                        <a id="button-tienda" href="#" class="btn-md-secundary fit-content">en tienda</a>
                    </div>
                </div>
            </div>

        </section>


    <!-- Paso 1: casa 1 regalo-->
    <section id="paso1Casa1" class="recoleccion-form pasosCasa">
        <div class="recoleccion-title">
            <div class="recoleccion-title--img-container">
                <img class="recoleccion-title--img" src="img/icon/house-white.svg" alt="recoleccion en casa">
            </div>
            <div class="recoleccion-title--text">
                <h1>recolección en casa</h1>
            </div>
        </div>

        <div class="recoleccion-content" >
            <div class="container-form-recoleccion">
                <div class="item-form-recoleccion">
                    <p class="recoleccion-content--title">Nombre del regalo:</p>
                    <form action="" class="recoleccion-content--form">
                        <div class="recoleccion-content--form--1cols">
                            <textarea id="nombre1RegaloCasa" class="recoleccion-content--form__md" placeholder="Nombre" cols="30" rows="5"></textarea>
                        </div>
                    </form>
                    <p class="recoleccion-content--title">Tamaño del regalo:</p>
                    <div id="tamano1RegaloCasa" class="recoleccion-content--sizes">
                        <div class="recoleccion-content--radios">
                            <div class="recoleccion-content--imgs">
                                <img src="img/icon/box--white.svg" alt="Tamaño especial" class="recoleccion-content--imgs__sm">
                            </div>
                            <div class="text-primary recoleccion-content--radiosText">
                                especial
                            </div>
                            <div class="recoleccion-content--radiosText recoleccion-content--radiosText__margin">
                                <label class="radio radio__primary">
                                    <input type="radio" name="radio" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="infoButton"></div>
                        <div class="recoleccion-content--radios">
                            <div class="recoleccion-content--imgs">
                                <img src="img/icon/box--white.svg" alt="Tamaño divertido" class="recoleccion-content--imgs__md">
                            </div>
                            <div class="text-primary recoleccion-content--radiosText">
                                divertido
                            </div>
                            <div class="recoleccion-content--radiosText recoleccion-content--radiosText__margin">
                                <label class="radio radio__primary">
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="infoButton"></div>
                        <div class="recoleccion-content--radios">
                            <div class="recoleccion-content--imgs">
                                <img src="img/icon/box--white.svg" alt="Tamaño en grande" class="recoleccion-content--imgs__lg">
                            </div>
                            <div class="text-primary recoleccion-content--radiosText">
                                en grande
                            </div>
                            <div class="recoleccion-content--radiosText recoleccion-content--radiosText__margin">
                                <label class="radio radio__primary">
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="infoButton"></div>
                    </div>
                    <p class="recoleccion-content--title">Día y hora de recolección:</p>
                    <div  class="pr-0 recoleccion-content--form">
                        <div class="container-time container-time__row">
                            <div class="recoleccion-content--form--1cols">
                                <div class="date-picker" id="fechaCasa1Regalo">
                                    <div class="input">
                                        <div class="result">Fecha <span></span></div>
                                        <button><i class="fa fa-calendar"></i></button>
                                    </div>
                                    <div class="calendar"></div>
                                </div>
                            </div>
                            <p class="recoleccion-content--title recoleccion-content--title__xs">Hora</p>
                            <div class="recoleccion-content--form--1cols">
                                <select id="horariosCasa1Regalo" class="recoleccion-content--form__bg primary input-hora input-hora__md text-primary">
                                    <option value="#">8:00AM - 11:00AM</option>
                                    <option value="#">11:00AM - 2:00PM</option>
                                    <option value="#">2:00PM - 5:00PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item-form-recoleccion">
                    <p class="recoleccion-content--title">Domicilio para recolección:</p>
                    <form action="" class="recoleccion-content--form">
                        <div class="recoleccion-content--form--1cols">
                            <input id="calleCasa1Regalo" type="text" class="recoleccion-content--form__big primary mr-1" placeholder="Calle">
                            <input id="numeroCasa1Regalo" type="text" class="recoleccion-content--form__big primary" placeholder="Número">
                        </div>
                        <div class="recoleccion-content--form--1cols">
                            <input id="coloniaCasa1Regalo" type="text" class="recoleccion-content--form__big primary" placeholder="Colonia">
                        </div>
                        <div class="recoleccion-content--form--1cols">
                            <input id="ciudadCasa1Regalo" type="text" class="recoleccion-content--form__big primary mr-1" placeholder="Ciudad">
                            <input id="cpCasa1Regalo" type="number" class="recoleccion-content--form__big primary" placeholder="Código postal">
                        </div>
                        <div class="recoleccion-content--form--1cols">
                            <input id="estadoCasa1Regalo" type="text" class="recoleccion-content--form__big primary mr-1" placeholder="Estado">
                            <input  type="text" class="recoleccion-content--form__big primary" style="visibility: hidden;" placeholder="Estado">
                        </div>
                        <div class="recoleccion-content--form--1cols">
                            <input id="refCasa1Regalo" type="text" class="recoleccion-content--form__big primary" placeholder="Punto de referencia">
                        </div>
                    </form>
                    <div class="recoleccion-footer">
                        <a href="#" class="btn-md-white fit-content paso1CNext" id="paso1CNext">confirmar</a>
                        <div class="validation-box"></div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Paso 1: casa varios regalos, misma dirección-->
    <section id="paso1CasaV1D" class="recoleccion-form pasosCasa">
        <div class="recoleccion-title">
            <div class="recoleccion-title--img-container">
                <img class="recoleccion-title--img" src="img/icon/house-white.svg" alt="recoleccion en casa">
            </div>
            <div class="recoleccion-title--text">
                <h1>recolección en casa</h1>
            </div>
        </div>

        <div class="recoleccion-content" >
            <p class=" text-white">Todos los regalos serán recolectados en el mismo domicilio, completa la información:
            </p>


            <div class="recoleccion-content--form--2cols">

                <div class="recoleccion-content--form--2sections">
                    <p class="recoleccion-content--title recoleccion-content--title__pt recoleccion-content--title__sm">Domicilio para recolección:</p>
                    <div class="recoleccion-content--form">
                        <div class="recoleccion-content--form--1cols">
                            <input type="text" class="recoleccion-content--form__big primary mr-1 inputCalle" placeholder="Calle">
                            <input type="text" class="recoleccion-content--form__big primary inputNumero" placeholder="Número">
                        </div>
                        <div class="recoleccion-content--form--1cols">
                            <input type="text" class="recoleccion-content--form__big primary inputColonia" placeholder="Colonia">
                        </div>
                        <div class="recoleccion-content--form--1cols">
                            <input type="text" class="recoleccion-content--form__big primary mr-1 inputCiudad" placeholder="Ciudad">
                            <input type="number" class="recoleccion-content--form__big primary inputCP" placeholder="Código postal">
                        </div>
                        <div class="recoleccion-content--form--1cols">
                            <input type="text" class="recoleccion-content--form__big primary mr-1 inputEstado" placeholder="Estado">
                            <input type="text" class="recoleccion-content--form__big primary" style="visibility: hidden;" placeholder="Estado">
                        </div>
                        <div class="recoleccion-content--form--1cols">
                            <input type="text" class="recoleccion-content--form__big primary inputReferencia" placeholder="Punto de referencia">
                        </div>
                        <div class="recoleccion-content--form--1cols">

                        </div>
                    </div>
                </div>
                <div class="recoleccion-content--form--2sections">
                    <p class="recoleccion-content--title recoleccion-content--title__pt recoleccion-content--title__sm">Día y hora de recolección:</p>
                    <div class="recoleccion-content--form">
                        <div class="container-time">
                            <div class="recoleccion-content--form--1cols">
                                <div class="date-picker">
                                    <div class="input">
                                        <div class="result">Fecha 1 <span></span></div>
                                        <button><i class="fa fa-calendar"></i></button>
                                    </div>
                                    <div class="calendar"></div>
                                </div>

                            </div>
                            <div class="recoleccion-content--form--1cols">
                                <div style="width: 100%">
                                    <p class="recoleccion-content--title recoleccion-content--title__xs">Hora</p>
                                    <div class="recoleccion-content--form--1cols">
                                        <select name="tiendas" id="horarios0" class="recoleccion-content--form__big primary input-hora input-hora__md secundary text-primary">
                                            <option value="#">8:00AM - 11:00AM</option>
                                            <option value="#">11:00AM - 2:00PM</option>
                                            <option value="#">2:00PM - 5:00PM</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <p class="pt-1 text-white">Agrega una breve descripción de los regalos:</p>
            <div id="contenedorRegalosMD-casa" class="container-entregas"></div>

        </div>
        <div class="recoleccion-footer recoleccion-footer__right">

            <a href="#" class="btn-md-white fit-content mr-2 paso1CNext" id="paso1Next1D">confirmar</a>

            <div class="validation-box validation-box--2"></div>
        </div>
    </section>
    <!-- Paso 1: casa varios regalos, diferentes direcciones-->
    <section id="paso1CasaVVD" class="recoleccion-form pasosCasa">
        <div class="recoleccion-title">
            <div class="recoleccion-title--img-container">
                <img class="recoleccion-title--img" src="img/icon/house-white.svg" alt="recoleccion en casa">
            </div>
            <div class="recoleccion-title--text">
                <h1>recolección en casa</h1>
            </div>
        </div>

        <div class="recoleccion-content" id="recoleccion-content">
            <p class=" text-white">¿Dónde haremos las recolecciones? Completa la información correspondiente a cada regalo:
            </p>
            <div id="contenedorRegalosDD-casa" class="container-entregas"></div>
        </div>
        <div class="recoleccion-footer recoleccion-footer__right">
            <a href="#" class="btn-md-white fit-content mr-2 paso1CNext" id="paso1NextVD">confirmar</a>

        </div>
    </section>
    <!-- Paso 1: tienda 1 regalo -->
    <section id="paso1Tienda1" class="recoleccion-form pasosTienda">
        <div class="recoleccion-title">
            <div class="recoleccion-title--img-container">
                <img class="recoleccion-title--img" src="img/icon/store-white.svg" alt="recoleccion en tienda">
            </div>
            <div class="recoleccion-title--text">
                <h1>recolección en tienda</h1>
            </div>
        </div>
        <div class="recoleccion-content">
            <div class="recoleccion-content--group">

                <div class="recoleccion-content--group__set ">
                    <p class="recoleccion-content--title recoleccion-content--title__sm">Nombre del regalo:</p>
                    <form action="" class="recoleccion-content--form">

                        <div class="recoleccion-content--form--1cols">
                            <textarea class="recoleccion-content--form__bg recoleccion-content--form__margin inputNombre" placeholder="Nombre" rows="4"></textarea>
                        </div>
                    </form>
                    <p class="recoleccion-content--title recoleccion-content--title__sm">Tamaño del regalo:</p>
                    <div  class="recoleccion-content--sizes inputTamano">
                        <div class="recoleccion-content--radios ">
                            <div class="recoleccion-content--imgs recoleccion-content--imgs__imgsm">
                                <img src="img/icon/box--white.svg" alt="Tamaño especial" class="recoleccion-content--imgs__xsm">
                            </div>
                            <div class="text-primary recoleccion-content--radiosText recoleccion-content--radiosText__sm">
                                especial
                            </div>
                            <div class="recoleccion-content--radiosText recoleccion-content--radiosText__margin">
                                <label class="radio radio__primary">
                                        <input type="radio" name="radio" checked="checked">
                                        <span class="checkmark"></span>
                                    </label>
                            </div>
                        </div>
                        <div class="infoButton"></div>
                        <div class="recoleccion-content--radios">
                            <div class="recoleccion-content--imgs recoleccion-content--imgs__imgsm">
                                <img src="img/icon/box--white.svg" alt="Tamaño divertido" class="recoleccion-content--imgs__xmd">
                            </div>
                            <div class="text-primary recoleccion-content--radiosText recoleccion-content--radiosText__sm">
                                divertido
                            </div>
                            <div class="recoleccion-content--radiosText recoleccion-content--radiosText__margin">
                                <label class="radio radio__primary">
                                        <input type="radio" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                            </div>
                        </div>
                        <div class="infoButton"></div>
                        <div class="recoleccion-content--radios">
                            <div class="recoleccion-content--imgs recoleccion-content--imgs__imgsm">
                                <img src="img/icon/box--white.svg" alt="Tamaño en grande" class="recoleccion-content--imgs__xlg">
                            </div>
                            <div class="text-primary recoleccion-content--radiosText recoleccion-content--radiosText__sm">
                                en grande
                            </div>
                            <div class="recoleccion-content--radiosText recoleccion-content--radiosText__margin">
                                <label class="radio radio__primary">
                                        <input type="radio" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                            </div>
                        </div>
                        <div class="infoButton"></div>
                    </form>
                </div>



                <div class="recoleccion-content--group__set">
                    <p class="recoleccion-content--title recoleccion-content--title__sm">¿Dónde encontrarlo?</p>
                    <form action="" class="recoleccion-content--form">
                        <div class="recoleccion-content--form--1cols">
                            <select name="tiendas" id="tiendas-sugeridas-select" class="recoleccion-content--form__bg secundary text-primary">
                                <option value="default">Tiendas sugeridas</option>
                                <option value="#">Liverpool</option>
                                <option value="#">Best Buy</option>
                                <option value="#">Julio Cepeda</option>
                                <option value="#">Sears</option>
                                <option value="#">Walmart</option>
                                <option value="#">Palacio de Hierro</option>
                                <option value="#">HEB</option>

                            </select>

                        </div>
                        <div id="otherStore" class="recoleccion-content--form--1cols">
                            <input type="text" class="recoleccion-content--form__bg primary" placeholder="Ingrese la tienda">
                        </div>
                    </form>
                    <p class="recoleccion-content--title recoleccion-content--title__sm">Link del producto</p>
                    <form action="" class="recoleccion-content--form">
                        <div class="recoleccion-content--form--1cols">
                            <input type="text" class="recoleccion-content--form__bg primary inputLink" placeholder="http://">
                        </div>
                    </form>
                    <p class="recoleccion-content--title recoleccion-content--title__sm">Precio</p>
                    <form action="" class="recoleccion-content--form">
                        <div class="recoleccion-content--form--1cols">
                            <input type="text" class="recoleccion-content--form__bg primary inputPrecio" placeholder="$">
                        </div>
                    </form>
                    <form action="" class="recoleccion-content--form">
                        <div class="recoleccion-content--form--1cols">
                            <label class="checkbox-container-primary text-sm">Confirmo que el producto se encuentra
                                disponible en tienda
                                <input type="checkbox" checked="checked">
                                <span class="checkbox-checkmark"></span>
                            </label>
                        </div>
                    </form>

                </div>



            </div>
            <div class="recoleccion-content--multiregalos"></div>
        </div>
        </div>
        <div class="recoleccion-footer">
            <a id="confirmarViaTienda1"  href="#" class="btn-md-white fit-content btn--right" >confirmar</a>
        </div>
    </section>

    <!-- Paso 1: tienda varios regalos, misma tienda -->
    <section id="paso1TiendaV1D" class="recoleccion-form pasosTienda">
        <div class="recoleccion-title">
            <div class="recoleccion-title--img-container">
                <img class="recoleccion-title--img" src="img/icon/store-white.svg" alt="recoleccion en tienda">
            </div>
            <div class="recoleccion-title--text">
                <h1>recolección en tienda</h1>
            </div>
        </div>
        <div class="recoleccion-content">
            <p class="mb-1 text-white">¿Dónde haremos las recolecciones? Completa la información correspondiente a cada regalo:
            </p>
            <div class="recoleccion-content--group recoleccion-content--group__cols">
                <div class="recoleccion-content--group__set">

                    <p class="recoleccion-content--title">¿Dónde encontrarlos?</p>
                    <form action="" class="recoleccion-content--form">

                        <div class="recoleccion-content--form--1cols">

                            <select name="tiendas" id="tiendas-sugeridas-V" class="recoleccion-content--form__bg secundary text-primary" >
                                <option value="#">Tiendas sugeridas</option>
                                <option value="#">Tienda 1</option>
                                <option value="#">Tienda 2</option>
                                <option value="other">Otra</option>
                            </select>

                        </div>

                    </form>
                </div>

                <div id="contenedorRegalosMD-tienda" class="container-entregas"></div>


            </div>
            <div class="recoleccion-content--multiregalos"></div>
        </div>

        <div class="recoleccion-footer recoleccion-footer__right">
            <a id="confirmarViaTienda2" href="#" class="btn-md-white fit-content mr-2" >confirmar</a>

        </div>
    </section>
    <!-- Paso 1: tienda varios regalos, diferentes tienda -->
    <section id="paso1TiendaVVD" class="recoleccion-form pasosTienda">
        <div class="recoleccion-title">
            <div class="recoleccion-title--img-container">
                <img class="recoleccion-title--img" src="img/icon/store-white.svg" alt="recoleccion en tienda">
            </div>
            <div class="recoleccion-title--text">
                <h1>recolección en tienda</h1>
            </div>
        </div>
        <div class="recoleccion-content">
            <p class="mb-1 text-white">¿Dónde haremos las recolecciones? Completa la información correspondiente a cada regalo:
            </p>
            <div class="recoleccion-content--group">
                <div id="contenedorRegalosDD-tienda" class="container-entregas"></div>
            </div>
            <div class="recoleccion-content--multiregalos"></div>
        </div>

        <div class="recoleccion-footer recoleccion-footer__right">
            <a id="paso1TNext" href="#" class="btn-md-white fit-content mr-2 paso1CNext">confirmar</a>

        </div>
    </section>

    </div>
    <!-- Paso 2 -->
    <div id="paso2">
    <div id="paso2Title" class="title-white">

        <div class="title-step title-step--blue">
            <div class="title-step__step title-step__step--blue">2</div>
            Selecciona tu envoltura
        </div>
    </div>
    <h3 class="text-primary unactiveChoice" style="margin-left:2em">Selecciona la envoltura de tu preferencia para cada regalo:
    </h3>

    <section id="paso2Botones" class="entregas">

        <div class="container">
            <div class="carrito">
                <div class="carrito-title">
                    <p class="text-primary">Selecciona la envoltura de tu preferencia para cada regalo:</p>
                </div>
                <div class="container-table" id="container-regalos">

                </div>
                <div class="btnCarrito">
                    <a id="conf-tabla-paso2" class="btn-md-blue">confirmar</a>
                </div>
            </div>
        </div>
    </section>




</div>

    <!-- Paso 3 -->
    <section id="paso3" class="globos ">
        <div class="title-white">

            <div class="title-step title-step--violet">
                <div class="title-step__step title-step__step--violet">3</div>
                Agrega globos
            </div>
        </div>
        <div class="entregas-header">
            <p class="entregas-header__title">¡Cuidemos juntos el medio ambiente! Nuestros globos tienen cualidades biodegradables
            </p>
        </div>
        <div class="globos-title">
            <div class="globos-title--img-container">
                <img class="globos-title--img" src="img/icon/balloons-violet.svg" alt="globos">
            </div>
            <div class="globos-title--text">
                <h1>agrega globos a tu regalo (opcional)</h1>
            </div>
        </div>
        <div class="boxes-grid boxes-grid--globos">


        </div>
        <div class="globos--footer">
            <a id="paso3Next" href="#" class="btn-md-secundary fit-content">confirmar</a>
        </div>
    </section>
    <!-- Paso 4 -->
    <section id="paso4" class="recoleccion-form envio-pago ">
        <div class="title-white">

            <div class="title-step title-step--blue">
                <div class="title-step__step title-step__step--blue">4</div>
                Envío
            </div>
        </div>
        <div class="recoleccion-content">
            <p class="recoleccion-content--title">datos de envío</p>
            <form action="" class="recoleccion-content--form">
                <div class="recoleccion-content--form--1cols">
                    <select name="zona" id="zona-envio" class="recoleccion-content--form__big primary envio--input">
                        <option value="volvo">Selecciona zona de envío</option>
                        <option value="saab">Saab</option>
                    </select>
                </div>
            </form>
            <p class="recoleccion-content--title">dirección de entrega</p>
            <form action="" class="recoleccion-content--form">

                <div class="recoleccion-content--form--2cols">
                    <input type="text" class="recoleccion-content--form__md primary envio--input" placeholder="Ciudad" id="city">
                    <input type="number" class="recoleccion-content--form__md primary envio--input" placeholder="Código Postal" id="zip">
                </div>
                <div class="recoleccion-content--form--1cols">
                    <input type="text" class="recoleccion-content--form__big primary envio--input" placeholder="casa, departamento, oficina, salón eventos">
                </div>
            </form>
            <p class="recoleccion-content--title">recibe</p>
            <form action="" class="recoleccion-content--form">
                <div class="recoleccion-content--form--2cols">
                    <input type="text" class="recoleccion-content--form__md primary envio--input" placeholder="Nombre">
                    <input type="text" class="recoleccion-content--form__md primary envio--input" placeholder="Apellido">
                </div>
                <div class="recoleccion-content--form--2cols">
                    <input type="text" class="recoleccion-content--form__md primary envio--input" placeholder="Teléfono">
                    <input type="text" class="recoleccion-content--form__md primary envio--input" placeholder="Correo electrónico">
                </div>
            </form>
        </div>
        <div class="recoleccion-footer recoleccion-footer__right">
            <a id="paso4Next" href="#" class="btn-md-secundary fit-content">confirmar</a>
        </div>
    </section>
    <!-- Paso 5: En casa-->
    <section id="paso5C" class="envio-pago">
        <div class="title-white">

            <div class="title-step title-step--secundary">
                <div class="title-step__step title-step__step--secundary">5</div>
                Pagar
            </div>
        </div>

        <div class="carrito">
            <div class="pago-title">
                <div class="pago-title--img-container">
                    <img class="pago-title--img" src="img/icon/house.svg" alt="mi carrito">
                </div>
                <div class="pago-title--text">
                    <h1>Recolección</h1>
                </div>
            </div>


            <div class="carrito-container">
                <div class="container-table">
                    <table id="container-table-5C">

                    </table>
                </div>
                <div class="btnCarrito">
                    <a href="finalizar-compra.html" class="btn-md-secundary fit-content">pagar</a>
                </div>
            </div>


        </div>
    </section>
    <!-- Paso 5: En tienda-->
    <section id="paso5T" class="envio-pago">
        <div class="title-white">

            <div class="title-step title-step--secundary">
                <div class="title-step__step title-step__step--secundary">5</div>
                Pagar
            </div>
        </div>

        <div class="carrito">
            <div class="pago-title">
                <div class="pago-title--img-container">
                    <img class="pago-title--img" src="img/icon/store.svg" alt="mi carrito">
                </div>
                <div class="pago-title--text">
                    <h1>elegiste recolección en tienda</h1>
                </div>
            </div>

            <div class="container-table">
                <table id="container-table-5T">

                </table>
            </div>
            <div class="btnCarrito">
                <a href="finalizar-compra.html" class="btn-md-secundary fit-content">pagar</a>
            </div>
        </div>

    </section>

    <footer>
        <div class="left">
            <div class="join">
                <p>¡Únete!</p>
                <div class="icon">
                    <a href=""><img src="img/icon/icon-instagram.svg" alt="Regalandia Instagram"></a>
                </div>
                <div class="icon">
                    <a href=""><img src="img/icon/icon-facebook.svg" alt="Regalandia Facebook"></a>
                </div>
            </div>
            <p>Monterrey, N.L. | correo@electronico.com</p>
            <div class="links">
                <a href="">Nosotros</a>
                <a href="">Entregas</a>
                <a href="">Envolturas</a>
                <a href="">Globos</a>
                <a href="">Contacto</a>
                <a href="">Tienda</a>
            </div>
        </div>
        <div class="right">
            <div class="logo">
                <img src="img/Logotipo/morado.svg" alt="">
            </div>
        </div>
    </footer>

    <script src="js/add-to-cart.js"></script>

    <script src="js/assets/navbar.js"></script>

    <script type="module" src="js/pages/entregas.js"></script>


</body>

</html>
