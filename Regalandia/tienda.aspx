﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tienda.aspx.cs" Inherits="Regalandia.tienda" %>

<!DOCTYPE html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Envolturas</title>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="icon" type="image/png" href="img/favico.png">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@200;300;500;800&display=swap" rel="stylesheet">
</head>

<body>
    <section class="banner" id="header">
        <div class="banner-left">
            <div class="media">
                <p>¡Únete!</p>
                <div class="icon">
                    <a href="https://www.instagram.com/regalandia.mx/"><img src="img/icon/icon-instagram.svg" alt="Regalandia Instagram"></a>
                </div>
                <div class="icon">
                    <a href=""><img src="img/icon/icon-facebook.svg" alt="Regalandia Facebook"></a>
                </div>
            </div>
        </div>
        <div class="logo">
            <a href="index.html"><img src="img/Rmono/6.svg" alt="Regalandia"></a>
        </div>
        <div class="banner-right">
            <a href="carrito.html" class="banner-cart">
                <div class="icon cart">
                    <img src="img/icon/shopping.svg" alt="carrito">
                    <span class="cart-items">0</span>
                </div>
                <p><span>$0.00</span></p>
            </a>
        </div>
    </section>

    <div class="topnav " id="myTopnav">
        <a href="index.html"></a>
        <a href="tienda.html" class="activeNav">Tienda</a>
        <a href="entregas.html">Entregas</a>
        <a href="tracking.html">Rastreo</a>
        <a href="contacto.html">Contacto</a>
        <a href="nosotros.html">Nosotros</a>
        <a id="responsiveNavbar" href="javascript:void(0);" class="icon">&#9776;</a>
    </div>

    <div class="title title--blue">
        envolturas (envíos locales y nacionales)
    </div>

    <section class="envolturas-grid">
        <div class="envoluturas-container">
            <div class="envolturas-product">
                <div class="image">
                    <img src="img/icon/pack-blue.svg" alt="">
                </div>
                <a href="bolsas.html" class="btn-md-blue">bolsas</a>
                <div class="description">
                    <p>Las bolsas incluyen pliegos de papel china </p>
                </div>
            </div>
        </div>
        <div class="envoluturas-container">
            <div class="envolturas-product">
                <div class="image">
                    <img src="img/icon/herramientas.svg" alt="">
                </div>
                <a href="herramientas-envolturas.html" class="btn-md-blue">Herramientas de envoltura</a>
                <div class="description">
                    <p>Elige <strong>cajas decorativas</strong> o <strong>papel</strong> para decorar</p>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="left">
            <div class="join">
                <p>¡Únete!</p>
                <div class="icon">
                    <a href=""><img src="img/icon/icon-instagram.svg" alt="Regalandia Instagram"></a>
                </div>
                <div class="icon">
                    <a href=""><img src="img/icon/icon-facebook.svg" alt="Regalandia Facebook"></a>
                </div>
            </div>
            <p>Monterrey, N.L. | correo@electronico.com</p>
            <div class="links">
                <a href="">Nosotros</a>
                <a href="">Entregas</a>
                <a href="">Globos</a>
                <a href="">Contacto</a>
                <a href="">Tienda</a>
            </div>
        </div>
        <div class="right">
            <div class="logo">
                <img src="img/Logotipo/morado.svg" alt="">
            </div>
        </div>
    </footer>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery/jquery.min.js "></script>
    <script src="js/assets/navbar.js"></script>

    <script>
        document.body.addEventListener('scroll', () => {
            scrollFunction()
        });
        window.onscroll = function() {
            scrollFunction()
        };
        var nav = $("#myTopnav");

        function scrollFunction() {
            var bannerH = document.getElementById('header').getBoundingClientRect().height;
            if (document.body.scrollTop > bannerH || document.documentElement.scrollTop > bannerH) {
                nav.addClass("fixed-topnav");
            } else {
                nav.removeClass("fixed-topnav");
            }
        }
    </script>
</body>

</html>
