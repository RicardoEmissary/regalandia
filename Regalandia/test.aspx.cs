﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using AutoMapper;
using Newtonsoft.Json;
using Regalandia.app.clasesDto;
using Regalandia.app.controllers;

namespace Regalandia
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*OrdenDto ordenDto = new OrdenDto();
            
            ordenDto.cliente.nombre = "Ricardo";
            ordenDto.cliente.telefono = "8181815462";
            ordenDto.cliente.correo = "ricardo@correo.com";

            ProductoGeneralDto productoGeneralDto = new ProductoGeneralDto();
            productoGeneralDto.idProducto

            ordenDto.productosGenerales.Add();*/


        }

        [WebMethod(EnableSession = true)]
        public static string insertGloboSeleccionado([FromBody]OrdenDto ordenData)
        {
            clienteController clieController = new clienteController();
            ordenController ordController = new ordenController();
            productoSeleccionadoController prodController = new productoSeleccionadoController();
            direccionController dirController = new direccionController();
            productoColorSeleccionadoController prodColController = new productoColorSeleccionadoController();
            globoColorSeleccionadoController colorSeleController = new globoColorSeleccionadoController();
            try
            {

                Cliente clienteBD = clieController.Post(clieController.ProfileMap(ordenData.cliente));
                
                Orden orden = ordController.Post(ordController.Set(0, clienteBD.id_Cliente));
                if(ordenData.productosGenerales != null)
                {
                    foreach (ProductoGeneralDto e in ordenData.productosGenerales)
                    {
                        Direccion direccion = dirController.Post(dirController.ProfileMap(e.direccion));

                        ProductoSeleccionado productoSeleccionado = prodController.ProfileMap(e);
                        productoSeleccionado.id_DireccionDestinatario = direccion.id_Direccion;
                        productoSeleccionado = prodController.Post(productoSeleccionado);
                        
                        foreach(string color in e.colores)
                        {
                            prodColController.Post(productoSeleccionado.id_ProductoSeleccionado, color);
                        }

                        orden.total_Orden = orden.total_Orden + productoSeleccionado.precio_ProductoSeleccionado;
                    }
                }
                if(ordenData.setGlobos != null)
                {
                    foreach(ProductoGloboDto e in ordenData.setGlobos)
                    {
                        Direccion direccion = dirController.Post(dirController.ProfileMap(e.direccion));

                        ProductoSeleccionado globoSeleccionado = prodController.ProfileMap(e);
                        globoSeleccionado.id_DireccionDestinatario = direccion.id_Direccion;
                        globoSeleccionado = prodController.Post(globoSeleccionado);

                        //HAY QUE AGREGAR LOS COLORES PARA LOS GLOBOS
                        foreach(GloboColorDto globocolor in e.globos)
                        {
                            foreach(string color in globocolor.colores)
                            {
                                colorSeleController.Post(globoSeleccionado.id_ProductoSeleccionado, color, globocolor.idGlobo);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(new
                {
                    status = false,
                    message = "Ocurrio un error al guardar el producto"
                });
            }
            return JsonConvert.SerializeObject(new
            {
                status = true,
                message = "Guardado con exito"
            });
        }
    }
}