﻿using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia
{
    public partial class entregas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var bolsas = getBolsas();
        }

        //Obtenemos todas las bolsas habilitadas
        [WebMethod(EnableSession = true)]
        public static Object getBolsas()
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    return controller.getAllProductosByTipo(2);
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //Obtenemos todas las envolturas habilitadas
        [WebMethod(EnableSession = true)]
        public static Object getEnvolturas()
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    return controller.getAllProductosByTipo(1);
                }
                catch(Exception E)
                {
                    return E;
                }
            }
        }

        //Obtenemos todas las bolsas y envolturas habilitada
        [WebMethod(EnableSession = true)]
        public static Object getAllProductos()
        {
            using(productoController controller = new productoController())
            {
                try
                {
                    return controller.getAllProductos();
                }
                catch(Exception E)
                {
                    return E;
                }
            }
        }

        //Obtenemos todos los globos 
        [WebMethod(EnableSession = true)]
        public static Object getGlobos()
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    return controller.getAllProductosByTipo(4);
                }
                catch(Exception E)
                {
                    return E;
                }
            }
        }

        //Obtenemos los globos que pertenecen al producto de tipo globo
        [WebMethod(EnableSession = true)]
        public static Object getProductoGlobo()
        {
            using (productoGloboController controller = new productoGloboController())
            {
                try
                {
                    return controller.GetProductoGlobo();
                }
                catch(Exception E)
                {
                    return E;
                }
            }
        }

        //Obtenemos los colores de cada globo individual
        [WebMethod(EnableSession = true)]
        public static Object getGloboColor()
        {
            using (globoColorController controller = new globoColorController())
            {
                try
                {
                    return controller.GetGloboColor();
                }
                catch(Exception E)
                {
                    return E;
                }
            }
        }
    }
}