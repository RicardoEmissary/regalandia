﻿using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.client
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        //Obtener los regalos mas nuevos
        [WebMethod(EnableSession = true)]
        public static Object getNuevosProductos()
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    return controller.getNuevosProductos();
                }
                catch(Exception E)
                {
                    return E;
                }
            }
        }

        //Obtenemos los colores registrados para los regalos
        [WebMethod(EnableSession = true)]
        public static Object getColoresRegalos()
        {
            using (productoColorController controller = new productoColorController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //Obtenemos todas las frases registradas
        [WebMethod(EnableSession = true)]
        public static Object getFrases()
        {
            using (frasesController controller = new frasesController())
            {
                try
                {
                    return controller.GetFrasesEnabled();
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

    }
}