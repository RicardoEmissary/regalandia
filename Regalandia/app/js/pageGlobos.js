﻿let globosArray = [];
let categoriasArray = [];
let IDGloboSelected;
let productoGloboArray = [];
let IDSetSelected;
let productosArray = [];

//INPUTS Producto
const txtNombreProducto = $('#txtNombreProducto');
const txtDescripcionProducto = $('#txtDescripcionProducto');
const txtPrecioProducto = $('#txtPrecioProducto');
const txtSKU = $('#txtSKU');
const dpCategorias = $('#dpCategorias');
const fileImg1 = $('#fileImg1');
const fileImg2 = $('#fileImg2');
const fileImg3 = $('#fileImg3');
const fileImg4 = $('#fileImg4');
const oldNameImg1 = $('#nameImage1');
const oldNameImg2 = $('#nameImage2');
const oldNameImg3 = $('#nameImage3');
const oldNameImg4 = $('#nameImage4');
const chkHabilitadoProducto = $('#chkHabilitadoProducto');
const btnAgregarProd = $('#btnAgregarProd');
const btnActualizarProd = $('#btnActualizarProd');
const modalSet = $('#ModalSet');
const img1Update = $('#img1Update');
const img2Update = $('#img2Update');
const img3Update = $('#img3Update');
const img4Update = $('#img4Update');

//INPUTS globos
const btnAgregarGlo = $('#btnAgregarGlo');
const btnActualizarGlo = $('#btnActualizarGlo');
const txtNombreGlobo = $('#txtNombreGlobo');
const chkMultiplesColores = $('#chkMultiplesColores');
const ModalGlobo = $('#ModalGlobo');

let dataImg1; let dataImg2; let dataImg3; let dataImg4;
let base64Img1; let base64Img2; let base64Img3; let base64Img4;

$(function () {
    initGlobos();
    initColores();
    initSet();
    initCategorias();
});

//GENERIC AJAX
function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

function initSet() {
    const response = ajaxRequest("pageGlobos.aspx/GetSetGlobos", "{}", "POST");
    if (response == null) {
        alert("No se encontraron set's en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableSet").DataTable();
    table.clear();
    productosArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let productos = {
            0: this.id_Producto,
            1: this.nombre_Producto.trim(),
            2: this.precio_Producto,
            3: this.nombre_ProductoTipo,
            4: (this.habilitado_Producto == true) ? 'Si' : 'No',
            5: `<button type="button" onclick="showModalActualizarSet(${this.id_Producto})" class="btn btn-primary" data-toggle="modal" data-target="#ModalSet">Editar</button>
                <button type="button" onclick="habilitarSet(${this.id_Producto})" class="btn btn-primary">Habilitar</button>`
        }
        objectArray.push(productos);
    });
    table.rows.add(objectArray).draw();
    initNumsGlobos();
    initProductoGlobo();
}

function initNumsGlobos() {
    $('#chksAreaGlobos').empty();
    globosArray.forEach(function (e) {
        $('#chksAreaGlobos').append(`<div class="form-group col-3" id="form${e.id_Globo}">
                                        <label for="numGlobo${e.id_Globo}">${e.nombre_Globo}</label>
                                        <input type="number" value=0 class="form-control" id="numGlobo${e.id_Globo}"/>
                                    </div>`);
    });
}

//MOSTRAR MODAL PARA AGREGAR PRODUCTO
$('#btnAgregarSet').click(function () {
    $('.modal-title').html('Agregar Producto');
    txtNombreProducto.val('');
    txtSKU.val('');
    txtDescripcionProducto.val('');
    txtPrecioProducto.val('');
    fileImg1.val('');
    fileImg2.val('');
    fileImg3.val('');
    fileImg4.val('');
    img1Update.attr('src', '');
    img2Update.attr('src', '');
    img3Update.attr('src', '');
    img4Update.attr('src', '');
    chkHabilitadoProducto.prop('checked', false);
    btnAgregarProd.css('display', 'block');
    btnActualizarProd.css('display', 'none');
});

//INSERTAR SET NUEVO
btnAgregarProd.click(function () {

    let habilitado = chkHabilitadoProducto.is(":checked") ? true : false;
    let nameImg1 = fileImg1.val().replace(/C:\\fakepath\\/i, '');
    nameImg1 = (nameImg1 == '') ? 'no image' : nameImg1;
    let nameImg2 = fileImg2.val().replace(/C:\\fakepath\\/i, '');
    nameImg2 = (nameImg2 == '') ? 'no image' : nameImg2;
    let nameImg3 = fileImg3.val().replace(/C:\\fakepath\\/i, '');
    nameImg3 = (nameImg3 == '') ? 'no image' : nameImg3;
    let nameImg4 = fileImg4.val().replace(/C:\\fakepath\\/i, '');
    nameImg4 = (nameImg4 == '') ? 'no image' : nameImg4;

    const Object = {
        id: 0,
        nombre: txtNombreProducto.val(),
        descripcion: txtDescripcionProducto.val(),
        precio: txtPrecioProducto.val(),
        sku: txtSKU.val(),
        tamanio: null,
        nameImg1: nameImg1,
        nameImg2: nameImg2,
        nameImg3: nameImg3,
        nameImg4: nameImg4,
        oldNameImg1: oldNameImg1.val(),
        oldNameImg2: oldNameImg2.val(),
        oldNameImg3: oldNameImg3.val(),
        oldNameImg4: oldNameImg4.val(),
        base64Img1: base64Img1,
        base64Img2: base64Img2,
        base64Img3: base64Img3,
        base64Img4: base64Img4,
        idCategoria: dpCategorias.val(),
        idTipo: '4',
        habilitado: habilitado
    }

    let arrayProdGlo = [];
    globosArray.forEach(function (e) {
        if ($(`#numGlobo${e.id_Globo}`).val() > 0) {
            arrayProdGlo.push({ id: e.id_Globo, cantidad: $(`#numGlobo${e.id_Globo}`).val() });
        }
    });

    let response = ajaxRequest("pageGlobos.aspx/postSetGlobos", JSON.stringify({ producto: Object, arrayGlobos: arrayProdGlo }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalSet.modal('hide');
    initSet();
    
});

//MOSTRAR MODAL PARA ACTUALIZAR PRODUCTOS
function showModalActualizarSet(id) {
    IDSetSelected = id;
    const info = productosArray.find(X => X.id_Producto == IDSetSelected);
    $('.modal-title').html('Editar Producto');
    txtNombreProducto.val(info.nombre_Producto);
    txtSKU.val(info.sku_Producto);
    txtPrecioProducto.val(info.precio_Producto);
    txtDescripcionProducto.val(info.descripcion_Producto);
    if (info.foto1_Producto != null) {
        oldNameImg1.val(info.foto1_Producto);
        $('#imgPreview1 img').attr('src', 'multimedia/' + info.foto1_Producto);
    }
    else {
        oldNameImg1.val('');
        $('#imgPreview1 img').attr('src', '');
    }
    if (info.foto2_Producto != null) {
        oldNameImg2.val(info.foto2_Producto);
        $('#imgPreview2 img').attr('src', 'multimedia/' + info.foto2_Producto);
    }
    else {
        oldNameImg2.val('');
        $('#imgPreview2 img').attr('src', '');
    }
    if (info.foto3_Producto != null) {
        oldNameImg3.val(info.foto3_Producto);
        $('#imgPreview3 img').attr('src', 'multimedia/' + info.foto3_Producto);
    }
    else {
        oldNameImg3.val('');
        $('#imgPreview3 img').attr('src', '');
    }
    if (info.foto4_Producto != null) {
        oldNameImg4.val(info.foto4_Producto);
        $('#imgPreview4 img').attr('src', 'multimedia/' + info.foto4_Producto);
    }
    else {
        oldNameImg4.val('');
        $('#imgPreview4 img').attr('src', '');
    }
    fileImg1.val('');
    fileImg2.val('');
    fileImg3.val('');
    fileImg4.val('');
    dpCategorias.val(info.id_Categoria);
    chkHabilitadoProducto.prop('checked', (info.habilitado_Producto) ? true : false);
    initNumsGlobos();
    let arrayProdGlo = productoGloboArray.filter(X => X.id_Producto == id);
    arrayProdGlo.forEach(function (e) {
        $(`#numGlobo${e.id_Globo}`).val(e.cantidad_ProductoGlobo);
    });

    btnAgregarProd.css('display', 'none');
    btnActualizarProd.css('display', 'block');
}

//UPDATE SET
btnActualizarProd.click(function () {
    let habilitado = chkHabilitadoProducto.is(":checked") ? true : false;
    let nameImg1 = fileImg1.val().replace(/C:\\fakepath\\/i, '');
    nameImg1 = (nameImg1 == '') ? 'no image' : nameImg1;
    let nameImg2 = fileImg2.val().replace(/C:\\fakepath\\/i, '');
    nameImg2 = (nameImg2 == '') ? 'no image' : nameImg2;
    let nameImg3 = fileImg3.val().replace(/C:\\fakepath\\/i, '');
    nameImg3 = (nameImg3 == '') ? 'no image' : nameImg3;
    let nameImg4 = fileImg4.val().replace(/C:\\fakepath\\/i, '');
    nameImg4 = (nameImg4 == '') ? 'no image' : nameImg4;
    const Object = {
        id: IDSetSelected,
        nombre: txtNombreProducto.val(),
        descripcion: txtDescripcionProducto.val(),
        precio: txtPrecioProducto.val(),
        sku: txtSKU.val(),
        tamanio: null,
        nameImg1: nameImg1,
        nameImg2: nameImg2,
        nameImg3: nameImg3,
        nameImg4: nameImg4,
        oldNameImg1: oldNameImg1.val(),
        oldNameImg2: oldNameImg2.val(),
        oldNameImg3: oldNameImg3.val(),
        oldNameImg4: oldNameImg4.val(),
        base64Img1: base64Img1,
        base64Img2: base64Img2,
        base64Img3: base64Img3,
        base64Img4: base64Img4,
        idCategoria: dpCategorias.val(),
        idTipo: '4',
        habilitado: habilitado
    }
    let arrayProdGlo = [];
    globosArray.forEach(function (e) {
        if ($(`#numGlobo${e.id_Globo}`).val() > 0) {
            arrayProdGlo.push({ id: e.id_Globo, cantidad: $(`#numGlobo${e.id_Globo}`).val() });
        }
    });
    let response = ajaxRequest("pageGlobos.aspx/putSetGlobos", JSON.stringify({ producto: Object, arrayGlobos: arrayProdGlo }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalSet.modal('hide');
    initSet();
});

//HABILITAR/DESHABILITAR
function habilitarSet(id) {
    IDSetSelected = id;
    const info = productosArray.find(X => X.id_Producto == IDSetSelected);
    let habilitado = info.habilitado_Producto ? false : true;
    const Object = {
        id: IDSetSelected,
        nombre: info.nombre_Producto,
        descripcion: info.descripcion_Producto,
        sku: info.sku_Producto,
        precio: info.precio_Producto,
        tamanio: info.id_Tamanio,
        oldNameImg1: info.foto1_Producto,
        oldNameImg2: info.foto2_Producto,
        oldNameImg3: info.foto3_Producto,
        oldNameImg4: info.foto4_Producto,
        idCategoria: info.id_Categoria,
        idTipo: info.id_ProductoTipo,
        habilitado: habilitado
    }
    let response = ajaxRequest("pageProductos.aspx/putProductoStatus", JSON.stringify({ producto: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initSet();
}

//CATEGORIAS
function initCategorias() {
    const response = ajaxRequest("pageProductos.aspx/GetCategorias", "{}", "POST");
    if (response == null) {
        alert("No se encontraron categorias en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    categoriasArray = response.d;
    dpCategorias.empty();
    categoriasArray.forEach(e => dpCategorias.append(`<option value=${e.id_Categoria}>${e.nombre_Categoria}</option>`));
}

function initProductoGlobo() {
    const response = ajaxRequest("pageGlobos.aspx/GetProductoGlobo", "{}", "POST");
    if (response == null) {
        alert("No se encontraron productosGlobo en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    productoGloboArray = response.d;
}

// Cargar imagen 1
function previewFile1(event) {
    const file = event.files[0];
    const reader = new FileReader();
    reader.onload = function () {
        let preview = document.getElementById('imgPreview1');
        let image = document.createElement('img');
        image.height = 100;
        image.width = 100
        image.src = reader.result;
        preview.innerHTML = '';
        preview.append(image);
        const base64Data = reader.result.match(/(?<=base64,)(.*)/gim);
        base64Img1 = base64Data[0];
    };
    if (file) {
        reader.readAsDataURL(file);
        console.log(file);
        console.log(reader);
    }
}

// Cargar imagen 2
function previewFile2(event) {
    //const preview = event.parentNode.nextElementSibling.nextElementSibling.children[0].children[0];
    const file = event.files[0];
    const reader = new FileReader();
    reader.onload = function () {
        let preview = document.getElementById('imgPreview2');
        let image = document.createElement('img');
        image.height = 100;
        image.width = 100
        image.src = reader.result;
        preview.innerHTML = '';
        preview.append(image);
        const base64Data = reader.result.match(/(?<=base64,)(.*)/gim);
        base64Img2 = base64Data[0];
    };
    if (file) {
        reader.readAsDataURL(file);
        console.log(file);
        console.log(reader);
    }
}

// Cargar imagen 3
function previewFile3(event) {
    //const preview = event.parentNode.nextElementSibling.nextElementSibling.children[0].children[0];
    const file = event.files[0];
    const reader = new FileReader();
    reader.onload = function () {
        let preview = document.getElementById('imgPreview3');
        let image = document.createElement('img');
        image.height = 100;
        image.width = 100
        image.src = reader.result;
        preview.innerHTML = '';
        preview.append(image);
        const base64Data = reader.result.match(/(?<=base64,)(.*)/gim);
        base64Img3 = base64Data[0];
    };
    if (file) {
        reader.readAsDataURL(file);
        console.log(file);
        console.log(reader);
    }
}

// Cargar imagen 4
function previewFile4(event) {
    //const preview = event.parentNode.nextElementSibling.nextElementSibling.children[0].children[0];
    const file = event.files[0];
    const reader = new FileReader();
    reader.onload = function () {
        let preview = document.getElementById('imgPreview4');
        let image = document.createElement('img');
        image.height = 100;
        image.width = 100
        image.src = reader.result;
        preview.innerHTML = '';
        preview.append(image);
        const base64Data = reader.result.match(/(?<=base64,)(.*)/gim);
        base64Img4 = base64Data[0];
    };
    if (file) {
        reader.readAsDataURL(file);
        console.log(file);
        console.log(reader);
    }
}

//------------------------------------------------------GLOBOS INDIVIDUALES----------------------------------------------------

$('#btnAgregarGlobo').click(function () {
    $('.modal-title').html('Agregar globo');
    txtNombreGlobo.val('');
    chkMultiplesColores.prop('checked', false);
    btnAgregarGlo.css('display', 'block');
    btnActualizarGlo.css('display', 'none');
});

function initGlobos() {
    const response = ajaxRequest("pageGlobos.aspx/GetGlobos", "{}", "POST");
    if (response == null) {
        alert("No se encontraron globos en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableGlobos").DataTable();
    table.clear();
    globosArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let globos = {
            0: this.id_Globo,
            1: this.nombre_Globo.trim(),
            2: (this.multiplesColores_Globo == true) ? 'Si' : 'No',
            3: `<button type="button" onclick="showModalActualizarGlobo(${this.id_Globo})" class="btn btn-primary" data-toggle="modal" data-target="#ModalGlobo">Editar</button>`
        }
        objectArray.push(globos);
    });
    table.rows.add(objectArray).draw();
}

//INSERTAR GLOBO NUEVO
btnAgregarGlo.click(function () {
    let multiColores = chkMultiplesColores.is(":checked") ? true : false;
    const Object = {
        id: 0,
        nombre: txtNombreGlobo.val(),
        multiplesColores: multiColores
    }
    let response = ajaxRequest("pageGlobos.aspx/postGlobo", JSON.stringify({ globo: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    ModalGlobo.modal('hide');
    initGlobos();
});

//ACTUALIZAR GLOBO
btnActualizarGlo.click(function () {
    let multiColores = chkMultiplesColores.is(":checked") ? true : false;
    const Object = {
        id: IDGloboSelected,
        nombre: txtNombreGlobo.val(),
        multiplesColores: multiColores
    }
    let response = ajaxRequest("pageGlobos.aspx/putGlobo", JSON.stringify({ globo: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    ModalGlobo.modal('hide');
    initGlobos();
});

//MOSTRAR MODAL PARA ACTUALIZAR PRODUCTOS
function showModalActualizarGlobo(id) {
    IDGloboSelected = id;
    const info = globosArray.find(X => X.id_Globo == IDGloboSelected);
    $('.modal-title').html('Editar Globo');
    txtNombreGlobo.val(info.nombre_Globo);
    chkMultiplesColores.prop('checked', (info.multiplesColores_Globo) ? true : false);
    btnAgregarGlo.css('display', 'none');
    btnActualizarGlo.css('display', 'block');
}

//------------------------------------------------------COLORES DE GLOBOS-------------------------------------------------------
let coloresArray = [];
let IDColoresSelected;

//INPUT COLORES
const ModalColor = $('#ModalColor');
const dpGlobos = $('#dpGlobos');
const btnAgregarCol = $('#btnAgregarCol');
const btnActualizarCol = $('#btnActualizarCol');
const chkHabilitadoColor = $('#chkHabilitadoColor');
const txtCodigoColor = $('#txtCodigoColor');
const txtNombreColor = $('#txtNombreColor');

function initColores() {
    const response = ajaxRequest("pageGlobos.aspx/GetColores", "{}", "POST");
    if (response == null) {
        alert("No se encontraron colores en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableColores").DataTable();
    table.clear();
    coloresArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let colores = {
            0: this.id_GloboColor,
            1: this.nombre_Globo.trim(),
            2: this.nombre_GloboColor.trim(),
            3: `<div style="background-color: ${this.codigo_GloboColor}">${this.codigo_GloboColor}</div>`,
            4: (this.habilitado_GloboColor == true) ? 'Si' : 'No',
            5: `<button type="button" onclick="showModalActualizarColor(${this.id_GloboColor})" class="btn btn-primary" data-toggle="modal" data-target="#ModalColor">Editar</button>
                <button type="button" onclick="habilitarColor(${this.id_GloboColor})" class="btn btn-primary">Habilitar</button>`
        }
        objectArray.push(colores);
    });
    table.rows.add(objectArray).draw();
}

$('#btnAgregarColor').click(function () {
    $('.modal-title').html('Agregar globo');
    txtNombreColor.val('');
    txtCodigoColor.val('');
    dpGlobos.val('');
    chkHabilitadoColor.prop('checked', false);
    btnAgregarCol.css('display', 'block');
    btnActualizarCol.css('display', 'none');
    initDpGlobos();
});

function initDpGlobos() {
    dpGlobos.empty();
    globosArray.forEach(function (e) {
        dpGlobos.append(`<option value='${e.id_Globo}'>${e.nombre_Globo}</option>`);
    });
}

//INSERTAR COLOR NUEVO
btnAgregarCol.click(function () {
    let colorHabilitado = chkHabilitadoColor.is(":checked") ? true : false;
    const Object = {
        id: 0,
        nombre: txtNombreColor.val(),
        codigo: txtCodigoColor.val(),
        habilitado: colorHabilitado,
        idGlobo: dpGlobos.val()
    }
    let response = ajaxRequest("pageGlobos.aspx/postColor", JSON.stringify({ color: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    ModalColor.modal('hide');
    initColores();
});

//ACTUALIZAR COLOR
btnActualizarCol.click(function () {
    let colorHabilitado = chkHabilitadoColor.is(":checked") ? true : false;
    const Object = {
        id: IDColoresSelected,
        nombre: txtNombreColor.val(),
        codigo: txtCodigoColor.val(),
        habilitado: colorHabilitado,
        idGlobo: dpGlobos.val()
    }
    let response = ajaxRequest("pageGlobos.aspx/putColor", JSON.stringify({ color: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    ModalColor.modal('hide');
    initColores();
});

//MOSTRAR MODAL PARA ACTUALIZAR COLOR
function showModalActualizarColor(id) {
    IDColoresSelected = id;
    const info = coloresArray.find(X => X.id_GloboColor == IDColoresSelected);
    $('.modal-title').html('Editar Color');
    txtNombreColor.val(info.nombre_GloboColor);
    txtCodigoColor.val(info.codigo_GloboColor);
    dpGlobos.val(info.id_Globo);
    chkHabilitadoColor.prop('checked', (info.habilitado_GloboColor) ? true : false);
    btnAgregarCol.css('display', 'none');
    btnActualizarCol.css('display', 'block');
}

//HABILITAR/DESHABILITAR COLOR
function habilitarColor(id) {
    IDColoresSelected = id;
    const info = coloresArray.find(X => X.id_GloboColor == IDColoresSelected);
    let habilitado = info.habilitado_GloboColor ? false : true;
    const Object = {
        id: IDColoresSelected,
        nombre: info.nombre_GloboColor,
        codigo: info.codigo_GloboColor,
        habilitado: habilitado,
        idGlobo: info.id_GloboColor
    }
    let response = ajaxRequest("pageGlobos.aspx/putColor", JSON.stringify({ color: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initColores();
}