﻿let categoriasArray = [];
let tiposArray = [];
let IDCategoriaSelected;
let IDTipoSelected;

//INPUTS Categoria
const btnAgregarCategoria = $('#btnAgregarCat');
const btnActualizarCategoria = $('#btnActualizarCat');
const txtNombre = $('#txtNombreCat');
const chkHabilitado = $('#chkHabilitadoCat');
const modalCategoria = $('#ModalCategoria');

$(function () {
    initCategorias();
    initTiposDeProductos();
});

function initCategorias() {
    const response = ajaxRequest("pageCategorias.aspx/GetCategorias", "{}", "POST");
    if (response == null) {
        alert("No se encontraron categorias en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableCategoria").DataTable();
    table.clear();
    categoriasArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let categorias = {
            0: this.id_Categoria,
            1: this.nombre_Categoria.trim(),
            2: (this.habilitado_Categoria == true) ? 'Si' : 'No',
            3: `<button type="button" onclick="showModalActualizarCategoria(${this.id_Categoria})" class="btn btn-primary" data-toggle="modal" data-target="#ModalCategoria">Editar</button>
                <button type="button" onclick="habilitarCategoria(${this.id_Categoria})" class="btn btn-primary">Habilitar</button>`
        }
        objectArray.push(categorias);
    });
    table.rows.add(objectArray).draw();
}

function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

//MOSTRAR MODAL PARA AGREGAR CATEGORIA
function showModalAgregarCategoria() {
    $('.modal-title').html('Agregar Categoria');
    txtNombre.val('');
    chkHabilitado.prop('checked', false);
    btnAgregarCategoria.css('display', 'block');
    btnActualizarCategoria.css('display', 'none');
}

//INSERTAR CATEGORIA NUEVA
btnAgregarCategoria.click(function () {
    let habilitado = chkHabilitado.is(":checked") ? true : false;
    const Object = {
        id: 0,
        nombre: txtNombre.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageCategorias.aspx/postCategoria", JSON.stringify({ categoria: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalCategoria.modal('hide');
    initCategorias();
});

//MOSTRAR MODAL PARA ACTUALIZAR CATEGORIA
function showModalActualizarCategoria(id) {
    IDCategoriaSelected = id;
    const info = categoriasArray.find(X => X.id_Categoria == IDCategoriaSelected);
    $('.modal-title').html('Editar Categoria');
    txtNombre.val(info.nombre_Categoria);
    chkHabilitado.prop('checked', (info.habilitado) ? true : false);
    btnAgregarCategoria.css('display', 'none');
    btnActualizarCategoria.css('display', 'block');
}

//UPDATE CATEGORIA
btnActualizarCategoria.click(function () {
    let habilitado = chkHabilitado.is(":checked") ? true : false;
    const Object = {
        id: IDCategoriaSelected,
        nombre: txtNombre.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageCategorias.aspx/putCategoria", JSON.stringify({ categoria: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalCategoria.modal('hide');
    initCategorias();
});

//HABILITAR/DESHABILITAR
function habilitarCategoria(id) {
    IDCategoriaSelected = id;
    const info = categoriasArray.find(X => X.id_Categoria == IDCategoriaSelected);
    let habilitado = info.habilitado_Categoria ? false : true;
    const Object = {
        id: IDCategoriaSelected,
        nombre: info.nombre_Categoria,
        habilitado: habilitado
    }
    let response = ajaxRequest("pageCategorias.aspx/putCategoria", JSON.stringify({ categoria: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initCategorias();
}

//------------------------------------------------------------------------------------

function initTiposDeProductos() {
    const response = ajaxRequest("pageCategorias.aspx/GetTiposProductos", "{}", "POST");
    if (response == null) {
        alert("No se encontraron Tipos de productos en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Tipo: Reponse es igual a error");
        return;
    }
    const table = $("#mytableTipos").DataTable();
    table.clear();
    tiposArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let tipos = {
            0: this.id_ProductoTipo,
            1: this.nombre_ProductoTipo.trim(),
            2: (this.habilitado_ProductoTipo == 1) ? 'Si' : 'No',
            3: `<button type="button" onclick="showModalActualizarTipo(${this.id_ProductoTipo})" class="btn btn-primary" data-toggle="modal" data-target="#ModalTipo">Editar</button>
                <button type="button" onclick="habilitarTipo(${this.id_ProductoTipo})" class="btn btn-primary">Habilitar</button>`
        }
        objectArray.push(tipos);
    });
    table.rows.add(objectArray).draw();
}

//INPUTS Tipos
const btnAgregarTipo = $('#btnAgregarTipoProd');
const btnActualizarTipo = $('#btnActualizarTipoProd');
const txtNombreTipo = $('#txtNombreTipo');
const chkHabilitadoTipo = $('#chkHabilitadoTipo');
const ModalTipo = $('#ModalTipo');

//MOSTRAR MODAL PARA AGREGAR TIPOS DE PRODUCTOS
function showModalAgregarTipo() {
    $('.modal-title').html('Agregar Tipo');
    txtNombreTipo.val('');
    chkHabilitadoTipo.prop('checked', false);
    btnAgregarTipo.css('display', 'block');
    btnActualizarTipo.css('display', 'none');
}

//INSERTAR TIPO NUEVO
btnAgregarTipo.click(function () {
    let habilitado = chkHabilitadoTipo.is(":checked") ? true : false;
    const Object = {
        id: 0,
        nombre: txtNombreTipo.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageCategorias.aspx/postTipo", JSON.stringify({ tipo: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    ModalTipo.modal('hide');
    initTiposDeProductos();
});

//MOSTRAR MODAL PARA ACTUALIZAR TIPO
function showModalActualizarTipo(id) {
    IDTipoSelected = id;
    const info = tiposArray.find(X => X.id_ProductoTipo == IDTipoSelected);
    $('.modal-title').html('Editar tipo de producto');
    txtNombreTipo.val(info.nombre_ProductoTipo);
    chkHabilitadoTipo.prop('checked', (info.habilitado_ProductoTipo) ? true : false);
    btnAgregarTipo.css('display', 'none');
    btnActualizarTipo.css('display', 'block');
}

//UPDATE TIPO
btnActualizarTipo.click(function () {
    let habilitado = chkHabilitadoTipo.is(":checked") ? true : false;
    const Object = {
        id: IDTipoSelected,
        nombre: txtNombreTipo.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageCategorias.aspx/putTipo", JSON.stringify({ tipo: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    ModalTipo.modal('hide');
    initTiposDeProductos();
});

//HABILITAR/DESHABILITAR
function habilitarTipo(id) {
    IDTipoSelected = id;
    const info = tiposArray.find(X => X.id_ProductoTipo == IDTipoSelected);
    let habilitado = info.habilitado_ProductoTipo ? false : true;
    const Object = {
        id: IDTipoSelected,
        nombre: info.nombre_ProductoTipo,
        habilitado: habilitado
    }
    let response = ajaxRequest("pageCategorias.aspx/putTipo", JSON.stringify({ tipo: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initTiposDeProductos();
}

$('#btnSalir').click(function () {
    const response = ajaxRequest("pageCategorias.aspx/CloseSession", "{}", "POST");
    if (response && response != "Error") {
        const result = JSON.parse(response.d);
        if (result.status) {
            window.location = result.location;
        }
    }
});