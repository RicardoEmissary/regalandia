﻿let categoriasArray = [];
let tiposArray = [];
let productosArray = [];
let tamaniosArray = [];
let IDProductoSelected;
let base64Img1;
let base64Img2;
let base64Img3;
let base64Img4;

let coloresArray = [];
let IDColorSelected;

//INPUTS Producto
const txtNombreProducto = $('#txtNombreProducto');
const txtDescripcionProducto = $('#txtDescripcionProducto');
const txtPrecioProducto = $('#txtPrecioProducto');
const txtSKU = $('#txtSKU');
const dpTamanio = $('#dpTamanio');
const dpCategorias = $('#dpCategorias');
const dpTipo = $('#dpTipos');
const fileImg1 = $('#fileImg1');
const fileImg2 = $('#fileImg2');
const fileImg3 = $('#fileImg3');
const fileImg4 = $('#fileImg4');
const oldNameImg1 = $('#nameImage1');
const oldNameImg2 = $('#nameImage2');
const oldNameImg3 = $('#nameImage3');
const oldNameImg4 = $('#nameImage4');
const chkHabilitadoProducto = $('#chkHabilitadoProducto');
const btnAgregarProd = $('#btnAgregarProd');
const btnActualizarProd = $('#btnActualizarProd');
const modalProducto = $('#ModalProducto');
const img1Update = $('#img1Update');
const img2Update = $('#img2Update');
const img3Update = $('#img3Update');
const img4Update = $('#img4Update');

//INPUTS Colores
const btnAgregarCol = $('#btnAgregarCol');
const btnActualizarCol = $('#btnActualizarCol');
const txtNombreColor = $('#txtNombreColor');
const hexColor = $('#hexColor');
const chkHabilitadoColor = $('#chkHabilitadoColor');
const ModalColor = $('#ModalColor');
const dpProducto = $('#dbProducto');

//INPUTS para tipos de modal
const general_form = $('.general_form');
const bolsa_form = $('.bolsa_form');

let dataImg1;
let dataImg2;
let dataImg3;
let dataImg4;

$(function () {
    initProductos();
    initColores();
    initCategorias();
    initAllTipos();
    initTamanios();
});

//GENERIC AJAX
function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

function initProductos() {
    const response = ajaxRequest("pageProductos.aspx/GetProductos", "{}", "POST");
    if (response == null) {
        alert("No se encontraron productos en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableProducto").DataTable();
    table.clear();
    productosArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let productos = {
            0: this.id_Producto,
            1: this.nombre_Producto.trim(),
            2: this.precio_Producto,
            3: this.nombre_ProductoTipo,
            4: (this.habilitado_Producto == true) ? 'Si' : 'No',
            5: `<button type="button" onclick="showModalActualizarProducto(${this.id_Producto})" class="btn btn-primary" data-toggle="modal" data-target="#ModalProducto">Editar</button>
                <button type="button" onclick="habilitarProducto(${this.id_Producto})" class="btn btn-primary">Habilitar</button>`
        }
        objectArray.push(productos);
    });
    table.rows.add(objectArray).draw();
}

function initTamanios() {
    const response = ajaxRequest("pageProductos.aspx/GetTamanios", "{}", "POST");
    if (response == null) {
        alert("No se encontraron Tamaños en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    tamaniosArray = response.d;
    dpTamanio.empty();
    tamaniosArray.forEach(e => dpTamanio.append(`<option value=${e.id_Tamanio}>${e.nombre_Tamanio}</option>`));
}

//MOSTRAR MODAL PARA AGREGAR PRODUCTO
$('#btnAgregarProducto').click(function () {
    $('.modal-title').html('Agregar Producto');
    txtNombreProducto.val('');
    txtSKU.val('');
    txtDescripcionProducto.val('');
    txtPrecioProducto.val('');
    fileImg1.val('');
    fileImg2.val('');
    fileImg3.val('');
    fileImg4.val('');
    img1Update.attr('src', '');
    img2Update.attr('src', '');
    img3Update.attr('src', '');
    img4Update.attr('src', '');
    chkHabilitadoProducto.prop('checked', false);
    btnAgregarProd.css('display', 'block');
    btnActualizarProd.css('display', 'none');
});

//INSERTAR PRODUCTO NUEVO
btnAgregarProd.click(function () {

    let habilitado = chkHabilitadoProducto.is(":checked") ? true : false;
    let nameImg1 = fileImg1.val().replace(/C:\\fakepath\\/i, '');
    nameImg1 = (nameImg1 == '') ? 'no image' : nameImg1;
    let nameImg2 = fileImg2.val().replace(/C:\\fakepath\\/i, '');
    nameImg2 = (nameImg2 == '') ? 'no image' : nameImg2;
    let nameImg3 = fileImg3.val().replace(/C:\\fakepath\\/i, '');
    nameImg3 = (nameImg3 == '') ? 'no image' : nameImg3;
    let nameImg4 = fileImg4.val().replace(/C:\\fakepath\\/i, '');
    nameImg4 = (nameImg4 == '') ? 'no image' : nameImg4;
    const Object = {
        id: 0,
        nombre: txtNombreProducto.val(),
        descripcion: txtDescripcionProducto.val(),
        precio: txtPrecioProducto.val(),
        sku: txtSKU.val(),
        tamanio: dpTamanio.val(),
        nameImg1: nameImg1,
        nameImg2: nameImg2,
        nameImg3: nameImg3,
        nameImg4: nameImg4,
        oldNameImg1: oldNameImg1.val(),
        oldNameImg2: oldNameImg2.val(),
        oldNameImg3: oldNameImg3.val(),
        oldNameImg4: oldNameImg4.val(),
        base64Img1: base64Img1,
        base64Img2: base64Img2,
        base64Img3: base64Img3,
        base64Img4: base64Img4,
        idCategoria: dpCategorias.val(),
        idTipo: dpTipo.val(),
        habilitado: habilitado,
    }
    let response = ajaxRequest("pageProductos.aspx/postProducto", JSON.stringify({ producto: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalProducto.modal('hide');
    initProductos();
});

//MOSTRAR MODAL PARA ACTUALIZAR PRODUCTOS
function showModalActualizarProducto(id) {
    IDProductoSelected = id;
    const info = productosArray.find(X => X.id_Producto == IDProductoSelected);
    $('.modal-title').html('Editar Producto');
    txtNombreProducto.val(info.nombre_Producto);
    txtSKU.val(info.sku_Producto);
    txtPrecioProducto.val(info.precio_Producto);
    txtDescripcionProducto.val(info.descripcion_Producto);
    if (info.foto1_Producto != null) {
        oldNameImg1.val(info.foto1_Producto);
        $('#imgPreview1 img').attr('src', 'multimedia/' + info.foto1_Producto);
    }
    else {
        oldNameImg1.val('');
        $('#imgPreview1 img').attr('src', '');
    }
    if (info.foto2_Producto != null) {
        oldNameImg2.val(info.foto2_Producto);
        $('#imgPreview2 img').attr('src', 'multimedia/' + info.foto2_Producto);
    }
    else {
        oldNameImg2.val('');
        $('#imgPreview2 img').attr('src', '');
    }
    if (info.foto3_Producto != null) {
        oldNameImg3.val(info.foto3_Producto);
        $('#imgPreview3 img').attr('src', 'multimedia/' + info.foto3_Producto);
    }
    else {
        oldNameImg3.val('');
        $('#imgPreview3 img').attr('src', '');
    }
    if (info.foto4_Producto != null) {
        oldNameImg4.val(info.foto4_Producto);
        $('#imgPreview4 img').attr('src', 'multimedia/' + info.foto4_Producto);
    }
    else {
        oldNameImg4.val('');
        $('#imgPreview4 img').attr('src', '');
    }
    fileImg1.val('');
    fileImg2.val('');
    fileImg3.val('');
    fileImg4.val('');

    dpCategorias.val(info.id_Categoria);
    dpTipo.val(info.id_ProductoTipo);
    dpTamanio.val(info.id_Tamanio);
    chkHabilitadoProducto.prop('checked', (info.habilitado_Producto) ? true : false);

    btnAgregarProd.css('display', 'none');
    btnActualizarProd.css('display', 'block');
}

//UPDATE PRODUCTO
btnActualizarProd.click(function () {
    let habilitado = chkHabilitadoProducto.is(":checked") ? true : false;
    let nameImg1 = fileImg1.val().replace(/C:\\fakepath\\/i, '');
    nameImg1 = (nameImg1 == '') ? 'no image' : nameImg1;
    let nameImg2 = fileImg2.val().replace(/C:\\fakepath\\/i, '');
    nameImg2 = (nameImg2 == '') ? 'no image' : nameImg2;
    let nameImg3 = fileImg3.val().replace(/C:\\fakepath\\/i, '');
    nameImg3 = (nameImg3 == '') ? 'no image' : nameImg3;
    let nameImg4 = fileImg4.val().replace(/C:\\fakepath\\/i, '');
    nameImg4 = (nameImg4 == '') ? 'no image' : nameImg4;
    const Object = {
        id: IDProductoSelected,
        nombre: txtNombreProducto.val(),
        descripcion: txtDescripcionProducto.val(),
        precio: txtPrecioProducto.val(),
        sku: txtSKU.val(),
        tamanio: dpTamanio.val(),
        nameImg1: nameImg1,
        nameImg2: nameImg2,
        nameImg3: nameImg3,
        nameImg4: nameImg4,
        oldNameImg1: oldNameImg1.val(),
        oldNameImg2: oldNameImg2.val(),
        oldNameImg3: oldNameImg3.val(),
        oldNameImg4: oldNameImg4.val(),
        base64Img1: base64Img1,
        base64Img2: base64Img2,
        base64Img3: base64Img3,
        base64Img4: base64Img4,
        idCategoria: dpCategorias.val(),
        idTipo: dpTipo.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageProductos.aspx/putProducto", JSON.stringify({ producto: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalProducto.modal('hide');
    initProductos();
});

//HABILITAR/DESHABILITAR
function habilitarProducto(id) {
    IDProductoSelected = id;
    const info = productosArray.find(X => X.id_Producto == IDProductoSelected);
    let habilitado = info.habilitado_Producto ? false : true;
    const Object = {
        id: IDProductoSelected,
        nombre: info.nombre_Producto,
        descripcion: info.descripcion_Producto,
        sku: info.sku_Producto,
        precio: info.precio_Producto,
        tamanio: info.id_Tamanio,
        oldNameImg1: info.foto1_Producto,
        oldNameImg2: info.foto2_Producto,
        oldNameImg3: info.foto3_Producto,
        oldNameImg4: info.foto4_Producto,
        idCategoria: info.id_Categoria,
        idTipo: info.id_ProductoTipo,
        habilitado: habilitado
    }
    let response = ajaxRequest("pageProductos.aspx/putProductoStatus", JSON.stringify({ producto: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initProductos();
}

//CATEGORIAS Y SUBCATEGORIAS
function initCategorias() {
    const response = ajaxRequest("pageProductos.aspx/GetCategorias", "{}", "POST");
    if (response == null) {
        alert("No se encontraron categorias en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    categoriasArray = response.d;
    dpCategorias.empty();
    categoriasArray.forEach(e => dpCategorias.append(`<option value=${e.id_Categoria}>${e.nombre_Categoria}</option>`));
}

function initTipos(id) {
    const response = ajaxRequest("pageProductos.aspx/GetTiposProductos", "{}", "POST");
    if (response == null) {
        alert("No se encontraron tipos de productos en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    tiposArray = response.d;
    tiposArray.forEach(function (e) {
        if (e.id_ProductoTipo == id) {
            dbTipo.append(`<option value=${e.id_ProductoTipo}>${e.nombre_ProductoTipo}</option>`);
        }
    });
}

function initAllTipos() {
    const response = ajaxRequest("pageProductos.aspx/GetTiposProductos", "{}", "POST");
    if (response == null) {
        alert("No se encontraron tipos en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    tiposArray = response.d;
    tiposArray.forEach(function (e) {
        dpTipo.append(`<option value=${e.id_ProductoTipo}>${e.nombre_ProductoTipo}</option>`);
    });
}

// Cargar imagen 1
function previewFile1(event) {
    const file = event.files[0];
    const reader = new FileReader();
    reader.onload = function () {
        let preview = document.getElementById('imgPreview1');
        let image = document.createElement('img');
        image.height = 100;
        image.width = 100
        image.src = reader.result;
        preview.innerHTML = '';
        preview.append(image);
        const base64Data = reader.result.match(/(?<=base64,)(.*)/gim);
        base64Img1 = base64Data[0];
    };
    if (file) {
        reader.readAsDataURL(file);
        console.log(file);
        console.log(reader);
    }
}

// Cargar imagen 2
function previewFile2(event) {
    //const preview = event.parentNode.nextElementSibling.nextElementSibling.children[0].children[0];
    const file = event.files[0];
    const reader = new FileReader();
    reader.onload = function () {
        let preview = document.getElementById('imgPreview2');
        let image = document.createElement('img');
        image.height = 100;
        image.width = 100
        image.src = reader.result;
        preview.innerHTML = '';
        preview.append(image);
        const base64Data = reader.result.match(/(?<=base64,)(.*)/gim);
        base64Img2 = base64Data[0];
    };
    if (file) {
        reader.readAsDataURL(file);
        console.log(file);
        console.log(reader);
    }
}

// Cargar imagen 3
function previewFile3(event) {
    //const preview = event.parentNode.nextElementSibling.nextElementSibling.children[0].children[0];
    const file = event.files[0];
    const reader = new FileReader();
    reader.onload = function () {
        let preview = document.getElementById('imgPreview3');
        let image = document.createElement('img');
        image.height = 100;
        image.width = 100
        image.src = reader.result;
        preview.innerHTML = '';
        preview.append(image);
        const base64Data = reader.result.match(/(?<=base64,)(.*)/gim);
        base64Img3 = base64Data[0];
    };
    if (file) {
        reader.readAsDataURL(file);
        console.log(file);
        console.log(reader);
    }
}

// Cargar imagen 4
function previewFile4(event) {
    //const preview = event.parentNode.nextElementSibling.nextElementSibling.children[0].children[0];
    const file = event.files[0];
    const reader = new FileReader();
    reader.onload = function () {
        let preview = document.getElementById('imgPreview4');
        let image = document.createElement('img');
        image.height = 100;
        image.width = 100
        image.src = reader.result;
        preview.innerHTML = '';
        preview.append(image);
        const base64Data = reader.result.match(/(?<=base64,)(.*)/gim);
        base64Img4 = base64Data[0];
    };
    if (file) {
        reader.readAsDataURL(file);
        console.log(file);
        console.log(reader);
    }
}

//----------------------------------------------------------COLORES-------------------------------------------------

function initColores() {
    const response = ajaxRequest("pageProductos.aspx/GetColores", "{}", "POST");
    if (response == null) {
        alert("No se encontraron colores en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableColor").DataTable();
    table.clear();
    coloresArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let colores = {
            0: this.id_ProductoColor,
            1: this.nombre_ProductoColor.trim(),
            2: `<div style="background-color:${this.codigo_ProductoColor.trim()};">${this.codigo_ProductoColor.trim()}</div>`,
            3: this.nombre_Producto.trim(),
            4: (this.habilitado_ProductoColor == true) ? 'Si' : 'No',
            5: `<button type="button" onclick="showModalActualizarColor(${this.id_ProductoColor})" class="btn btn-primary" data-toggle="modal" data-target="#ModalColor">Editar</button>
                <button type="button" onclick="habilitarColor(${this.id_ProductoColor})" class="btn btn-primary">Habilitar</button>`
        }
        objectArray.push(colores);
    });
    table.rows.add(objectArray).draw();
}

function initProductosDp() {
    dpProducto.empty();
    productosArray.forEach(e => dpProducto.append(`<option value=${e.id_Producto}>${e.nombre_Producto}</option>`));
}

//MOSTRAR MODAL PARA AGREGAR COLOR
function showModalAgregarColor() {
    initProductosDp();
    $('.modal-title').html('Agregar Color');
    txtNombreColor.val('');
    hexColor.val('');
    dpProducto.val('');
    chkHabilitadoColor.prop('checked', false);
    btnAgregarCol.css('display', 'block');
    btnActualizarCol.css('display', 'none');
}

//INSERTAR COLOR NUEVO
btnAgregarCol.click(function () {
    let habilitado = chkHabilitadoColor.is(":checked") ? true : false;
    const Object = {
        id: 0,
        nombre: txtNombreColor.val(),
        codigo: hexColor.val(),
        idProducto: dpProducto.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageProductos.aspx/postColor", JSON.stringify({ color: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    ModalColor.modal('hide');
    initColores();
});

//MOSTRAR MODAL PARA ACTUALIZAR COLOR
function showModalActualizarColor(id) {
    initProductosDp();
    IDColorSelected = id;
    const info = coloresArray.find(X => X.id_ProductoColor == IDColorSelected);
    $('.modal-title').html('Editar Color');
    txtNombreColor.val(info.nombre_ProductoColor);
    hexColor.val(info.codigo_ProductoColor);
    dpProducto.val(info.id_Producto);
    chkHabilitadoColor.prop('checked', (info.habilitado_ProductoColor) ? true : false);
    btnAgregarCol.css('display', 'none');
    btnActualizarCol.css('display', 'block');
}

//UPDATE COLOR
btnActualizarCol.click(function () {
    let habilitado = chkHabilitadoColor.is(":checked") ? true : false;
    const Object = {
        id: IDColorSelected,
        nombre: txtNombreColor.val(),
        codigo: hexColor.val(),
        idProducto: dpProducto.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageProductos.aspx/putColor", JSON.stringify({ color: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    ModalColor.modal('hide');
    initColores();
});

//HABILITAR/DESHABILITAR
function habilitarColor(id) {
    IDColorSelected = id;
    const info = coloresArray.find(X => X.id_ProductoColor == IDColorSelected);
    let habilitado = info.habilitado_ProductoColor ? false : true;
    const Object = {
        id: IDColorSelected,
        nombre: info.nombre_ProductoColor,
        codigo: info.codigo_ProductoColor,
        idProducto: info.id_Producto,
        habilitado: habilitado
    }
    let response = ajaxRequest("pageProductos.aspx/putColor", JSON.stringify({ color: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initColores();
}

$('#btnSalir').click(function () {
    const response = ajaxRequest("pageProductos.aspx/CloseSession", "{}", "POST");
    if (response && response != "Error") {
        const result = JSON.parse(response.d);
        if (result.status) {
            window.location = result.location;
        }
    }
});