﻿let lugaresArray = [];
let IDLugarSelected;

//INPUTS lugares
const btnAgregarLugar = $('#btnAgregarLug');
const btnActualizarLugar = $('#btnActualizarLug');
const txtNombreLugar = $('#txtNombreLugar');
const chkHabilitado = $('#chkHabilitado');
const modalLugar = $('#ModalLugar');

$(function () {
    initLugares();
});

//AJAX GENERIC
function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

function initLugares() {
    const response = ajaxRequest("pageLugares.aspx/GetLugares", "{}", "POST");
    if (response == null) {
        alert("No se encontraron lugares en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableLugares").DataTable();
    table.clear();
    lugaresArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let lugares = {
            0: this.id_Lugar,
            1: this.nombre_Lugar.trim(),
            2: (this.habilitado == true) ? 'Si' : 'No',
            3: `<button type="button" onclick="showModalActualizarLugar(${this.id_Lugar})" class="btn btn-primary" data-toggle="modal" data-target="#ModalLugar">Editar</button>
                <button type="button" onclick="habilitarLugar(${this.id_Lugar})" class="btn btn-primary">Habilitar</button>`
        }
        objectArray.push(lugares);
    });
    table.rows.add(objectArray).draw();
}

//MOSTRAR MODAL PARA AGREGAR FRASE
function showModalAgregarLugar() {
    $('.modal-title').html('Agregar Lugar');
    txtNombreLugar.val('');
    chkHabilitado.prop('checked', false);
    btnAgregarLugar.css('display', 'block');
    btnActualizarLugar.css('display', 'none');
}

//INSERTAR LUGAR NUEVO
btnAgregarLugar.click(function () {
    let habilitado = chkHabilitado.is(":checked") ? true : false;
    const Object = {
        id: 0,
        nombre: txtNombreLugar.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageLugares.aspx/postLugar", JSON.stringify({ lugar: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalLugar.modal('hide');
    initLugares();
});

//MOSTRAR MODAL PARA ACTUALIZAR LUGAR
function showModalActualizarLugar(id) {
    IDLugarSelected = id;
    const info = lugaresArray.find(X => X.id_Lugar == IDLugarSelected);
    $('.modal-title').html('Editar Lugar');
    txtNombreLugar.val(info.nombre_Lugar.trim());
    chkHabilitado.prop('checked', (info.habilitado) ? true : false);
    btnAgregarLugar.css('display', 'none');
    btnActualizarLugar.css('display', 'block');
}

//UPDATE LUGAR
btnActualizarLugar.click(function () {
    let habilitado = chkHabilitado.is(":checked") ? true : false;
    const Object = {
        id: IDLugarSelected,
        nombre: txtNombreLugar.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageLugares.aspx/putLugar", JSON.stringify({ lugar: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalLugar.modal('hide');
    initLugares();
});

//HABILITAR/DESHABILITAR
function habilitarLugar(id) {
    IDLugarSelected = id;
    const info = lugaresArray.find(X => X.id_Lugar == IDLugarSelected);
    let habilitado = info.habilitado ? false : true;
    const Object = {
        id: IDLugarSelected,
        nombre: info.nombre_Lugar,
        habilitado: habilitado
    }
    let response = ajaxRequest("pageLugares.aspx/putLugar", JSON.stringify({ lugar: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initLugares();
}

$('#btnSalir').click(function () {
    const response = ajaxRequest("pageLugares.aspx/CloseSession", "{}", "POST");
    if (response && response != "Error") {
        const result = JSON.parse(response.d);
        if (result.status) {
            window.location = result.location;
        }
    }
});