﻿let ordenesArray = [];
let regalosArray = [];
let globosArray = [];
let direccionesArray = [];

$(function () {
    initOrdenes();
});

//GENERIC AJAX
function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

function initOrdenes() {
    const response = ajaxRequest("pageOrdenes.aspx/GetOrdenes", "{}", "POST");
    if (response == null) {
        alert("No se encontraron ordenes en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableOrdenes").DataTable();
    table.clear();
    ordenesArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let ordenes = {
            0: this.id_Orden,
            1: this.total_Orden,
            2: this.nombreCompleto_Cliente,
            3: this.nombre_MetodoPago,
            4: `<button type="button" onclick="initProductos(${this.id_Orden})" class="btn btn-primary" data-toggle="modal" data-target="#ModalOrden">Detalles</button>`
        }
        objectArray.push(ordenes);
    });
    table.rows.add(objectArray).draw();
}

function initProductos(id) {
    const response = ajaxRequest("pageOrdenes.aspx/GetProductos", JSON.stringify({ id: id }), "POST");
    if (response == null) {
        alert("No se encontraron productos en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableProductos").DataTable();
    table.clear();
    regalosArray = [];
    regalosArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let productos = {
            0: this.id_Regalo,
            1: this.nombre_Producto,
            2: this.cantidadRegalo_Regalo,
            3: this.descripcion_Frase,
            4: `<div style="background-color:${this.codigo_ProductoColor.trim()};">${this.codigo_ProductoColor.trim()}</div>`
        }
        objectArray.push(productos);
    });
    table.rows.add(objectArray).draw();
    if (regalosArray.length > 0)
        initGlobos(regalosArray[0].id_Orden);

    initDirecciones(id);
}

function initGlobos(id) {
    debugger;
    const response = ajaxRequest("pageOrdenes.aspx/GetGlobos", JSON.stringify({ id: id }), "POST");
    if (response == null) {
        alert("No se encontraron globos en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableGlobosProd").DataTable();
    table.clear();
    globosArray = [];
    globosArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let globos = {
            0: this.id_GloboProducto,
            1: this.nombre_Producto,
            2: `<div style="background-color:${this.color_RegaloGlobo.trim()};">${this.color_RegaloGlobo.trim()}</div>`
        }
        objectArray.push(globos);
    });
    table.rows.add(objectArray).draw();
}

function initDirecciones(id) {
    const response = ajaxRequest("pageOrdenes.aspx/GetDirecciones", JSON.stringify({ id: id }), "POST");
    if (response == null) {
        alert("No se encontraron direcciones en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableDireccionesProd").DataTable();
    table.clear();
    direccionesArray = [];
    direccionesArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let direcciones = {
            0: this.id_Producto,
            1: `${this.nombreRemitente}, ${this.calleRemitente}, ${this.numeroRemitente}, ${this.coloniaRemintente}, ${this.CPRemitente}, ${this.ciudadRemitente}, ${this.estadoRemitente}`,
            2: `${this.nombreDestinatario}, ${this.calleDestinatario}, ${this.numeroDestinatario}, ${this.coloniaDestinatario}, ${this.CPDestinatario}, ${this.ciudadDestinatario}, ${this.estadoDestinatario}`,
            3: (this.id_Tienda != null) ? 'Si' : 'No'
        }
        objectArray.push(direcciones);
    });
    table.rows.add(objectArray).draw();
}

$('#btnSalir').click(function () {
    const response = ajaxRequest("pageOrdenes.aspx/CloseSession", "{}", "POST");
    if (response && response != "Error") {
        const result = JSON.parse(response.d);
        if (result.status) {
            window.location = result.location;
        }
    }
});