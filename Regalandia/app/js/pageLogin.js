﻿const btnEntrar = $('#btnEntrar');
const txtEmail = $('#txtEmail');
const txtPass = $('#txtPass');

$(function () {
    
});

//AJAX GENERIC
function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

btnEntrar.click(function () {
    const Object = {
        id: 0,
        email: txtEmail.val(),
        pass: txtPass.val()
    }
    let response = ajaxRequest("pageLogin.aspx/Login", JSON.stringify({ usuario: Object }), "POST");
    if (response != null || response != 'Error') {
        const result = JSON.parse(response.d);
        if (result.status)
            window.location.href = result.location;
        else {
            alert(result.message);
            location.reload();
        }
    }
});