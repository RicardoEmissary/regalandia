﻿let regalosArray = [];
let globosArray = [];
let IDRegaloSelected;

//INPUTS REGALOS
const txtIdRegalo = $('#txtIdRegalo');
const txtIdOrden = $('#txtIdOrden');
const txtSKU = $('#txtSKU');
const txtNombreProducto = $('#txtNombreProducto');
const txtCantidad = $('#txtCantidad');
const txtFechaRecoleccion = $('#txtFechaRecoleccion');
const dpEstatus = $('#dpEstatus');
const txtFrase = $('#txtFrase');
const txtColor = $('#txtColor');

$(function () {
    initRegalos();
});

//GENERIC AJAX
function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

function initRegalos() {
    const response = ajaxRequest("pageRegalos.aspx/GetRegalos", "{}", "POST");
    if (response == null) {
        alert("No se encontraron regalos en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableRegalos").DataTable();
    table.clear();
    regalosArray = [];
    regalosArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let regalos = {
            0: this.id_Orden,
            1: this.id_Regalo,
            2: this.nombre_Producto,
            3: this.cantidadRegalo_Regalo,
            4: this.precioRegalo_Regalo,
            5: this.nombre_RegaloStatus,
            6: `<button type="button" onclick="DetallesRegalo(${this.id_Regalo})" class="btn btn-primary" data-toggle="modal" data-target="#ModalRegalo">Detalles</button>`
        }
        objectArray.push(regalos);
    });
    table.rows.add(objectArray).draw();
}

function initGlobos(id) {
    const response = ajaxRequest("pageRegalos.aspx/GetGlobos", JSON.stringify({ id: id }), "POST");
    if (response == null) {
        alert("No se encontraron globos en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableGlobos").DataTable();
    table.clear();
    globosArray = [];
    globosArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let globos = {
            0: this.id_Regalo,
            1: this.nombre_Producto,
            2: this.precio_RegaloGlobo,
            3: `<div style="background-color: ${this.color_RegaloGlobo}">${this.color_RegaloGlobo}</div>`
        }
        objectArray.push(globos);
    });
    table.rows.add(objectArray).draw();
}

function DetallesRegalo(id) {
    debugger;
    IDRegaloSelected = id;
    const info = regalosArray.find(X => X.id_Regalo == IDRegaloSelected);
    txtIdRegalo.val(info.id_Regalo);
    txtIdOrden.val(info.id_Orden);
    txtSKU.val(info.sku_Producto);
    txtNombreProducto.val(info.nombre_Producto.trim());
    txtCantidad.val(info.cantidadRegalo_Regalo);
    let fecha = parseJsonDate(info.fechaRecoleccion_Regalo);
    txtFechaRecoleccion.val(fecha);
    dpEstatus.val(info.id_RegaloStatus);
    txtFrase.html(info.descripcion_Frase.trim());
    txtColor.val(info.codigo_ProductoColor.trim());
    txtColor.css("background-color", info.codigo_ProductoColor);
    initGlobos(id);
}

function parseJsonDate(jsonDateString) {
    return new Date(parseInt(jsonDateString.replace('/Date(', '')));
}

$('#btnGuardarStatus').click(function () {
    const Object = {
        id: IDRegaloSelected,
        status: dpEstatus.val()
    }
    let response = ajaxRequest("pageRegalos.aspx/putRegalo", JSON.stringify({ regalo: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initRegalos();
});

$('#btnSalir').click(function () {
    const response = ajaxRequest("pageRegalos.aspx/CloseSession", "{}", "POST");
    if (response && response != "Error") {
        const result = JSON.parse(response.d);
        if (result.status) {
            window.location = result.location;
        }
    }
});