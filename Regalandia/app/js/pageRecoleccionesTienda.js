﻿let tiendasArray = [];
let IDTiendaSelected;

const txtIdProducto = $('#txtIdProducto');
const txtIdOrden = $('#txtIdOrden');
const txtNombreProducto = $('#txtNombreProducto');
const txtPaginaWeb = $('#txtPaginaWeb');
const txtPrecio = $('#txtPrecio');
const txtLugar = $('#txtLugar');
const txtDisponible = $('#txtDisponible');
const txtDescripcion = $('#txtDescripcion');
const picFoto = $('#picFoto');

$(function () {
    initTiendas();
});

//GENERIC AJAX
function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

function initTiendas() {
    const response = ajaxRequest("pageRecoleccionesEnTienda.aspx/GetRecoleccionesEnTienda", "{}", "POST");
    if (response == null) {
        alert("No se encontraron tiendas en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableTiendas").DataTable();
    table.clear();
    tiendasArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let tiendas = {
            0: this.id_Producto,
            1: this.id_Orden,
            2: `${this.RemitenteNombre}, ${this.RemitenteCalle}, ${this.RemitenteNumero}, ${this.RemitenteColonia}, ${this.RemitenteCP}, ${this.RemitenteCiudad}, ${this.RemitenteEstado}`,
            3: `${this.DestinatarioNombre}, ${this.DestinatarioCalle}, ${this.DestinatarioNumero}, ${this.DestinatarioColonia}, ${this.DestinatarioCP}, ${this.DestinatarioCiudad}, ${this.DestinatarioEstado}`,
            4: this.nombre_Lugar.trim(),
            5: `<button type="button" onclick="showDetallesTienda(${this.id_Tienda})" class="btn btn-primary" data-toggle="modal" data-target="#ModalTienda">Detalles</button>`
        }
        objectArray.push(tiendas);
    });
    table.rows.add(objectArray).draw();
}

function showDetallesTienda(id) {
    IDTiendaSelected = id;
    const info = tiendasArray.find(X => X.id_Tienda == IDTiendaSelected);
    txtIdProducto.val(info.id_Producto);
    txtIdOrden.val(info.id_Orden);
    txtNombreProducto.val(info.nombre_Tienda);
    txtPaginaWeb.val(info.linkProducto_Tienda);
    txtPrecio.val(info.precio_Tienda);
    txtLugar.val(info.nombre_Lugar);
    txtDisponible.val((info.disponible_Tienda) ? 'Si' : 'No');
    txtDescripcion.val(info.descripcion_Tienda);
    picFoto.attr("src", `multimedia/tiendas/${info.Foto_Tienda}`);
}

$('#btnSalir').click(function () {
    const response = ajaxRequest("pageRecoleccionesEnTienda.aspx/CloseSession", "{}", "POST");
    if (response && response != "Error") {
        const result = JSON.parse(response.d);
        if (result.status) {
            window.location = result.location;
        }
    }
});