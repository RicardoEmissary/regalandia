﻿let frasesArray = [];
let IDFraseSelected;

//INPUTS frases
const btnAgregarFrase = $('#btnAgregarFra');
const btnActualizarFrase = $('#btnActualizarFra');
const txtFraseDesc = $('#txtFrase');
const chkHabilitado = $('#chkHabilitado');
const modalFrase = $('#ModalFrases');

$(function () {
    initFrases();
});

//AJAX GENERIC
function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

function initFrases() {
    const response = ajaxRequest("pageFrases.aspx/GetFrases", "{}", "POST");
    if (response == null) {
        alert("No se encontraron frases en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableFrases").DataTable();
    table.clear();
    frasesArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let frases = {
            0: this.id_Frase,
            1: this.descripcion_Frase.trim(),
            2: (this.habilitado == true) ? 'Si' : 'No',
            3: `<button type="button" onclick="showModalActualizarFrase(${this.id_Frase})" class="btn btn-primary" data-toggle="modal" data-target="#ModalFrases">Editar</button>
                <button type="button" onclick="habilitarFrase(${this.id_Frase})" class="btn btn-primary">Habilitar</button>`
        }
        objectArray.push(frases);
    });
    table.rows.add(objectArray).draw();
}

//MOSTRAR MODAL PARA AGREGAR FRASE
function showModalAgregarFrase() {
    $('.modal-title').html('Agregar Frase');
    txtFraseDesc.val('');
    chkHabilitado.prop('checked', false);
    btnAgregarFrase.css('display', 'block');
    btnActualizarFrase.css('display', 'none');
}

//INSERTAR FRASE NUEVA
btnAgregarFrase.click(function () {
    let habilitado = chkHabilitado.is(":checked") ? true : false;
    const Object = {
        id: 0,
        descripcion: txtFraseDesc.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageFrases.aspx/postFrase", JSON.stringify({ frase: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalFrase.modal('hide');
    initFrases();
});

//MOSTRAR MODAL PARA ACTUALIZAR FRASE
function showModalActualizarFrase(id) {
    IDFraseSelected = id;
    const info = frasesArray.find(X => X.id_Frase == IDFraseSelected);
    $('.modal-title').html('Editar Frase');
    txtFraseDesc.val(info.descripcion_Frase.trim());
    chkHabilitado.prop('checked', (info.habilitado) ? true : false);
    btnAgregarFrase.css('display', 'none');
    btnActualizarFrase.css('display', 'block');
}

//UPDATE FRASE
btnActualizarFrase.click(function () {
    let habilitado = chkHabilitado.is(":checked") ? true : false;
    const Object = {
        id: IDFraseSelected,
        descripcion: txtFraseDesc.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageFrases.aspx/putFrase", JSON.stringify({ frase: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalFrase.modal('hide');
    initFrases();
});

//HABILITAR/DESHABILITAR
function habilitarFrase(id) {
    IDFraseSelected = id;
    const info = frasesArray.find(X => X.id_Frase == IDFraseSelected);
    let habilitado = info.habilitado ? false : true;
    const Object = {
        id: IDFraseSelected,
        descripcion: info.descripcion_Frase,
        habilitado: habilitado
    }
    let response = ajaxRequest("pageFrases.aspx/putFrase", JSON.stringify({ frase: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initFrases();
}

$('#btnSalir').click(function () {
    const response = ajaxRequest("pageFrases.aspx/CloseSession", "{}", "POST");
    if (response && response != "Error") {
        const result = JSON.parse(response.d);
        if (result.status) {
            window.location = result.location;
        }
    }
});