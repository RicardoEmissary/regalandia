﻿let tamaniosArray = [];
let IDTamanioSelected;

//INPUTS tamanios
const btnAgregarTam = $('#btnAgregarTam');
const btnActualizarTam = $('#btnActualizarTam');
const txtNombreTam = $('#txtNombreTamaño');
const txtAlto = $('#txtAlto');
const txtAncho = $('#txtAncho');
const txtLargo = $('#txtLargo');
const txtPeso = $('#txtPeso');
const chkHabilitado = $('#chkHabilitado');
const modalTamanio = $('#ModalTamaño');

$(function () {
    initTamanios();
});

//AJAX GENERIC
function ajaxRequest(url, data, type) {
    let response = null;
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        async: false,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) { }
    });
    return response;
}

function initTamanios() {
    const response = ajaxRequest("pageTamanio.aspx/GetTamanios", "{}", "POST");
    if (response == null) {
        alert("No se encontraron tamaños en la base de datos");
        return;
    }
    else if (response == "Error") {
        alert("Reponse es igual a error");
        return;
    }
    const table = $("#mytableTamanios").DataTable();
    table.clear();
    tamaniosArray = response.d;
    var objectArray = [];
    $(response.d).each(function () {
        let tamanios = {
            0: this.id_Tamanio,
            1: this.nombre_Tamanio.trim(),
            2: (this.habilitado == true) ? 'Si' : 'No',
            3: `<button type="button" onclick="showModalActualizarTam(${this.id_Tamanio})" class="btn btn-primary" data-toggle="modal" data-target="#ModalTamaño">Editar</button>
                <button type="button" onclick="habilitarTam(${this.id_Tamanio})" class="btn btn-primary">Habilitar</button>`
        }
        objectArray.push(tamanios);
    });
    table.rows.add(objectArray).draw();
}

//MOSTRAR MODAL PARA AGREGAR TAMAÑO
function showModalAgregarTam() {
    $('.modal-title').html('Agregar Tamaño');
    txtNombreTam.val('');
    txtAlto.val('');
    txtAncho.val('');
    txtLargo.val('');
    txtPeso.val('');
    chkHabilitado.prop('checked', false);
    btnAgregarTam.css('display', 'block');
    btnActualizarTam.css('display', 'none');
}

//INSERTAR LUGAR TAMAÑO
btnAgregarTam.click(function () {
    let habilitado = chkHabilitado.is(":checked") ? true : false;
    const Object = {
        id: 0,
        nombre: txtNombreTam.val(),
        alto: txtAlto.val(),
        ancho: txtAncho.val(),
        largo: txtLargo.val(),
        peso: txtPeso.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageTamanio.aspx/postTamanio", JSON.stringify({ tamanio: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalTamanio.modal('hide');
    initTamanios();
});

//MOSTRAR MODAL PARA ACTUALIZAR TAMAÑO
function showModalActualizarTam(id) {
    IDTamanioSelected = id;
    const info = tamaniosArray.find(X => X.id_Tamanio == IDTamanioSelected);
    $('.modal-title').html('Editar Tamaño');
    txtNombreTam.val(info.nombre_Tamanio.trim());
    txtAlto.val(info.alto);
    txtAncho.val(info.ancho);
    txtLargo.val(info.largo);
    txtPeso.val(info.peso);
    chkHabilitado.prop('checked', (info.habilitado) ? true : false);
    btnAgregarTam.css('display', 'none');
    btnActualizarTam.css('display', 'block');
}

//UPDATE TAMAÑO
btnActualizarTam.click(function () {
    let habilitado = chkHabilitado.is(":checked") ? true : false;
    const Object = {
        id: IDTamanioSelected,
        nombre: txtNombreTam.val(),
        alto: txtAlto.val(),
        ancho: txtAncho.val(),
        largo: txtLargo.val(),
        peso: txtPeso.val(),
        habilitado: habilitado
    }
    let response = ajaxRequest("pageTamanio.aspx/putTamanio", JSON.stringify({ tamanio: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    modalTamanio.modal('hide');
    initTamanios();
});

//HABILITAR/DESHABILITAR
function habilitarTam(id) {
    IDTamanioSelected = id;
    const info = tamaniosArray.find(X => X.id_Tamanio == IDTamanioSelected);
    let habilitado = info.habilitado ? false : true;
    const Object = {
        id: IDTamanioSelected,
        nombre: info.nombre_Tamanio,
        alto: info.alto,
        ancho: info.ancho,
        largo: info.largo,
        peso: info.peso,
        habilitado: habilitado
    }
    let response = ajaxRequest("pageTamanio.aspx/putTamanio", JSON.stringify({ tamanio: Object }), "POST");
    const result = JSON.parse(response.d);
    alert(result.message);
    initTamanios();
}

$('#btnSalir').click(function () {
    const response = ajaxRequest("pageTamanio.aspx/CloseSession", "{}", "POST");
    if (response && response != "Error") {
        const result = JSON.parse(response.d);
        if (result.status) {
            window.location = result.location;
        }
    }
});