﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.app
{
    public partial class page : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if(HttpContext.Current.Session["usuario"] == null || string.IsNullOrWhiteSpace(HttpContext.Current.Session["usuario"].ToString()))
            {
                Response.Redirect("~/app/pageLogin.aspx");
            }
        }
    }
}