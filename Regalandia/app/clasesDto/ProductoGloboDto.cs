﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Regalandia.app.clasesDto
{
    public class ProductoGloboDto : Profile
    {
        public ProductoGloboDto()
        {
            this.CreateMap<ProductoGloboDto, ProductoSeleccionado>()
                .ForMember(U => U.id_Producto, P => P.MapFrom(M => M.idProducto))
                .ForMember(U => U.id_Frase, P => P.MapFrom(M => M.idFrase))
                .ForMember(U => U.cantidad_ProductoSeleccionado, P => P.MapFrom(M => M.cantidad))
                .ForMember(U => U.combinarColores_ProductoSeleccionado, P => P.MapFrom(M => M.combinarColores))
                .ForMember(U => U.comentario_ProductoSeleccionado, P => P.MapFrom(M => M.comentario));
        }
        public int idProducto { get; set; }

        public int idFrase { get; set; }

        public int cantidad { get; set; }

        public bool combinarColores { get; set; }

        public string comentario { get; set; }

        public DireccionDto direccion { get; set; }

        public List<GloboColorDto> globos { get; set; }
    }
}