﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Regalandia.app.clasesDto
{
    public class DireccionDto: Profile
    {
        public DireccionDto()
        {
            this.CreateMap<DireccionDto, Direccion>()
                .ForMember(U => U.calle_Direccion, P => P.MapFrom(M => M.calle))
                .ForMember(U => U.numeroExterior_Direccion, P => P.MapFrom(M => M.numeroExterior))
                .ForMember(U => U.numeroInterior_Direccion, P => P.MapFrom(M => M.numeroInterior))
                .ForMember(U => U.colonia_Direccion, P => P.MapFrom(M => M.colonia))
                .ForMember(U => U.ciudad_Direccion, P => P.MapFrom(M => M.ciudad))
                .ForMember(U => U.codigoPostal_Direccion, P => P.MapFrom(M => M.codigoPostal))
                .ForMember(U => U.estado_Direccion, P => P.MapFrom(M => M.estado))
                .ForMember(U => U.referencia_Direccion, P => P.MapFrom(M => M.referencia))
                .ForMember(U => U.nombreCompleto_Direccion, P => P.MapFrom(M => M.nombreCompleto))
                .ForMember(U => U.telefono_Direccion, P => P.MapFrom(M => M.telefono))
                .ForMember(U => U.correo_Direccion, P => P.MapFrom(M => M.correo));
        }
        public string calle { get; set; }

        public string numeroExterior { get; set; }
        
        public string numeroInterior { get; set; }
        
        public string colonia { get; set; }
        
        public string ciudad { get; set; }
        
        public string codigoPostal { get; set; }
        
        public string estado { get; set; }
        
        public string referencia { get; set; }

        public string nombreCompleto { get; set; }

        public string telefono { get; set; }

        public string correo { get; set; }

    }
}