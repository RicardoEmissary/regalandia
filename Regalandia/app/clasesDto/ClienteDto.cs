﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Regalandia.app.clasesDto
{
    public class ClienteDto: Profile
    {
        public ClienteDto()
        {
            this.CreateMap<ClienteDto, Cliente>()
                .ForMember(U => U.nombreCompleto_Cliente, P => P.MapFrom(M => M.nombre))
                .ForMember(U => U.telefono_Cliente, P => P.MapFrom(M => M.telefono))
                .ForMember(U => U.correo_Cliente, P => P.MapFrom(M => M.correo));
        }
        public string nombre { get; set; }

        public string telefono { get; set; }

        public string correo { get; set; }
    }
}