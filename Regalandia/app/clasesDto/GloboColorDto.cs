﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Regalandia.app.clasesDto
{
    public class GloboColorDto
    {
        public int idGlobo { get; set; }

        public List<string> colores { get; set; }
    }
}