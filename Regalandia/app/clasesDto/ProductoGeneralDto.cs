﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Regalandia.app.clasesDto
{
    public class ProductoGeneralDto : Profile
    {
        public ProductoGeneralDto()
        {
            this.CreateMap<ProductoGeneralDto, ProductoSeleccionado>()
                .ForMember(U => U.id_Producto, P => P.MapFrom(U => U.idProducto))
                .ForMember(U => U.id_Frase, P => P.MapFrom(U => U.idFrase))
                .ForMember(U => U.cantidad_ProductoSeleccionado, P => P.MapFrom(U => U.cantidad))
                .ForMember(U => U.combinarColores_ProductoSeleccionado, P => P.MapFrom(U => U.combinarColores))
                .ForMember(U => U.comentario_ProductoSeleccionado, P => P.MapFrom(U => U.comentario))
                .ForMember(U => U.comentario_ProductoSeleccionado, P => P.MapFrom(U => U.comentario));
        }
        public int idProducto { get; set; }

        public int idFrase { get; set; }

        public int cantidad { get; set; }

        public bool combinarColores { get; set; }

        public string comentario { get; set; }

        public DireccionDto direccion { get; set; }

        public List<string> colores { get; set; }


    }
}