﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Regalandia.app.clasesDto
{
    public class OrdenDto
    {
        
        public ClienteDto cliente { get; set; }

        public List<ProductoGeneralDto> productosGenerales { get; set; }

        public List<ProductoGloboDto> setGlobos { get; set; }
    }
}