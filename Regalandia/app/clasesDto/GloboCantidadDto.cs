﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Regalandia.app.clasesDto
{
    public class GloboCantidadDto
    {
        public int id { get; set; }

        public int cantidad { get; set; }
    }
}