﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.app
{
    public partial class pageRecoleccionesEnTienda : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(HttpContext.Current.Session["usuario"].ToString()) || HttpContext.Current.Session["usuario"].ToString() == null)
            {
                Response.Redirect("~/app/login.aspx");
            }
        }

        /*[WebMethod(EnableSession = true)]
        public static Object GetRecoleccionesEnTienda()
        {
            try
            {
                using (regalandiaEntities db = new regalandiaEntities())
                {
                    var tiendas = (from R in db.Regalo
                                   join DR in db.Direccion on R.id_DireccionRemitente equals DR.id_Direccion
                                   join DD in db.Direccion on R.id_DireccionDestinatario equals DD.id_Direccion
                                   join RT in db.RecoleccionEnTienda on R.id_Tienda equals RT.id_Tienda
                                   join L in db.Lugar on RT.id_Lugar equals L.id_Lugar
                                   select new
                                   {
                                       R.id_Producto,
                                       R.id_Orden,
                                       RT.id_Tienda,
                                       RT.nombre_Tienda,
                                       RT.linkProducto_Tienda,
                                       RT.precio_Tienda,
                                       RT.disponible_Tienda,
                                       RT.descripcion_Tienda,
                                       RT.Foto_Tienda,
                                       L.nombre_Lugar,
                                       RemitenteNombre = DR.nombreCompleto_Direccion,
                                       RemitenteCalle = DR.calle_Direccion,
                                       RemitenteNumero = DR.numero_Direccion,
                                       RemitenteColonia = DR.colonia_Direccion,
                                       RemitenteCP = DR.codigoPostal_Direccion,
                                       RemitenteCiudad = DR.ciudad_Direccion,
                                       RemitenteEstado = DR.estado_Direccion,
                                       DestinatarioNombre = DD.nombreCompleto_Direccion,
                                       DestinatarioCalle = DD.calle_Direccion,
                                       DestinatarioNumero = DD.numero_Direccion,
                                       DestinatarioColonia = DD.colonia_Direccion,
                                       DestinatarioCP = DD.codigoPostal_Direccion,
                                       DestinatarioCiudad = DD.ciudad_Direccion,
                                       DestinatarioEstado = DD.estado_Direccion
                                   }).ToList();
                    return tiendas;
                }
            }
            catch (Exception E)
            {
                return E;
            }
        }*/

        //Cerrar sesion
        [WebMethod(EnableSession = true)]
        public static string CloseSession()
        {
            HttpContext.Current.Session.Clear();
            string path = HttpContext.Current.Request.UrlReferrer.AbsolutePath;
            string location = "pageLogin.aspx";
            return JsonConvert.SerializeObject(new
            {
                status = true,
                location = location
            });
        }
    }
}