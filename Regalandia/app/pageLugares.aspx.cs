﻿using Newtonsoft.Json;
using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.app
{
    public partial class pageLugares : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(HttpContext.Current.Session["usuario"].ToString()) || HttpContext.Current.Session["usuario"].ToString() == null)
            {
                Response.Redirect("~/app/login.aspx");
            }
        }
        //LUGARES
        [WebMethod(EnableSession = true)]
        public static Object GetLugares()
        {
            using (lugarController controller = new lugarController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //INSERT
        [WebMethod(EnableSession = true)]
        public static string postLugar(Object lugar)
        {
            using (lugarController controller = new lugarController())
            {
                try
                {
                    lugarController.Status _object = controller.Validate(lugar);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.lugar);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el lugar"
                    });
                }
                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE
        [WebMethod(EnableSession = true)]
        public static string putLugar(Object lugar)
        {
            using (lugarController controller = new lugarController())
            {
                try
                {
                    dynamic _lugar = controller.Validate(lugar);
                    if (!_lugar.status)
                        return JsonConvert.SerializeObject(_lugar);
                    controller.Put(_lugar.lugar);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el lugar"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //Cerrar sesion
        [WebMethod(EnableSession = true)]
        public static string CloseSession()
        {
            HttpContext.Current.Session.Clear();
            string path = HttpContext.Current.Request.UrlReferrer.AbsolutePath;
            string location = "pageLogin.aspx";
            return JsonConvert.SerializeObject(new
            {
                status = true,
                location = location
            });
        }
    }
}