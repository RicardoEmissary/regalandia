﻿using Newtonsoft.Json;
using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.app
{
    public partial class pageGlobos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(HttpContext.Current.Session["usuario"].ToString()) || HttpContext.Current.Session["usuario"].ToString() == null)
            {
                Response.Redirect("~/app/login.aspx");
            }
        }

        //SET GLOBOS/////////////////////
        [WebMethod(EnableSession = true)]
        public static Object GetSetGlobos()
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    var setGlobos = controller.GetByTipo(4);
                    return setGlobos;
                }
                catch(Exception exce)
                {
                    return exce;
                }
            }
        }

        //GET PRODUCTO GLOBOS
        [WebMethod(EnableSession = true)]
        public static Object GetProductoGlobo()
        {
            using (productoGloboController controller = new productoGloboController())
            {
                try
                {
                    var prodGlo = controller.Get();
                    return prodGlo;
                }
                catch (Exception exce)
                {
                    return exce;
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public static string postSetGlobos(Object producto, Object arrayGlobos)
        {
            try
            {
                //Insertamos los datos generales del producto
                using (productoController controller = new productoController())
                {
                    productoController.Status _object = controller.Validate(producto);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.producto);
                }
                //Insertamos los globos que perteneceran al set de globos
                using (productoGloboController controller = new productoGloboController())
                {
                    productoGloboController.Status _object = controller.Validate(arrayGlobos);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    foreach (ProductoGlobo e in _object.productoGlobo)
                    {
                        controller.Post(e);
                    }
                }
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(new
                {
                    status = false,
                    message = "Ocurrio un error al guardar el globo"
                });
            }
            return JsonConvert.SerializeObject(new
            {
                status = true,
                message = "Guardado con exito"
            });
        }

        [WebMethod(EnableSession = true)]
        public static string putSetGlobos(Object producto, Object arrayGlobos)
        {
            try
            {
                //Actualizamos los datos generales del Set
                using (productoController controller = new productoController())
                {
                    dynamic _producto = controller.Validate(producto);
                    if (!_producto.status)
                        return JsonConvert.SerializeObject(_producto);
                    controller.Put(_producto.producto);
                }
                //Remplazamos los nuevos globos que conforman el nuevo set
                using (productoGloboController controller = new productoGloboController())
                {
                    productoGloboController.Status _object = controller.Validate(arrayGlobos);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.DeleteGlobosByProducto(_object.productoGlobo.FirstOrDefault().id_Producto);
                    foreach (ProductoGlobo e in _object.productoGlobo)
                    {
                        controller.Post(e);
                    }
                }
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(new
                {
                    status = false,
                    message = "Ocurrio un error al guardar el Set"
                });
            }
            return JsonConvert.SerializeObject(new
            {
                status = true,
                message = "Guardado con exito"
            });
        }



        //GLOBOS/////////////////////////
        [WebMethod(EnableSession = true)]
        public static Object GetGlobos()
        {
            try
            {
                using (globoController controller = new globoController())
                {
                    var globos = controller.Get();
                    return globos;
                }
            }
            catch(Exception exce)
            {
                return exce;
            }
        }

        //INSERT
        [WebMethod(EnableSession = true)]
        public static string postGlobo(Object globo)
        {
            using (globoController controller = new globoController())
            {
                try
                {
                    globoController.Status _object = controller.Validate(globo);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.globo);
                }
                catch (Exception)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el globo"
                    });
                }
                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE
        [WebMethod(EnableSession = true)]
        public static string putGlobo(Object globo)
        {
            using (globoController controller = new globoController())
            {
                try
                {
                    dynamic _globo = controller.Validate(globo);
                    if (!_globo.status)
                        return JsonConvert.SerializeObject(_globo);
                    controller.Put(_globo.globo);
                }
                catch (Exception)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el globo"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }
        


        //COLORES////////////////////////
        [WebMethod(EnableSession = true)]
        public static Object GetColores()
        {
            try
            {
                using (globoColorController controller = new globoColorController())
                {
                    var colores = controller.Get();
                    return colores;
                }
            }
            catch(Exception exce)
            {
                return exce;
            }
        }

        //INSERT
        [WebMethod(EnableSession = true)]
        public static string postColor(Object color)
        {
            using (globoColorController controller = new globoColorController())
            {
                try
                {
                    globoColorController.Status _object = controller.Validate(color);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.globoColor);
                }
                catch (Exception)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el color"
                    });
                }
                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE
        [WebMethod(EnableSession = true)]
        public static string putColor(Object color)
        {
            using (globoColorController controller = new globoColorController())
            {
                try
                {
                    dynamic _color = controller.Validate(color);
                    if (!_color.status)
                        return JsonConvert.SerializeObject(_color);
                    controller.Put(_color.globoColor);
                }
                catch (Exception)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el color"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }
    }
}