﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/app/page.Master" CodeBehind="pageRecoleccionesEnTienda.aspx.cs" Inherits="Regalandia.app.pageRecoleccionesEnTienda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentBody" runat="server">
    <!--Recolecciones en tiendas-->
    <div class="container">
        <h1>Recolecciones en tiendas</h1>
        <div class="table-responsive">
            <table id="mytableTiendas" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID del producto</th>
                        <th>ID de la orden</th>
                        <th>Direccion Remitente</th>
                        <th>Direccion Destinatario</th>
                        <th>Donde Encontrarlo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            <nav aria-label="Page navigation example">
                <ul class="pagination"></ul>
            </nav>
        </div>
    </div>
    <!--Modal Recolecciones en tienda-->
    <div class="modal fade" id="ModalTienda" tabindex="-1" role="dialog" aria-labelledby="ModalTienda" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LabelTienda">Agregrar Lugar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtIdProducto">Id Producto</label>
                            <input type="text" class="form-control" id="txtIdProducto" disabled/>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtIdOrden">Id Orden</label>
                            <input type="text" class="form-control" id="txtIdOrden" disabled/>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtNombreProducto">Nombre del producto</label>
                            <input type="text" class="form-control" id="txtNombreProducto" disabled/>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtPaginaWeb">Pagina web</label>
                            <input type="text" class="form-control" id="txtPaginaWeb" disabled/>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtPrecio">Precio en tienda</label>
                            <input type="text" class="form-control" id="txtPrecio" disabled/>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtLugar">Lugar</label>
                            <input type="text" class="form-control" id="txtLugar" disabled/>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtDisponible">¿Esta disponible en la tienda?</label>
                            <input type="text" class="form-control" id="txtDisponible" disabled/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txtDescripcion">Descripcion</label>
                        <textarea class="form-control" id="txtDescripcion" disabled></textarea>
                    </div>
                    <div class="form-group">
                        <label for="picFoto">Foto: </label>
                        <img width=100 id="picFoto" class="img-thumbnail" alt="No image"/>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/pageRecoleccionesTienda.js"></script>
</asp:Content>