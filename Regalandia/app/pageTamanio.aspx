﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/app/page.Master" CodeBehind="pageTamanio.aspx.cs" Inherits="Regalandia.app.pageTamanio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentBody" runat="server">
    <!--Tamaños-->
    <div class="container">
        <h1>Tamaños</h1>
        <div class="table-responsive">
            <button type="button" id="btnAgregarTamaño" class="btn btn-primary float-right" onclick="showModalAgregarTam()" data-toggle="modal" data-target="#ModalTamaño">Agregar</button>
            <table id="mytableTamanios" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tamaño</th>
                        <th>Habilitado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            <nav aria-label="Page navigation example">
                <ul class="pagination"></ul>
            </nav>
        </div>
    </div>
    <button class="btn btn-primary" id="btnSalir" onclick="CloseSession()">Salir</button>
    <!--Modal Frase-->
    <div class="modal fade" id="ModalTamaño" tabindex="-1" role="dialog" aria-labelledby="ModalTamaño" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LabelTamaño">Agregrar Tamaño</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtNombreTamaño">Nombre</label>
                        <input type="text" class="form-control" id="txtNombreTamaño"/>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtAlto">Alto</label>
                            <input type="number" class="form-control" id="txtAlto"/>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtAncho">Ancho</label>
                            <input type="number" class="form-control" id="txtAncho"/>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtLargo">Largo</label>
                            <input type="number" class="form-control" id="txtLargo"/>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="txtPeso">Peso</label>
                            <input type="number" class="form-control" id="txtPeso"/>
                        </div>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="chkHabilitado">
                        <label class="form-check-label" for="chkHabilitado">Habilitado</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarTam" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarTam" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/pageTamanio.js"></script>
</asp:Content>