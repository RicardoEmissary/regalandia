﻿using Newtonsoft.Json;
using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.app
{
    public partial class pageProductos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(HttpContext.Current.Session["usuario"].ToString()) || HttpContext.Current.Session["usuario"].ToString() == null)
            {
                Response.Redirect("~/app/login.aspx");
            }
        }

        //--------------------------------------------------------------------------

        //PRODUCTOS
        [WebMethod(EnableSession = true)]
        public static Object GetProductos()
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    var list = controller.Get();
                    return list;
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //INSERT
        [WebMethod(EnableSession = true)]
        public static string postProducto(Object producto)
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    productoController.Status _object = controller.Validate(producto);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.producto);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el producto"
                    });
                }
                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE
        [WebMethod(EnableSession = true)]
        public static string putProducto(Object producto)
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    dynamic _producto = controller.Validate(producto);
                    if (!_producto.status)
                        return JsonConvert.SerializeObject(_producto);
                    controller.Put(_producto.producto);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el producto"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE STATUS
        [WebMethod(EnableSession = true)]
        public static string putProductoStatus(Object producto)
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    dynamic _producto = controller.ValidateStatusUpdate(producto);
                    if (!_producto.status)
                        return JsonConvert.SerializeObject(_producto);
                    controller.Put(_producto.producto);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el regalo"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //----------------------------------------------------------------
        //TAMAÑOS
        [WebMethod(EnableSession = true)]
        public static Object GetTamanios()
        {
            using (tamanioController controller = new tamanioController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //-----------------------------------------------------------------
        //COLORES
        [WebMethod(EnableSession = true)]
        public static Object GetColores()
        {
            using (productoColorController controller = new productoColorController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //INSERT
        [WebMethod(EnableSession = true)]
        public static string postColor(Object color)
        {
            using (productoColorController controller = new productoColorController())
            {
                try
                {
                    productoColorController.Status _object = controller.Validate(color);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.productoColor);
                }
                catch (Exception)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el color"
                    });
                }
                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE
        [WebMethod(EnableSession = true)]
        public static string putColor(Object color)
        {
            using (productoColorController controller = new productoColorController())
            {
                try
                {
                    dynamic _color = controller.Validate(color);
                    if (!_color.status)
                        return JsonConvert.SerializeObject(_color);
                    controller.Put(_color.productoColor);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el color"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //-----------------------------------------------------------------
        //CATEGORIAS
        [WebMethod(EnableSession = true)]
        public static Object GetCategorias()
        {
            using (categoriaController controller = new categoriaController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return E.InnerException;
                }
            }
        }

        //TIPOS
        [WebMethod(EnableSession = true)]
        public static Object GetTiposProductos()
        {
            using (tipoProductoController controller = new tipoProductoController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }


        //Cerrar sesion
        [WebMethod(EnableSession = true)]
        public static string CloseSession()
        {
            HttpContext.Current.Session.Clear();
            string path = HttpContext.Current.Request.UrlReferrer.AbsolutePath;
            string location = "pageLogin.aspx";
            return JsonConvert.SerializeObject(new
            {
                status = true,
                location = location
            });
        }
    }
}