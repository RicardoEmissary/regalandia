﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/app/page.Master" CodeBehind="pageProductos.aspx.cs" Inherits="Regalandia.app.pageProductos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentBody" runat="server">
    <!--Tabs-->
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Producto</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Colores</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <!--Producto-->
            <div class="container">
                <h1>Producto</h1>
                <div class="table-responsive">
                    <button type="button" id="btnAgregarProducto" class="btn btn-primary float-right" data-toggle="modal" data-target="#ModalProducto">Agregar</button>
                    <table id="mytableProducto" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Tipo</th>
                                <th>Habilitado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="pagination-container">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination"></ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <!--Colores-->
            <div class="container">
                <h1>Colores</h1>
                <div class="table-responsive">
                    <button type="button" id="btnAgregarColor" class="btn btn-primary float-right" onclick="showModalAgregarColor()" data-toggle="modal" data-target="#ModalColor">Agregar</button>
                    <table id="mytableColor" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Color</th>
                                <th>Producto</th>
                                <th>Habilitado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="pagination-container">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination"></ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!--Modal Producto-->
    <div class="modal fade bd-example-modal-lg" id="ModalProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LabelProducto">Agregrar Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="txtNombreProducto">Nombre del producto</label>
                            <input type="text" class="form-control" id="txtNombreProducto" placeholder="Nombre">
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="txtPrecioProducto">Precio</label>
                            <input type="number" class="form-control" id="txtPrecioProducto">
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="txtSKU">SKU</label>
                            <input type="text" class="form-control" id="txtSKU">
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="dbCategorias">Categoria</label>
                            <select class="form-control" id="dpCategorias"></select>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="dpTamanio">Tamaño</label>
                            <select class="form-control" id="dpTamanio"></select>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="dpTipos">Tipos</label>
                            <select class="form-control" id="dpTipos"></select>
                        </div>
                        <div class="col-12 mb-2">
                            <label class="my-0" for="txtDescripcionProducto">Descripcion</label>
                            <textarea class="form-control" id="txtDescripcionProducto" rows="3"></textarea>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="fileImg1">Imagen 1</label>
                            <input type="file" class="form-control" id="fileImg1" onchange="previewFile1(this)" accept="image/x-png,image/jpeg"/>
                            <div id="imgPreview1">
                                <img src="" width="100" height="100" id="img1Update" alt="Cargar una imagen"/>
                            </div>
                            <input type="text" id="nameImage1" style="display: none"/>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="fileImg2">Imagen 2</label>
                            <input type="file" class="form-control" id="fileImg2" onchange="previewFile2(this)" accept="image/x-png,image/jpeg"/>
                            <div id="imgPreview2">
                                <img src="" width="100" height="100" id="img2Update" alt="Cargar una imagen"/>
                            </div>
                            <input type="text" id="nameImage2" style="display: none"/>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="fileImg3">Imagen 3</label>
                            <input type="file" class="form-control" id="fileImg3" onchange="previewFile3(this)" accept="image/x-png,image/jpeg"/>
                            <div id="imgPreview3">
                                <img src="" width="100" height="100" id="img3Update" alt="Cargar una imagen"/>
                            </div>
                            <input type="text" id="nameImage3" style="display: none"/>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="fileImg4">Imagen 4</label>
                            <input type="file" class="form-control" id="fileImg4" onchange="previewFile4(this)" accept="image/x-png,image/jpeg"/>
                            <div id="imgPreview4">
                                <img src="" width="100" height="100" id="img4Update" alt="Cargar una imagen"/>
                            </div>
                            <input type="text" id="nameImage4" style="display: none"/>
                        </div>
                    </div>
                    <div class="form-check general_form">
                        <input type="checkbox" class="form-check-input" id="chkHabilitadoProducto">
                        <label class="form-check-label" for="chkHabilitadoProducto">Habilitado</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarProd" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarProd" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal ProductoColor-->
    <div class="modal fade" id="ModalColor" tabindex="-1" role="dialog" aria-labelledby="ModalProductoColor" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LabelColor">Agregrar Color</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtNombreColor">Nombre del color</label>
                        <input type="text" class="form-control" id="txtNombreColor" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="hexColor">Color</label>
                        <input type="color" class="form-control" id="hexColor">
                    </div>
                    <div class="form-group">
                        <label for="dbProducto">Producto</label>
                        <select class="form-control" id="dbProducto">
                        </select>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="chkHabilitadoColor">
                        <label class="form-check-label" for="chkHabilitadoColor">Habilitado</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarCol" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarCol" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal Globos-->
    <div class="modal fade bd-example-modal-lg" id="ModalGlobo" tabindex="-1" role="dialog" aria-labelledby="ModalGlobo" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LabelGlobo">Agregrar Globo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtNombreGlobo">Nombre del Globo</label>
                        <input type="text" class="form-control" id="txtNombreGlobo" placeholder="Nombre">
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="chkHabilitadoMultiColor">
                        <label class="form-check-label" for="chkHabilitadoMultiColor">Multiples colores</label>
                    </div>
                    <button type="button" id="btnAgregarGlo" class="btn btn-primary float-right">Agregar</button>
                    <button type="button" id="btnActualizarGlo" class="btn btn-primary float-right">Guardar</button>
                    <br /><br /><hr />
                    <div class="row">
                        <div class="col-12">
                            <h5>Colores para este globo: </h5>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-3">
                            <label for="txtIdGloboColor">ID globo color: </label>
                            <input type="text" class="form-control" id="txtIdGloboColor" disabled/>
                        </div>
                        <div class="col-3">
                            <label for="txtNombreGloboColor">Nombre del color: </label>
                            <input type="text" class="form-control" id="txtNombreGloboColor"/>
                        </div>
                        <div class="col-3">
                            <label for="txtCodigoGloboColor">Color: </label>
                            <input type="color" class="form-control" id="txtCodigoGloboColor"/>
                        </div>
                        <div class="col-3 my-auto">
                            <input type="checkbox" class="form-check-input" id="chkHabilitadoGloboColor">
                            <label class="form-check-label" for="chkHabilitadoGloboColor">Habilitado</label>
                        </div>
                        <div class="col-12">
                            <button id="btnGuardarGloboColor" class="btn btn-primary float-right">Guardar</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="mytableGloboColor" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Codigo color</th>
                                    <th>Habilitado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/pageProducto.js"></script>
</asp:Content>
