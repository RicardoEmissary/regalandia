﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/app/page.Master" CodeBehind="pageGlobos.aspx.cs" Inherits="Regalandia.app.pageGlobos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentBody" runat="server">
    <!--Tabs-->
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="set-tab" data-toggle="tab" href="#set" role="tab" aria-controls="set" aria-selected="true">Set de globos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Globos individuales</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Colores</a>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">
        <!--Set de globos-->
        <div class="tab-pane fade show active" id="set" role="tabpanel" aria-labelledby="set-tab">
            <div class="container">
                <h1>Set de globos</h1>
                <div class="table-responsive">
                    <button type="button" id="btnAgregarSet" class="btn btn-primary float-right" data-toggle="modal" data-target="#ModalSet">Agregar</button>
                    <table id="mytableSet" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Tipo</th>
                                <th>Habilitado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="pagination-container">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination"></ul>
                    </nav>
                </div>
            </div>
        </div>
        <!--Tab globos individuales-->
        <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div class="container">
                <h1>Globos individuales</h1>
                <div class="table-responsive">
                    <button type="button" id="btnAgregarGlobo" class="btn btn-primary float-right" data-toggle="modal" data-target="#ModalGlobo">Agregar</button>
                    <table id="mytableGlobos" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Multiples colores</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="pagination-container">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination"></ul>
                    </nav>
                </div>
            </div>
        </div>
        <!--Tab colores-->
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="container">
                <h1>Colores de globos</h1>
                <div class="table-responsive">
                    <button type="button" id="btnAgregarColor" class="btn btn-primary float-right" data-toggle="modal" data-target="#ModalColor">Agregar</button>
                    <table id="mytableColores" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Globo</th>
                                <th>Nombre</th>
                                <th>Codigo</th>
                                <th>Habilitado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="pagination-container">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination"></ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!--Modal set de globos-->
    <div class="modal fade bd-example-modal-lg" id="ModalSet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregrar set de globos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="txtNombreProducto">Nombre del producto</label>
                            <input type="text" class="form-control" id="txtNombreProducto" placeholder="Nombre">
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="txtPrecioProducto">Precio</label>
                            <input type="number" class="form-control" id="txtPrecioProducto">
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="txtSKU">SKU</label>
                            <input type="text" class="form-control" id="txtSKU">
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="dbCategorias">Categoria</label>
                            <select class="form-control" id="dpCategorias"></select>
                        </div>
                        <div class="col-12 mb-2">
                            <label class="my-0" for="txtDescripcionProducto">Descripcion</label>
                            <textarea class="form-control" id="txtDescripcionProducto" rows="3"></textarea>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="fileImg1">Imagen 1</label>
                            <input type="file" class="form-control" id="fileImg1" onchange="previewFile1(this)" accept="image/x-png,image/jpeg"/>
                            <div id="imgPreview1">
                                <img src="" width="100" height="100" id="img1Update" alt="Cargar una imagen"/>
                            </div>
                            <input type="text" id="nameImage1" style="display: none"/>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="fileImg2">Imagen 2</label>
                            <input type="file" class="form-control" id="fileImg2" onchange="previewFile2(this)" accept="image/x-png,image/jpeg"/>
                            <div id="imgPreview2">
                                <img src="" width="100" height="100" id="img2Update" alt="Cargar una imagen"/>
                            </div>
                            <input type="text" id="nameImage2" style="display: none"/>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="fileImg3">Imagen 3</label>
                            <input type="file" class="form-control" id="fileImg3" onchange="previewFile3(this)" accept="image/x-png,image/jpeg"/>
                            <div id="imgPreview3">
                                <img src="" width="100" height="100" id="img3Update" alt="Cargar una imagen"/>
                            </div>
                            <input type="text" id="nameImage3" style="display: none"/>
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="fileImg4">Imagen 4</label>
                            <input type="file" class="form-control" id="fileImg4" onchange="previewFile4(this)" accept="image/x-png,image/jpeg"/>
                            <div id="imgPreview4">
                                <img src="" width="100" height="100" id="img4Update" alt="Cargar una imagen"/>
                            </div>
                            <input type="text" id="nameImage4" style="display: none"/>
                        </div>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="chkHabilitadoProducto">
                        <label class="form-check-label" for="chkHabilitadoProducto">Habilitado</label>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <h3>Globos disponibles</h3>
                        </div>
                    </div>
                    <div class="row mt-1" id="chksAreaGlobos">
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarProd" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarProd" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal globos individuales-->
    <div class="modal fade bd-example-modal-lg" id="ModalGlobo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LabelProducto">Agregrar Globo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="txtNombreGlobo">Nombre del globo</label>
                            <input type="text" class="form-control" id="txtNombreGlobo" placeholder="Nombre">
                        </div>
                        <div class="col-lg-6 col-sm-12 my-auto form-check">
                            <input type="checkbox" class="form-check-input" id="chkMultiplesColores">
                            <label class="form-check-label" for="chkMultiplesColores">Multiples colores</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarGlo" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarGlo" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal Colores-->
    <div class="modal fade bd-example-modal-lg" id="ModalColor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregrar Globo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="txtNombreColor">Nombre del color</label>
                            <input type="text" class="form-control" id="txtNombreColor" placeholder="Nombre">
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="txtCodigoColor">Color</label>
                            <input type="color" class="form-control" id="txtCodigoColor">
                        </div>
                        <div class="col-lg-6 col-sm-12 mb-2">
                            <label class="my-0" for="dpGlobos">Globo</label>
                            <select class="form-control" id="dpGlobos"></select>
                        </div>
                        <div class="col-lg-6 col-sm-12 my-auto form-check">
                            <input type="checkbox" class="form-check-input" id="chkHabilitadoColor">
                            <label class="form-check-label" for="chkHabilitadoColor">Habilitado</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarCol" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarCol" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/pageGlobos.js"></script>
</asp:Content>