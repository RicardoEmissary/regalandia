﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/app/page.Master" CodeBehind="pageRecoleccionesEnCasa.aspx.cs" Inherits="Regalandia.app.pageRecoleccionesEnCasa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentBody" runat="server">
    <!--Recolecciones en casa-->
    <div class="container">
        <h1>Recolecciones en casa</h1>
        <div class="table-responsive">
            <table id="mytableCasa" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID del producto</th>
                        <th>ID de la orden</th>
                        <th>Direccion Remitente</th>
                        <th>Direccion Destinatario</th>
                        <th>Nombre del producto</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>1</th>
                        <th>2277</th>
                        <th>Loma Redonda 1234 66666 Monterrey Nuevo Leon</th>
                        <th>Pino Noble 1340 66612 Apodaca Nuevo Leon</th>
                        <th>Laptop</th>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            <nav aria-label="Page navigation example">
                <ul class="pagination"></ul>
            </nav>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/pageRecoleccionesCasa.js"></script>
</asp:Content>