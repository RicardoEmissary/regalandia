﻿using Newtonsoft.Json;
using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.app
{
    public partial class pageLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        //LOGIN
        [WebMethod(EnableSession = true)]
        public static string Login(Object usuario)
        {
            using (usuarioController controller = new usuarioController())
            {
                try
                {
                    usuarioController.Status _object = controller.Validate(usuario);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    _object.usuario.password_Usuario = cryptoServiceController.MD5BasicEncrypt(_object.usuario.password_Usuario);

                    if(controller.Login(_object.usuario.email_Usuario, _object.usuario.password_Usuario))
                    {

                        HttpContext.Current.Session.Add("usuario", _object.usuario.email_Usuario);

                        return JsonConvert.SerializeObject(new
                        {
                            status = true,
                            location = "pageCategorias.aspx",
                            message = "Acceso permitido"
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            status = false,
                            message = "Acceso denegado, revise sus datos"
                        });
                    }
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al logear al usuario"
                    });
                }
            }
        }
    }
}