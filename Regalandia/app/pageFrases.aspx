﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/app/page.Master" CodeBehind="pageFrases.aspx.cs" Inherits="Regalandia.app.pageFrases" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>REGALANDIA | Frases</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentBody" runat="server">
    <!--Frases-->
    <div class="container">
        <h1>Frases</h1>
        <div class="table-responsive">
            <button type="button" id="btnAgregarFrase" class="btn btn-primary float-right" onclick="showModalAgregarFrase()" data-toggle="modal" data-target="#ModalFrases">Agregar</button>
            <table id="mytableFrases" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Frase</th>
                        <th>Habilitado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            <nav aria-label="Page navigation example">
                <ul class="pagination"></ul>
            </nav>
        </div>
    </div>
    <!--Modal Frase-->
    <div class="modal fade" id="ModalFrases" tabindex="-1" role="dialog" aria-labelledby="ModalFrase" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LabelFrase">Agregrar Frase</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtNombreColor">Frase</label>
                        <textarea class="form-control" id="txtFrase"></textarea>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="chkHabilitado">
                        <label class="form-check-label" for="chkHabilitado">Habilitado</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarFra" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarFra" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/pageFrases.js"></script>
</asp:Content>