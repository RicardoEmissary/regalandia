﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/app/page.Master" CodeBehind="pageLugares.aspx.cs" Inherits="Regalandia.app.pageLugares" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>REGALANDIA | Lugares</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentBody" runat="server">
    <!--Lugares-->
    <div class="container">
        <h1>Lugares</h1>
        <div class="table-responsive">
            <button type="button" id="btnAgregarLugar" class="btn btn-primary float-right" onclick="showModalAgregarLugar()" data-toggle="modal" data-target="#ModalLugar">Agregar</button>
            <table id="mytableLugares" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Lugar</th>
                        <th>Habilitado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            <nav aria-label="Page navigation example">
                <ul class="pagination"></ul>
            </nav>
        </div>
    </div>
    <!--Modal Lugar-->
    <div class="modal fade" id="ModalLugar" tabindex="-1" role="dialog" aria-labelledby="ModalLugar" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LabelLugar">Agregrar Lugar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtNombreLugar">Nombre</label>
                        <input type="text" class="form-control" id="txtNombreLugar"/>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="chkHabilitadoLugar">
                        <label class="form-check-label" for="chkHabilitadoLugar">Habilitado</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarLug" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarLug" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/pageLugares.js"></script>
</asp:Content>