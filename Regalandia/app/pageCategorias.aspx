﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/app/page.Master" CodeBehind="pageCategorias.aspx.cs" Inherits="Regalandia.app.pageCategorias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>REGALANDIA | Categorias</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentBody" runat="server">
    <!--Tabs-->
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Categoria</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Tipos de productos</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <!--Categoria-->
            <div class="container">
                <h1>Categorias</h1>
                <div class="table-responsive">
                    <button type="button" id="btnAgregarCategoria" class="btn btn-primary float-right" onclick="showModalAgregarCategoria()" data-toggle="modal" data-target="#ModalCategoria">Agregar</button>
                    <table id="mytableCategoria" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Habilitado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="pagination-container">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination"></ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <!--Tipo-->
            <div class="container">
                <h1>Tipos de productos</h1>
                <div class="table-responsive">
                    <button type="button" id="btnAgregarTipo" class="btn btn-primary float-right" onclick="showModalAgregarTipo()" data-toggle="modal" data-target="#ModalTipo">Agregar</button>
                    <table id="mytableTipos" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Habilitado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="pagination-container">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination"></ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!--Modal agregar Categoria-->
    <div class="modal fade" id="ModalCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregrar Categoria</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre de la categoria</label>
                        <input type="text" class="form-control" id="txtNombreCat" placeholder="Nombre">
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="chkHabilitadoCat">
                        <label class="form-check-label" for="chkHabilitadoCat">Habilitado</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarCat" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarCat" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal agregar Tipo-->
    <div class="modal fade" id="ModalTipo" tabindex="-1" role="dialog" aria-labelledby="ModalTipo" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregrar Tipo de producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="txtNombreTipo">Nombre del tipo de producto</label>
                        <input type="text" class="form-control" id="txtNombreTipo" placeholder="Nombre">
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="chkHabilitadoTipo">
                        <label class="form-check-label" for="chkHabilitadoTipo">Habilitado</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btnAgregarTipoProd" class="btn btn-primary">Agregar</button>
                    <button type="button" id="btnActualizarTipoProd" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/pageCategorias.js"></script>
</asp:Content>