﻿using Newtonsoft.Json;
using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.app
{
    public partial class pageTamanio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(HttpContext.Current.Session["usuario"].ToString()) || HttpContext.Current.Session["usuario"].ToString() == null)
            {
                Response.Redirect("~/app/login.aspx");
            }
        }

        //TAMAÑOS
        [WebMethod(EnableSession = true)]
        public static List<Tamanio> GetTamanios()
        {
            using (tamanioController controller = new tamanioController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return null;
                }
            }
        }

        //INSERT
        [WebMethod(EnableSession = true)]
        public static string postTamanio(Object tamanio)
        {
            using (tamanioController controller = new tamanioController())
            {
                try
                {
                    tamanioController.Status _object = controller.Validate(tamanio);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.tamanio);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el lugar"
                    });
                }
                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE
        [WebMethod(EnableSession = true)]
        public static string putTamanio(Object tamanio)
        {
            using (tamanioController controller = new tamanioController())
            {
                try
                {
                    dynamic _tamanio = controller.Validate(tamanio);
                    if (!_tamanio.status)
                        return JsonConvert.SerializeObject(_tamanio);
                    controller.Put(_tamanio.tamanio);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el lugar"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //Cerrar sesion
        [WebMethod(EnableSession = true)]
        public static string CloseSession()
        {
            HttpContext.Current.Session.Clear();
            string path = HttpContext.Current.Request.UrlReferrer.AbsolutePath;
            string location = "pageLogin.aspx";
            return JsonConvert.SerializeObject(new
            {
                status = true,
                location = location
            });
        }
    }
}