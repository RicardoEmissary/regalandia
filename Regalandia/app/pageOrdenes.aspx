﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/app/page.Master" CodeBehind="pageOrdenes.aspx.cs" Inherits="Regalandia.app.pageOrdenes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentBody" runat="server">
    <!--Ordenes-->
    <div class="container">
        <h1>Ordenes</h1>
        <div class="table-responsive">
            <table id="mytableOrdenes" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Total</th>
                        <th>Cliente</th>
                        <th>Metodo de pago</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            <nav aria-label="Page navigation example">
                <ul class="pagination"></ul>
            </nav>
        </div>
    </div>

    <!--Modal Detalles de la orden-->
    <div class="modal fade bd-example-modal-lg" id="ModalOrden" tabindex="-1" role="dialog" aria-labelledby="ModalOrden" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LabelFrase">Detalles de la Orden</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--Tabs-->
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Regalos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Globos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="direcciones-tab" data-toggle="tab" href="#direcciones" role="tab" aria-controls="direcciones" aria-selected="false">Direcciones</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <!--Productos-->
                            <div class="container">
                                <h1>Regalos</h1>
                                <div class="table-responsive">
                                    <table id="mytableProductos" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID Regalo</th>
                                                <th>Nombre</th>
                                                <th>Cantidad</th>
                                                <th>Frase</th>
                                                <th>Color</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="pagination-container">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination"></ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <!--Globos-->
                            <div class="container">
                                <h1>Globos</h1>
                                <div class="table-responsive">
                                    <table id="mytableGlobosProd" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID Regalo</th>
                                                <th>Nombre del globo</th>
                                                <th>Color</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="pagination-container">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination"></ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="direcciones" role="tabpanel" aria-labelledby="direcciones-tab">
                            <!--Direcciones-->
                            <div class="container">
                                <h1>Direcciones</h1>
                                <div class="table-responsive">
                                    <table id="mytableDireccionesProd" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID Producto</th>
                                                <th>Remitente</th>
                                                <th>Destinatario</th>
                                                <th>Recoger en tienda</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="pagination-container">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination"></ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>



</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="js/pageOrdenes.js"></script>
</asp:Content>
