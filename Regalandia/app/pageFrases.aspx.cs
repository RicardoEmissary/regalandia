﻿using Newtonsoft.Json;
using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.app
{
    public partial class pageFrases : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(HttpContext.Current.Session["usuario"].ToString()) || HttpContext.Current.Session["usuario"].ToString() == null)
            {
                Response.Redirect("~/app/login.aspx");
            }
        }

        //FRASES
        [WebMethod(EnableSession = true)]
        public static Object GetFrases()
        {
            using (frasesController controller = new frasesController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //INSERT
        [WebMethod(EnableSession = true)]
        public static string postFrase(Object frase)
        {
            using (frasesController controller = new frasesController())
            {
                try
                {
                    frasesController.Status _object = controller.Validate(frase);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.frase);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar la frase"
                    });
                }
                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE
        [WebMethod(EnableSession = true)]
        public static string putFrase(Object frase)
        {
            using (frasesController controller = new frasesController())
            {
                try
                {
                    dynamic _frase = controller.Validate(frase);
                    if (!_frase.status)
                        return JsonConvert.SerializeObject(_frase);
                    controller.Put(_frase.frase);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar la frase"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //Cerrar sesion
        [WebMethod(EnableSession = true)]
        public static string CloseSession()
        {
            HttpContext.Current.Session.Clear();
            string path = HttpContext.Current.Request.UrlReferrer.AbsolutePath;
            string location = "pageLogin.aspx";
            return JsonConvert.SerializeObject(new
            {
                status = true,
                location = location
            });
        }
    }
}