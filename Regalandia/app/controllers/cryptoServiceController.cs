﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class cryptoServiceController : ApiController
    {

        public static string MD5BasicEncrypt(string textToEncrypt)
        {
            byte[] bytesConverter = new byte[textToEncrypt.Length];
            for (int i = 0; i < textToEncrypt.Length; i++)
                bytesConverter[i] = Convert.ToByte(textToEncrypt[i]);
            var bytes = MD5.Create().ComputeHash(bytesConverter);

            String[] contraDecode = BitConverter.ToString(bytes).Split('-');
            StringBuilder builder = new StringBuilder();
            foreach (string value in contraDecode)
            {
                builder.Append(value);
            }
            string result = builder.ToString().ToLower();

            return result;
        }

    }
}
