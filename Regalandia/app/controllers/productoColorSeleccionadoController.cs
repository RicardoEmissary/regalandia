﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class productoColorSeleccionadoController : ApiController
    {
        public void Post (int idProductoSeleccionado, string codigoColor)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                ProductoColorSeleccionado productoColor = new ProductoColorSeleccionado();

                productoColor.id_ProductoSeleccionado = idProductoSeleccionado;
                productoColor.codigo_ProductoColorSeleccionado = codigoColor;

                db.ProductoColorSeleccionado.Add(productoColor);
                db.SaveChanges();
            }
        }
    }
}
