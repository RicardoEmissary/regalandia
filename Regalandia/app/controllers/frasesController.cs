﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class frasesController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public Frase frase;
        }

        public List<Frase> Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Frase.ToList();
            }
        }

        public List<Frase> GetFrasesEnabled()
        {
            using(regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Frase.Where(X => X.habilitado == true).ToList();
            }
        }

        public List<Frase> getFraseById(int id)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Frase.Where(X => X.id_Frase == id).ToList();
            }
        }

        public void Post(Frase frase)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Frase.Add(frase);
                db.SaveChanges();
            }
        }

        public void Put(Frase frase)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Frase.Add(frase);
                db.Entry(frase).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public Status Validate(Object frase)
        {
            var json = JsonConvert.SerializeObject(frase);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            Frase _frase = new Frase();
            Status status = new Status();
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _frase.id_Frase = int.Parse(dictionary["id"]);

                _frase.descripcion_Frase = dictionary["descripcion"];
                if (string.IsNullOrWhiteSpace(_frase.descripcion_Frase))
                {
                    status.status = false;
                    status.message = "Escribe una frase";
                    return status;
                }

                _frase.habilitado = Boolean.Parse(dictionary["habilitado"]);
            }
            catch (Exception)
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.frase = _frase;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
