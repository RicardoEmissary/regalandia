﻿using Newtonsoft.Json;
using Regalandia.app.clasesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class productoGloboController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public List<ProductoGlobo> productoGlobo;
        }

        private int getLastProductoId()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                int id = db.Producto.Max(X => X.id_Producto);
                return id;
            }
        }

        public List<ProductoGlobo> Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.ProductoGlobo.ToList();
            }
        }

        public Object GetProductoGlobo()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                var productoGlobo = (from PG in db.ProductoGlobo
                                     join G in db.Globo on PG.id_Globo equals G.id_Globo
                                     select new
                                     {
                                         PG.id_Globo,
                                         G.nombre_Globo,
                                         G.multiplesColores_Globo,
                                         PG.id_Producto,
                                         PG.cantidad_ProductoGlobo
                                     }).ToList();

                return productoGlobo;
            }
        }

        public void Post(ProductoGlobo productoGlobo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.ProductoGlobo.Add(productoGlobo);
                db.SaveChanges();
            }
        }

        public void DeleteGlobosByProducto(int ? idProductoGlobo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                List<ProductoGlobo> _prodGlob = db.ProductoGlobo.Where(X => X.id_Producto == idProductoGlobo).ToList();
                foreach(ProductoGlobo e in _prodGlob)
                {
                    db.ProductoGlobo.Remove(e);
                }
                db.SaveChanges();
            }
        }

        public Status Validate(Object array)
        {
            var json = JsonConvert.SerializeObject(array);
            List<GloboCantidadDto> dictionary = JsonConvert.DeserializeObject<List<GloboCantidadDto>>(json);
            ProductoGlobo _color = new ProductoGlobo();
            Status status = new Status();
            status.productoGlobo = new List<ProductoGlobo>();
            int idProducto = getLastProductoId();
            try
            {
                foreach (GloboCantidadDto e in dictionary) 
                {
                    ProductoGlobo _prodGlo = new ProductoGlobo();
                    _prodGlo.id_Globo = e.id;
                    if(_prodGlo.id_Globo == 0 || _prodGlo.id_Globo == null)
                    {
                        status.status = false;
                        status.message = "Falta idGlobo";
                        return status;
                    }
                    _prodGlo.cantidad_ProductoGlobo = e.cantidad;
                    if(_prodGlo.cantidad_ProductoGlobo == 0 || _prodGlo.cantidad_ProductoGlobo == null)
                    {
                        status.status = false;
                        status.message = "La cantidad es igual a cero";
                        return status;
                    }
                    _prodGlo.id_Producto = idProducto;
                    if(_prodGlo.id_Producto == 0 || _prodGlo.id_Producto == null)
                    {
                        status.status = false;
                        status.message = "Falta id del producto";
                        return status;
                    }
                    status.productoGlobo.Add(_prodGlo);
                }
            }
            catch (Exception)
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
