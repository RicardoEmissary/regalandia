﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class lugarController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public Lugar lugar;
        }

        public List<Lugar> Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Lugar.ToList();
            }
        }

        public List<Lugar> getLugarById(int id)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Lugar.Where(X => X.id_Lugar == id).ToList();
            }
        }

        public void Post(Lugar lugar)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Lugar.Add(lugar);
                db.SaveChanges();
            }
        }

        public void Put(Lugar lugar)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Lugar.Add(lugar);
                db.Entry(lugar).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public Status Validate(Object lugar)
        {
            var json = JsonConvert.SerializeObject(lugar);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            Lugar _lugar = new Lugar();
            Status status = new Status();
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _lugar.id_Lugar = int.Parse(dictionary["id"]);

                _lugar.nombre_Lugar = dictionary["nombre"];
                if (string.IsNullOrWhiteSpace(_lugar.nombre_Lugar))
                {
                    status.status = false;
                    status.message = "Falta nombre del lugar";
                    return status;
                }

                _lugar.habilitado = Boolean.Parse(dictionary["habilitado"]);
            }
            catch (Exception)
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.lugar = _lugar;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
