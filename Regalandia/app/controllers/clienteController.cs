﻿using AutoMapper;
using Regalandia.app.clasesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class clienteController : ApiController
    {

        public List<Cliente> Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var clientes = db.Cliente.ToList();
                return clientes;
            }
        }

        public Cliente Post(Cliente cliente)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                Cliente clienteBD = db.Cliente.FirstOrDefault(X => 
                    X.nombreCompleto_Cliente == cliente.nombreCompleto_Cliente && 
                    X.correo_Cliente == cliente.correo_Cliente && 
                    X.telefono_Cliente == cliente.telefono_Cliente);

                if(clienteBD == null)
                {
                    db.Cliente.Add(cliente);
                    db.SaveChanges();
                    return cliente;
                }

                return clienteBD;
            }
        }

        public Cliente ProfileMap(ClienteDto clienteDto)
        {
            var config = new MapperConfiguration(cfg =>
                   cfg.CreateMap<Cliente, ClienteDto>()
               );
            var mapper = new Mapper(config);
            var cliente = mapper.Map<Cliente>(clienteDto);

            return cliente;
        }
    }
}
