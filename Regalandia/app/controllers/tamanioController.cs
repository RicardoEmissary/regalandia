﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class tamanioController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public Tamanio tamanio;
        }

        public List<Tamanio> Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Tamanio.ToList();
            }
        }

        public List<Tamanio> getTamanioById(int id)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Tamanio.Where(X => X.id_Tamanio == id).ToList();
            }
        }

        public void Post(Tamanio tamanio)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Tamanio.Add(tamanio);
                db.SaveChanges();
            }
        }

        public void Put(Tamanio tamanio)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Tamanio.Add(tamanio);
                db.Entry(tamanio).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public Status Validate(Object tamanio)
        {
            var json = JsonConvert.SerializeObject(tamanio);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            Tamanio _tam = new Tamanio();
            Status status = new Status();
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _tam.id_Tamanio = int.Parse(dictionary["id"]);

                _tam.nombre_Tamanio = dictionary["nombre"];
                if (string.IsNullOrWhiteSpace(_tam.nombre_Tamanio))
                {
                    status.status = false;
                    status.message = "Falta nombre del Tamaño";
                    return status;
                }
                _tam.largo = float.Parse(dictionary["largo"]);
                _tam.ancho = float.Parse(dictionary["ancho"]);
                _tam.alto = float.Parse(dictionary["alto"]);
                _tam.peso = float.Parse(dictionary["peso"]);
                _tam.habilitado = Boolean.Parse(dictionary["habilitado"]);
            }
            catch (Exception)
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.tamanio = _tam;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
