﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class productoColorController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public ProductoColor productoColor;
        }

        public Object Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var colores = (from C in db.ProductoColor
                               join P in db.Producto on C.id_Producto equals P.id_Producto
                               select new
                               {
                                   C.id_ProductoColor,
                                   C.nombre_ProductoColor,
                                   C.codigo_ProductoColor,
                                   C.habilitado_ProductoColor,
                                   C.id_Producto,
                                   P.nombre_Producto
                               }).ToList();

                return colores;
            }
        }

        public void Post(ProductoColor color)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.ProductoColor.Add(color);
                db.SaveChanges();
            }
        }

        public void Put(ProductoColor color)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.ProductoColor.Add(color);
                db.Entry(color).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public List<ProductoColor> getColorById(int id)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.ProductoColor.Where(X => X.id_ProductoColor == id).ToList();
            }
        }

        public Object getColoresByTipo(long idTipo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var colores = (from pc in db.ProductoColor
                               join p in db.Producto on pc.id_Producto equals p.id_Producto
                               join c in db.Categoria on p.id_Categoria equals c.id_Categoria
                               select new
                               {
                                   pc.id_ProductoColor,
                                   pc.nombre_ProductoColor,
                                   pc.codigo_ProductoColor,
                                   pc.habilitado_ProductoColor,
                                   pc.id_Producto,
                                   p.id_ProductoTipo
                               })
                               .Where(X => X.habilitado_ProductoColor == true && X.id_ProductoTipo == idTipo)
                               .ToList();
                return colores;
            }
        }

        public Status Validate(Object color)
        {
            var json = JsonConvert.SerializeObject(color);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            ProductoColor _color = new ProductoColor();
            Status status = new Status();
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _color.id_ProductoColor = int.Parse(dictionary["id"]);

                _color.nombre_ProductoColor = dictionary["nombre"];
                if (string.IsNullOrWhiteSpace(_color.nombre_ProductoColor))
                {
                    status.status = false;
                    status.message = "Falta nombre del color";
                    return status;
                }

                _color.codigo_ProductoColor = dictionary["codigo"];
                if (string.IsNullOrWhiteSpace(_color.codigo_ProductoColor))
                {
                    status.status = false;
                    status.message = "Falta el codigo del color";
                    return status;
                }

                _color.id_Producto = int.Parse(dictionary["idProducto"]);
                if (_color.id_Producto == 0)
                {
                    status.status = false;
                    status.message = "Falta elegir el Producto";
                    return status;
                }

                _color.habilitado_ProductoColor = Boolean.Parse(dictionary["habilitado"]);
            }
            catch (Exception)
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.productoColor = _color;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
