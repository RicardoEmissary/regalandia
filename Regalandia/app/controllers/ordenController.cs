﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class ordenController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public Orden orden;
        }

        public Object Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                var ordenes = (from O in db.Orden
                               join C in db.Cliente on O.id_Cliente equals C.id_Cliente
                               join M in db.MetodoDePago on O.id_MetodoPago equals M.id_MetodoPago
                               select new
                               {
                                   O.id_Orden,
                                   O.total_Orden,
                                   C.nombreCompleto_Cliente,
                                   M.nombre_MetodoPago
                               }).ToList();
                return ordenes;
            }
        }

        public Orden Set(float totalOrden, int idCliente)
        {
            Orden orden = new Orden();
            orden.id_Cliente = idCliente;
            DateTime server = DateTime.UtcNow;
            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time (Mexico)");
            orden.fecha_Orden = TimeZoneInfo.ConvertTimeFromUtc(server, timeZoneInfo); ;
            orden.total_Orden = totalOrden;
            orden.id_MetodoPago = null;
            
            return orden;
        }

        public Orden Post(Orden orden)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Orden.Add(orden);
                db.SaveChanges();

                return orden;
            }
        }

        public void Put(Orden orden)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Orden.Add(orden);
                db.Entry(orden).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
