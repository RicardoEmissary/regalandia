﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class globoController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public Globo globo;
        }

        public List<Globo> Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Globo.ToList();
            }
        }

        public void Post(Globo globo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Globo.Add(globo);
                db.SaveChanges();
            }
        }

        public void Put(Globo globo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Globo.Add(globo);
                db.Entry(globo).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public Status Validate(Object globo)
        {
            var json = JsonConvert.SerializeObject(globo);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            Globo _globo = new Globo();
            Status status = new Status();
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _globo.id_Globo = int.Parse(dictionary["id"]);

                _globo.nombre_Globo = dictionary["nombre"];
                if (string.IsNullOrWhiteSpace(_globo.nombre_Globo))
                {
                    status.status = false;
                    status.message = "Falta nombre del globo";
                    return status;
                }

                _globo.multiplesColores_Globo = Boolean.Parse(dictionary["multiplesColores"]);
            }
            catch (Exception)
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.globo = _globo;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
