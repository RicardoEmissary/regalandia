﻿using AutoMapper;
using Regalandia.app.clasesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class productoSeleccionadoController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public ProductoSeleccionado productoSeleccionado;
        }

        public ProductoSeleccionado Post(ProductoSeleccionado productoSeleccionado)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                productoSeleccionado.precio_ProductoSeleccionado = db.Producto.FirstOrDefault(X => X.id_Producto == productoSeleccionado.id_Producto).precio_Producto;
                db.ProductoSeleccionado.Add(productoSeleccionado);
                db.SaveChanges();

                return productoSeleccionado;
            }
        }

        public ProductoSeleccionado ProfileMap(ProductoGeneralDto productoGeneralDto) //Para productos generales
        {
            var config = new MapperConfiguration(cfg =>
                   cfg.CreateMap<ProductoSeleccionado, ProductoGeneralDto>()
               );
            var mapper = new Mapper(config);
            var productoSeleccionado = mapper.Map<ProductoSeleccionado>(productoGeneralDto);

            return productoSeleccionado;
        }

        public ProductoSeleccionado ProfileMap(ProductoGloboDto productoGloboDto) //Para globos
        {
            var config = new MapperConfiguration(cfg =>
                   cfg.CreateMap<ProductoSeleccionado, ProductoGloboDto>()
               );
            var mapper = new Mapper(config);
            var globoSeleccionado = mapper.Map<ProductoSeleccionado>(productoGloboDto);

            return globoSeleccionado;
        }
    }
}
