﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class usuarioController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public Usuario usuario;
        }

        public List<Usuario> Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Usuario.ToList();
            }
        }

        public bool Login (string email, string pass)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                if (db.Usuario.Where(X => X.email_Usuario == email && X.password_Usuario == pass).Count() == 1)
                    return true;
                else
                    return false;
            }
        }

        public void Post(Usuario usuario)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Usuario.Add(usuario);
                db.SaveChanges();
            }
        }

        public void Put(Usuario usuario)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Usuario.Add(usuario);
                db.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public Status Validate(Object usuario)
        {
            var json = JsonConvert.SerializeObject(usuario);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            Usuario _usu = new Usuario();
            Status status = new Status();
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _usu.id_Usuario = int.Parse(dictionary["id"]);

                _usu.email_Usuario = dictionary["email"];
                if (string.IsNullOrWhiteSpace(_usu.email_Usuario))
                {
                    status.status = false;
                    status.message = "Le falta email al usuario";
                    return status;
                }

                _usu.password_Usuario = dictionary["pass"];
                if (string.IsNullOrWhiteSpace(_usu.password_Usuario))
                {
                    status.status = false;
                    status.message = "Le falta password al usuario";
                    return status;
                }
            }
            catch (Exception E)
            {
                Console.WriteLine(E);
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.usuario = _usu;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
