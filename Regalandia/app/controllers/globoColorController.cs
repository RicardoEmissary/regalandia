﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class globoColorController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public GloboColor globoColor;
        }

        public Object Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                var colores = (from gc in db.GloboColor
                               join g in db.Globo on gc.id_Globo equals g.id_Globo
                               select new
                               {
                                   gc.id_GloboColor,
                                   gc.nombre_GloboColor,
                                   gc.codigo_GloboColor,
                                   gc.habilitado_GloboColor,
                                   gc.id_Globo,
                                   g.nombre_Globo
                               }).ToList();
                return colores;
            }
        }

        public Object GetGloboColor()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                var globoColor = (from GC in db.GloboColor
                                  where GC.habilitado_GloboColor == true
                                  select new 
                                  {
                                      GC.id_GloboColor,
                                      GC.nombre_GloboColor,
                                      GC.codigo_GloboColor,
                                      GC.id_Globo
                                  }).ToList();
                
                return globoColor;
            }
        }

        public void Post(GloboColor color)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.GloboColor.Add(color);
                db.SaveChanges();
            }
        }

        public void Put(GloboColor color)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.GloboColor.Add(color);
                db.Entry(color).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public Status Validate(Object color)
        {
            var json = JsonConvert.SerializeObject(color);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            GloboColor _color = new GloboColor();
            Status status = new Status();
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _color.id_GloboColor = int.Parse(dictionary["id"]);

                _color.nombre_GloboColor = dictionary["nombre"];
                if (string.IsNullOrWhiteSpace(_color.nombre_GloboColor))
                {
                    status.status = false;
                    status.message = "Falta nombre del color";
                    return status;
                }
                _color.codigo_GloboColor = dictionary["codigo"];
                if (string.IsNullOrWhiteSpace(_color.codigo_GloboColor))
                {
                    status.status = false;
                    status.message = "Falta elegir el color";
                    return status;
                }

                _color.habilitado_GloboColor = Boolean.Parse(dictionary["habilitado"]);

                _color.id_Globo = int.Parse(dictionary["idGlobo"]);
                if(_color.id_Globo <= 0)
                {
                    status.status = false;
                    status.message = "Falta elegir el globo";
                    return status;
                }
            }
            catch (Exception)
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.globoColor = _color;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
