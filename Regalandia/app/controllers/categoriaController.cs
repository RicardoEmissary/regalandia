﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class categoriaController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public Categoria categoria;
        }

        public List<Categoria> Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Categoria.ToList();
            }
        }

        public List<Categoria> getCategoriaById(int id)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Categoria.Where(X => X.id_Categoria == id).ToList();
            }
        }

        public void Post(Categoria categoria)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Categoria.Add(categoria);
                db.SaveChanges();
            }
        }

        public void Put(Categoria categoria)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Categoria.Add(categoria);
                db.Entry(categoria).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public Status Validate(Object categoria)
        {
            var json = JsonConvert.SerializeObject(categoria);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            Categoria _cate = new Categoria();
            Status status = new Status();
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _cate.id_Categoria = int.Parse(dictionary["id"]);

                _cate.nombre_Categoria = dictionary["nombre"];
                if (string.IsNullOrWhiteSpace(_cate.nombre_Categoria))
                {
                    status.status = false;
                    status.message = "Le falta nombre a la categoria";
                    return status;
                }

                _cate.habilitado_Categoria = Boolean.Parse(dictionary["habilitado"]);
            }
            catch (Exception)
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.categoria = _cate;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
