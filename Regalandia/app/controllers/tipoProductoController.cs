﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class tipoProductoController : ApiController
    {
        public struct Status
        {
            public bool status;
            public string message;
            public ProductoTipo productoTipo;
        }

        public List<ProductoTipo> Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.ProductoTipo.ToList();
            }
        }

        public List<ProductoTipo> getTipoById(int id)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.ProductoTipo.Where(X => X.id_ProductoTipo == id).ToList();
            }
        }

        public void Post(ProductoTipo productoTipo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.ProductoTipo.Add(productoTipo);
                db.SaveChanges();
            }
        }

        public void Put(ProductoTipo productoTipo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.ProductoTipo.Add(productoTipo);
                db.Entry(productoTipo).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public Status Validate(Object tipo)
        {
            var json = JsonConvert.SerializeObject(tipo);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            ProductoTipo _tipo = new ProductoTipo();
            Status status = new Status();
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _tipo.id_ProductoTipo = int.Parse(dictionary["id"]);

                _tipo.nombre_ProductoTipo = dictionary["nombre"];
                if (string.IsNullOrWhiteSpace(_tipo.nombre_ProductoTipo))
                {
                    status.status = false;
                    status.message = "Le falta nombre a la categoria";
                    return status;
                }

                _tipo.habilitado_ProductoTipo = Boolean.Parse(dictionary["habilitado"]);
            }
            catch (Exception)
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.productoTipo = _tipo;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
