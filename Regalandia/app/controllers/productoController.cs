﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class productoController : ApiController
    {
        private Producto _prod;
        public struct Status
        {
            public bool status;
            public string message;
            public Producto producto;
        }

        public List<Producto> getProductoById(int id)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return db.Producto.Where(X => X.id_Producto == id).ToList();
            }
        }

        public Object Get()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var productos = (from P in db.Producto
                                 join T in db.ProductoTipo on P.id_ProductoTipo equals T.id_ProductoTipo
                                 select new
                                 {
                                     P.id_Producto,
                                     P.nombre_Producto,
                                     P.descripcion_Producto,
                                     P.precio_Producto,
                                     P.foto1_Producto,
                                     P.foto2_Producto,
                                     P.foto3_Producto,
                                     P.foto4_Producto,
                                     P.habilitado_Producto,
                                     P.id_Tamanio,
                                     P.id_ProductoTipo,
                                     T.nombre_ProductoTipo,
                                     P.id_Categoria,
                                     P.sku_Producto
                                 }).Where(X => X.id_ProductoTipo != 4).ToList();
                return productos;
            }
        }

        public Object GetByTipo(int idTipo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var productos = (from P in db.Producto
                                 join T in db.ProductoTipo on P.id_ProductoTipo equals T.id_ProductoTipo
                                 select new
                                 {
                                     P.id_Producto,
                                     P.nombre_Producto,
                                     P.descripcion_Producto,
                                     P.precio_Producto,
                                     P.foto1_Producto,
                                     P.foto2_Producto,
                                     P.foto3_Producto,
                                     P.foto4_Producto,
                                     P.habilitado_Producto,
                                     P.id_Tamanio,
                                     P.id_ProductoTipo,
                                     T.nombre_ProductoTipo,
                                     P.id_Categoria,
                                     P.sku_Producto
                                 }).Where(X => X.id_ProductoTipo == idTipo).ToList();
                return productos;
            }
        }

        public void Post(Producto producto)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Producto.Add(producto);
                db.SaveChanges();
            }
        }

        public void Put(Producto producto)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Producto.Add(producto);
                db.Entry(producto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public int CountRegalos()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                return db.Producto.Count();
            }
        }

        private string saveImage(int id, string base64Img, string oldNameImg)
        {
            try
            {
                var GuidImagen = Guid.NewGuid();
                byte[] imageBytes = null;
                string name = string.Empty;

                name = id + "_" + GuidImagen.GetHashCode() + ".png";
                imageBytes = Convert.FromBase64String(base64Img);
                File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + "\\app\\multimedia\\" + name, imageBytes);
                if (!string.IsNullOrWhiteSpace(oldNameImg))
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "\\app\\multimedia\\" + oldNameImg);
                return name;
            }
            catch (Exception)
            {
                return "Error";
            }
        }

        public Object getAllProductos()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var regalos = (from p in db.Producto
                               join t in db.Tamanio on p.id_Tamanio equals t.id_Tamanio
                               join c in db.Categoria on p.id_Categoria equals c.id_Categoria
                               select new
                               {
                                   p.id_Producto,
                                   p.nombre_Producto,
                                   p.descripcion_Producto,
                                   p.precio_Producto,
                                   p.foto1_Producto,
                                   p.foto2_Producto,
                                   p.foto3_Producto,
                                   p.foto4_Producto,
                                   p.sku_Producto,
                                   p.habilitado_Producto,
                                   p.id_Tamanio,
                                   t.nombre_Tamanio,
                                   p.id_Categoria,
                                   c.nombre_Categoria,
                                   p.id_ProductoTipo
                               })
                                .Where(X => X.habilitado_Producto == true && X.id_ProductoTipo != 3)
                                .ToList();
                return regalos;
            }
        }

        public Object getNuevosProductos()
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var regalos = (from p in db.Producto
                               join t in db.Tamanio on p.id_Tamanio equals t.id_Tamanio
                               join c in db.Categoria on p.id_Categoria equals c.id_Categoria
                               select new
                               {
                                   p.id_Producto,
                                   p.nombre_Producto,
                                   p.descripcion_Producto,
                                   p.precio_Producto,
                                   p.foto1_Producto,
                                   p.foto2_Producto,
                                   p.foto3_Producto,
                                   p.foto4_Producto,
                                   p.sku_Producto,
                                   p.habilitado_Producto,
                                   p.id_Tamanio,
                                   t.nombre_Tamanio,
                                   p.id_Categoria,
                                   c.nombre_Categoria,
                                   p.id_ProductoTipo
                               })
                                .Where(X => X.habilitado_Producto == true)
                                .Take(6)
                                .OrderByDescending(Y => Y.id_Producto)
                                .ToList();
                return regalos;
            }
        }

        public Object getAllProductosByTipo(long idTipo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var regalos = (from p in db.Producto
                               join t in db.Tamanio on p.id_Tamanio equals t.id_Tamanio
                               join c in db.Categoria on p.id_Categoria equals c.id_Categoria
                               select new
                               {
                                   p.id_Producto,
                                   p.nombre_Producto,
                                   p.descripcion_Producto,
                                   p.precio_Producto,
                                   p.foto1_Producto,
                                   p.foto2_Producto,
                                   p.foto3_Producto,
                                   p.foto4_Producto,
                                   p.sku_Producto,
                                   p.habilitado_Producto,
                                   p.id_Tamanio,
                                   t.nombre_Tamanio,
                                   p.id_Categoria,
                                   c.nombre_Categoria,
                                   p.id_ProductoTipo
                               })
                                .Where(X => X.habilitado_Producto == true && X.id_ProductoTipo == idTipo)
                                .ToList();
                return regalos;
            }
        }

        public Status Validate(Object producto)
        {
            var json = JsonConvert.SerializeObject(producto);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            _prod = new Producto();
            Status status = new Status();
            int idPost = 0;
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _prod.id_Producto = int.Parse(dictionary["id"]);
                else
                    idPost = CountRegalos() + 1;

                _prod.nombre_Producto = dictionary["nombre"];
                if (string.IsNullOrWhiteSpace(_prod.nombre_Producto))
                {
                    status.status = false;
                    status.message = "Le falta nombre al producto";
                    return status;
                }
                _prod.sku_Producto = dictionary["sku"];
                if (string.IsNullOrWhiteSpace(_prod.sku_Producto))
                {
                    status.status = false;
                    status.message = "Le falta sku al producto";
                    return status;
                }
                if (dictionary["tamanio"] == null)
                    _prod.id_Tamanio = null;
                else
                _prod.id_Tamanio = int.Parse(dictionary["tamanio"]);
                
                _prod.descripcion_Producto = dictionary["descripcion"];
                if (string.IsNullOrWhiteSpace(_prod.descripcion_Producto))
                {
                    status.status = false;
                    status.message = "Le falta descripcion al producto";
                    return status;
                }
                _prod.precio_Producto = float.Parse(dictionary["precio"]);
                if (_prod.precio_Producto == null)
                {
                    status.status = false;
                    status.message = "Le falta precio al producto";
                    return status;
                }

                if (dictionary["nameImg1"] != "no image")
                {
                    int id = (_prod.id_Producto > 0) ? _prod.id_Producto : idPost;
                    _prod.foto1_Producto = saveImage(id, dictionary["base64Img1"], dictionary["oldNameImg1"]);
                    if (_prod.foto1_Producto == "Error")
                    {
                        status.status = false;
                        status.message = "Fallo la carga del la Imagen 1";
                        return status;
                    }
                }
                else
                    _prod.foto1_Producto = dictionary["oldNameImg1"];

                if (dictionary["nameImg2"] != "no image")
                {
                    int id = (_prod.id_Producto > 0) ? _prod.id_Producto : idPost;
                    _prod.foto2_Producto = saveImage(id, dictionary["base64Img2"], dictionary["oldNameImg2"]);
                    if (_prod.foto2_Producto == "Error")
                    {
                        status.status = false;
                        status.message = "Fallo la carga del la Imagen 2";
                        return status;
                    }
                }
                else
                    _prod.foto2_Producto = dictionary["oldNameImg2"];

                if (dictionary["nameImg3"] != "no image")
                {
                    int id = (_prod.id_Producto > 0) ? _prod.id_Producto : idPost;
                    _prod.foto3_Producto = saveImage(id, dictionary["base64Img3"], dictionary["oldNameImg3"]);
                    if (_prod.foto3_Producto == "Error")
                    {
                        status.status = false;
                        status.message = "Fallo la carga del la Imagen 3";
                        return status;
                    }
                }
                else
                    _prod.foto3_Producto = dictionary["oldNameImg3"];

                if (dictionary["nameImg4"] != "no image")
                {
                    int id = (_prod.id_Producto > 0) ? _prod.id_Producto : idPost;
                    _prod.foto4_Producto = saveImage(id, dictionary["base64Img4"], dictionary["oldNameImg4"]);
                    if (_prod.foto4_Producto == "Error")
                    {
                        status.status = false;
                        status.message = "Fallo la carga del la Imagen 4";
                        return status;
                    }
                }
                else
                    _prod.foto4_Producto = dictionary["oldNameImg4"];

                _prod.id_Categoria = int.Parse(dictionary["idCategoria"]);
                if (_prod.id_Categoria == null || _prod.id_Categoria == 0)
                {
                    status.status = false;
                    status.message = "Le falta Categoria al producto";
                    return status;
                }

                _prod.id_ProductoTipo = int.Parse(dictionary["idTipo"]);
                if (_prod.id_ProductoTipo == null || _prod.id_ProductoTipo == 0)
                {
                    status.status = false;
                    status.message = "Le falta tipo al producto";
                    return status;
                }

                _prod.habilitado_Producto = Boolean.Parse(dictionary["habilitado"]);
            }
            catch
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.producto = _prod;
            status.message = "Todos los datos estan correctos";
            return status;
        }

        public Status ValidateStatusUpdate(Object producto)
        {
            var json = JsonConvert.SerializeObject(producto);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            _prod = new Producto();
            Status status = new Status();
            int idPost = 0;
            try
            {
                if (int.Parse(dictionary["id"]) > 0)
                    _prod.id_Producto = int.Parse(dictionary["id"]);
                else
                    idPost = CountRegalos();

                _prod.nombre_Producto = dictionary["nombre"];
                if (string.IsNullOrWhiteSpace(_prod.nombre_Producto))
                {
                    status.status = false;
                    status.message = "Le falta nombre al producto";
                    return status;
                }
                _prod.sku_Producto = dictionary["sku"];
                if (string.IsNullOrWhiteSpace(_prod.sku_Producto))
                {
                    status.status = false;
                    status.message = "Le falta sku al producto";
                    return status;
                }
                if (dictionary["tamanio"] == null)
                    _prod.id_Tamanio = null;
                else
                    _prod.id_Tamanio = int.Parse(dictionary["tamanio"]);

                _prod.descripcion_Producto = dictionary["descripcion"];
                if (string.IsNullOrWhiteSpace(_prod.descripcion_Producto))
                {
                    status.status = false;
                    status.message = "Le falta descripcion al producto";
                    return status;
                }
                _prod.precio_Producto = float.Parse(dictionary["precio"]);
                if (_prod.precio_Producto == null)
                {
                    status.status = false;
                    status.message = "Le falta precio al regalo";
                    return status;
                }
                _prod.foto1_Producto = dictionary["oldNameImg1"];
                _prod.foto2_Producto = dictionary["oldNameImg2"];
                _prod.foto3_Producto = dictionary["oldNameImg3"];
                _prod.foto4_Producto = dictionary["oldNameImg4"];

                _prod.id_Categoria = int.Parse(dictionary["idCategoria"]);
                if (_prod.id_Categoria == null || _prod.id_Categoria == 0)
                {
                    status.status = false;
                    status.message = "Le falta Categoria al producto";
                    return status;
                }
                _prod.id_ProductoTipo = int.Parse(dictionary["idTipo"]);
                if (_prod.id_ProductoTipo == null || _prod.id_ProductoTipo == 0)
                {
                    status.status = false;
                    status.message = "Le falta Tipo al producto";
                    return status;
                }

                _prod.habilitado_Producto = Boolean.Parse(dictionary["habilitado"]);
            }
            catch
            {
                status.status = false;
                status.message = "Algun campo no esta completado. Favor de llenarlo";
                return status;
            }

            status.status = true;
            status.producto = _prod;
            status.message = "Todos los datos estan correctos";
            return status;
        }
    }
}
