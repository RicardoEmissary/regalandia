﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class globoColorSeleccionadoController : ApiController
    {
        public void Post (int idProductoSeleccionado, string codigoColor, int idGlobo)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                GloboColorSeleccionado globoColorSeleccionado = new GloboColorSeleccionado();
                globoColorSeleccionado.id_ProductoSeleccionado = idProductoSeleccionado;
                globoColorSeleccionado.codigo_GloboColorSeleccionado = codigoColor;
                globoColorSeleccionado.id_Globo = idGlobo;

                db.GloboColorSeleccionado.Add(globoColorSeleccionado);
                db.SaveChanges();
            }
        }
    }
}
