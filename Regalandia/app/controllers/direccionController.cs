﻿using AutoMapper;
using Regalandia.app.clasesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Regalandia.app.controllers
{
    public class direccionController : ApiController
    {
        public Direccion Post (Direccion direccion)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                Direccion direccionDB = db.Direccion.FirstOrDefault(X =>
                X.calle_Direccion == direccion.calle_Direccion &&
                X.numeroExterior_Direccion == direccion.numeroExterior_Direccion &&
                X.colonia_Direccion == direccion.colonia_Direccion &&
                X.codigoPostal_Direccion == direccion.codigoPostal_Direccion &&
                X.ciudad_Direccion == direccion.ciudad_Direccion &&
                X.estado_Direccion == direccion.estado_Direccion);
                if(direccionDB == null)
                {
                    db.Direccion.Add(direccion);
                    db.SaveChanges();
                    return direccion;
                }
                if(direccionDB.nombreCompleto_Direccion != direccion.nombreCompleto_Direccion || direccionDB.telefono_Direccion != direccion.telefono_Direccion || direccionDB.correo_Direccion != direccion.correo_Direccion)
                {
                    direccion.id_Direccion = direccionDB.id_Direccion;
                    Put(direccion);
                    return direccion;
                }
                
                return direccionDB;
            }
        }

        public Direccion Put (Direccion direccion)
        {
            using (regalandiaEntities db = new regalandiaEntities())
            {
                db.Direccion.Add(direccion);
                db.Entry(direccion).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return direccion;
            }
        }

        public Direccion ProfileMap(DireccionDto direccionDto)
        {
            var config = new MapperConfiguration(cfg =>
                   cfg.CreateMap<Direccion, DireccionDto>()
               );
            var mapper = new Mapper(config);
            var direccion = mapper.Map<Direccion>(direccionDto);

            return direccion;
        }
    }
}
