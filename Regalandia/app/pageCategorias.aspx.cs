﻿using Newtonsoft.Json;
using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia.app
{
    public partial class pageCategorias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(HttpContext.Current.Session["usuario"].ToString()) || HttpContext.Current.Session["usuario"].ToString() == null)
            {
                Response.Redirect("~/app/login.aspx");
            }
        }
        //CATEGORIAS
        [WebMethod(EnableSession = true)]
        public static Object GetCategorias()
        {
            using (categoriaController controller = new categoriaController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //INSERT
        [WebMethod(EnableSession = true)]
        public static string postCategoria(Object categoria)
        {
            using (categoriaController controller = new categoriaController())
            {
                try
                {
                    categoriaController.Status _object = controller.Validate(categoria);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.categoria);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar la categoria"
                    });
                }
                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE
        [WebMethod(EnableSession = true)]
        public static string putCategoria(Object categoria)
        {
            using (categoriaController controller = new categoriaController())
            {
                try
                {
                    dynamic _categoria = controller.Validate(categoria);
                    if (!_categoria.status)
                        return JsonConvert.SerializeObject(_categoria);
                    controller.Put(_categoria.categoria);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar la categoria"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //-----------------------------------------------------------------------

        //TIPOS
        [WebMethod(EnableSession = true)]
        public static Object GetTiposProductos()
        {
            using (tipoProductoController controller = new tipoProductoController())
            {
                try
                {
                    return controller.Get();
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //INSERT
        [WebMethod(EnableSession = true)]
        public static string postTipo(Object tipo)
        {
            using (tipoProductoController controller = new tipoProductoController())
            {
                try
                {
                    tipoProductoController.Status _object = controller.Validate(tipo);
                    if (!_object.status)
                        return JsonConvert.SerializeObject(_object);

                    controller.Post(_object.productoTipo);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el tipo"
                    });
                }
                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //UPDATE
        [WebMethod(EnableSession = true)]
        public static string putTipo(Object tipo)
        {
            using (tipoProductoController controller = new tipoProductoController())
            {
                try
                {
                    dynamic _tipo = controller.Validate(tipo);
                    if (!_tipo.status)
                        return JsonConvert.SerializeObject(_tipo);
                    controller.Put(_tipo.productoTipo);
                }
                catch (Exception E)
                {
                    System.Diagnostics.Debug.WriteLine(E);
                    return JsonConvert.SerializeObject(new
                    {
                        status = false,
                        message = "Ocurrio un error al guardar el tipo"
                    });
                }

                return JsonConvert.SerializeObject(new
                {
                    status = true,
                    message = "Guardado con exito"
                });
            }
        }

        //Cerrar sesion
        [WebMethod(EnableSession = true)]
        public static string CloseSession()
        {
            HttpContext.Current.Session.Clear();
            string path = HttpContext.Current.Request.UrlReferrer.AbsolutePath;
            string location = "pageLogin.aspx";
            return JsonConvert.SerializeObject(new
            {
                status = true,
                location = location
            });
        }

    }
}