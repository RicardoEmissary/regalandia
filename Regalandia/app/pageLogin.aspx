﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pageLogin.aspx.cs" Inherits="Regalandia.app.pageLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Regalandia</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styleapp.css"/>
    <style>
        body{
            background-color: #f1b144;
        }
    </style>
</head>
<body>
    <div class="row" style="background-color: #B41064">
        <div class="col-6 mx-auto my-5">
            <h1>Administracion Regalandia</h1>
            <div class="form-group">
                <label for="txtEmail">Usuario</label>
                <input type="email" class="form-control" id="txtEmail" placeholder="Correo electronico"/>
            </div>
            <div class="form-group">
                <label for="txtPass">Contraseña</label>
                <input type="password" class="form-control" id="txtPass" placeholder="Password"/>
            </div>
            <button type="submit" id="btnEntrar" class="btn btn-primary float-right">Entrar</button>
        </div>
    </div>

    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> 
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="js/pageLogin.js"></script>
</body>
</html>
