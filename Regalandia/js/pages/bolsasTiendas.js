import { Tiendas } from "../assets/tiendas.js";
import { Subcategoria } from "../models/subcategoria.js";

import { Producto } from "../models/producto.js";




function bolsasTiendasMain(){

    const subcategoriasTemplate = document.querySelector("#articulos-envolturas").innerHTML;

    const envolturasTemplate = document.querySelector("#modalEnvolturasTemplate").innerHTML;

    const modalContainer = document.querySelector("#modal-product-container");
    const modalTemplate = document.querySelector("#modalProductTemplate").innerHTML;

    let envolturasRaw = []; //arreglo para guardar el arreglo traido del model

    const envolturas = []; //arreglo para guardar las envolturas de acuerdo al view-model

    const subcategoriasIds = []; //guarda los ids de las subcategorias que existan en el array de envolturas

    const subcategorias = [];

    let envolturasFlag = false;     //valida si ya han sido cargadas las envolturas


    let response = null;
    $.ajax({
        type: "POST",
        url: "bolsas.aspx/getBolsas",
        dataType: "json",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        success: function (result) {
            response = result;
        },
        error: function (err) {
            console.log("Error: ", err);
            response = "Error";
        },
        complete: function (result) {

            envolturasRaw = response.d;

            for (const i of envolturasRaw) {

                if (!subcategoriasIds.includes(i.id_Categoria)) {

                    subcategoriasIds.push(i.id_Categoria);

                    const subcategoria = new Subcategoria();

                    subcategoria.id = i.id_Categoria;

                    subcategoria.nombre = i.nombre_Categoria;

                    subcategorias.push(subcategoria);

                }

                
                const producto = new Producto();

                producto.categoria = i.id_Categoria;
                producto.descripcion = i.descripcion_Producto;
                producto.foto1 = i.foto1_Producto;
                producto.foto2 = i.foto2_Producto;
                producto.foto3 = i.foto3_Producto;
                producto.foto4 = i.foto4_Producto;
                producto.id = i.id_Producto;
                producto.nombre = i.nombre_Producto;
                producto.precio = i.precio_Producto;
                producto.tamano = i.id_Tamanio;

                console.log(producto.tamano);

                envolturas.push(producto);


            }

            envolturasFlag = true;

        }




    });


    const intervalID = setInterval(()=>{

        if(envolturasFlag){

            clearInterval(intervalID);

            const tiendaHandler =
                new Tiendas(1,
                    document.querySelector("main#main-tienda-container"),
                    subcategoriasTemplate,
                    subcategorias,
                    envolturasTemplate,
                    envolturas,
                    modalTemplate,
                    modalContainer,
                    1);

        }

    },500);

}

bolsasTiendasMain();
