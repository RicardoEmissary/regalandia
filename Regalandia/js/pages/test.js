﻿import { ModalProductInfoHandler, DataButton } from "../assets/modalProducto/DataButton/dataButton.js";
import { Producto } from "../models/producto.js";

const main = () => {

    const buttonElements = [document.querySelector('#button-prueba0'), document.querySelector('#button-prueba1'), document.querySelector('#button-prueba2')];

    const buttons = [];

    let producto = new Producto();
    producto.id = 0;
    producto.nombre = 'bolsa hola';
    producto.descripcion = 'lorem ipsum dolor sit amet';
    producto.precio = 75.54;
    producto.foto1 = 'https://images.unsplash.com/photo-1586348943529-beaae6c28db9?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=658&q=80';
    producto.foto2 = 'https://images.unsplash.com/photo-1586348943529-beaae6c28db9?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=658&q=80';
    producto.foto3 = 'https://images.unsplash.com/photo-1586348943529-beaae6c28db9?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=658&q=80';
    producto.foto4 = 'https://images.unsplash.com/photo-1586348943529-beaae6c28db9?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=658&q=80';
    producto.categoria = 0;
    producto.tamano = 1;
   

    buttons.push(new DataButton(buttonElements[0], producto, 'A'));

    producto = null;
    producto = new Producto();
    producto.id = 0;
    producto.nombre = 'bolsa adios';
    producto.descripcion = 'lorem ipsum dolor sit amet';
    producto.precio = 34.546;
    producto.foto1 = 'https://images.unsplash.com/photo-1433086966358-54859d0ed716?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';
    producto.foto2 = 'https://images.unsplash.com/photo-1433086966358-54859d0ed716?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';
    producto.foto3 = 'https://images.unsplash.com/photo-1433086966358-54859d0ed716?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';
    producto.foto4 = 'https://images.unsplash.com/photo-1433086966358-54859d0ed716?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';
    producto.categoria = 0;
    producto.tamano = 1;

    buttons.push(new DataButton(buttonElements[1], producto, 'B'));

    producto = null;
    producto = new Producto();
    producto.id = 0;
    producto.nombre = 'bolsa bye';
    producto.descripcion = 'lorem ipsum dolor sit amet';
    producto.precio = 67.046;
    producto.foto1 = 'https://images.unsplash.com/photo-1546514355-7fdc90ccbd03?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';
    producto.foto2 = 'https://images.unsplash.com/photo-1546514355-7fdc90ccbd03?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';
    producto.foto3 = 'https://images.unsplash.com/photo-1546514355-7fdc90ccbd03?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';
    producto.foto4 = 'https://images.unsplash.com/photo-1546514355-7fdc90ccbd03?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';
    producto.categoria = 0;
    producto.tamano = 1;

    buttons.push(new DataButton(buttonElements[2], producto, 'C'));
    

    const modalProductsHandler = new ModalProductInfoHandler(buttons, document.querySelector('.modal-producto-container'));


    
};


main();