import { ModalLugarHandler } from "../assets/entregas/paso1/modal-eleccion-lugar.js";
import {Pasos} from "../assets/entregas/pasos.js";
import {ConfirmarFase1} from "../assets/entregas/paso1/confirmar-fase1.js";
import {StartCalendar} from "../date-picker.js";
import {TemplateHandler} from "../assets/templateHandlebars.js";
import {RegalosTable} from "../assets/entregas/paso2/regalos-table.js";
import {ModalRegaloHandler} from "../assets/entregas/paso2/modal-regalo.js";
import {TiendasEntregas} from "../assets/entregas/paso2/tiendas-entregas.js";



function EntregasMain() {

    const templateHandler = new TemplateHandler();

    StartCalendar();

    let regalos = [];
    let globos = [];

    let fase1Completada = false;

    const buttonCasa = document.querySelector("#button-casa");
    const buttonTienda = document.querySelector("#button-tienda");

    const modalRecoleccionCasa = document.querySelector("#modalRecoleccionCasa");
    const modalRecoleccionTienda = document.querySelector("#modalRecoleccionTienda");

    //Botones para cerrar modal
    const closeButtons = [
        document.querySelector("#close-ModalCasa"),
        document.querySelector("#close-ModalTienda")
    ];

    //Inicializa el navbar responsive
    InitializeNavbar();

    //Inicializa las ventanas modales de elección de lugar de recogida y su manejador.
    const modalLugarHandler =
        new ModalLugarHandler(buttonCasa,
                            buttonTienda,
                            modalRecoleccionCasa,
                            modalRecoleccionTienda,
                            closeButtons[0],
                            closeButtons[1]);

    const pasos = new Pasos();
    pasos.changePaso(0);

    document.addEventListener("fase1Confirmada", ($e)=>{

        regalos = $e.detail.regalosArray;

        templateHandler.desplegarTemplate(document.querySelector("#container-regalos"),
            document.querySelector("#paso2TemplateTienda").innerHTML,
            {regalos: regalos});

        const rT = new RegalosTable(regalos);

        const tiendasEntregas = new TiendasEntregas(regalos);

        pasos.changePaso(1);

    });

    const fase1Confirmada = new CustomEvent('fase1Confirmada', {'detail':{

        regalosArray: []

        }});

    //objeto que inicializa los botones de confirmar de la fase1
    const confirmarFase1 = new ConfirmarFase1(fase1Confirmada);







    document.querySelector("#conf-tabla-paso2").addEventListener("click",()=>{

        pasos.changePaso(2);

    });

    document.querySelector("#paso3Next").addEventListener("click",()=>{

        pasos.changePaso(3);

    });

    document.querySelector("#paso4Next").addEventListener("click",()=>{

        pasos.changePaso(4);

    });



}

EntregasMain();
