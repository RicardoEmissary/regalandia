import { CarouselNewer } from "../assets/carousel-newer.js";
import { CarouselBG } from "../assets/Carousel-Index.js";
import { UltimosArticulos } from "../assets/ultimosArticulos.js";
import { LoadingSpinner } from "../assets/loading-spinner.js";



//Inicializa el modal de consultar zona y sus botones
function InitializeModalZonas(){

    var modal = document.getElementById("modalZonas");
    var span = document.getElementsByClassName("close-purple")[0];

    let carouselViewport = document.querySelector('ol#vport');
    let seccionesCarousel = carouselViewport.querySelectorAll('li');

    for (let seccion of seccionesCarousel){
        seccion.style.display = 'none';
    }

    seccionesCarousel[0].style.display = 'block';

    let carousel1Buttons = seccionesCarousel[0].querySelectorAll('a');
    let carousel2Buttons = seccionesCarousel[1].querySelectorAll('a');
    let carousel3Buttons = seccionesCarousel[2].querySelectorAll('a');

    let carouselButtonsBottom = document.querySelectorAll('.carousel__navigation-list a');



    for(let btn of carousel2Buttons){

        btn.style.display = 'none';

    }
    for(let btn of carousel3Buttons){

        btn.style.display = 'none';

    }
    for(let btn of carousel1Buttons){

        btn.style.display = 'block';

    }

    carouselButtonsBottom[0].addEventListener('click', ()=>{

        for(let btn of carousel2Buttons){

            btn.style.display = 'none';

        }
        for(let btn of carousel3Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel1Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[0].style.display = 'block';

        seccionesCarousel[1].style.display = 'none';
        seccionesCarousel[2].style.display = 'none';

    })


    carouselButtonsBottom[1].addEventListener('click', ()=>{

        for(let btn of carousel1Buttons){

            btn.style.display = 'none';

        }
        for(let btn of carousel3Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel2Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[0].style.display = 'none';
        seccionesCarousel[2].style.display = 'none';

        seccionesCarousel[1].style.display = 'block';
    })

    carouselButtonsBottom[2].addEventListener('click', ()=>{
        for(let btn of carousel1Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel2Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel3Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[0].style.display = 'none';
        seccionesCarousel[1].style.display = 'none';

        seccionesCarousel[2].style.display = 'block';

    })

    carousel1Buttons[0].addEventListener('click', ()=>{

        for(let btn of carousel1Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel3Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[0].style.display = 'none';

        seccionesCarousel[2].style.display = 'block';

    })

    carousel1Buttons[1].addEventListener('click', ()=>{

        for(let btn of carousel1Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel2Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[0].style.display = 'none';

        seccionesCarousel[1].style.display = 'block';

    })

    carousel1Buttons[0].addEventListener('click', ()=>{

        for(let btn of carousel1Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel3Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[0].style.display = 'none';

        seccionesCarousel[2].style.display = 'block';

    })

    carousel2Buttons[0].addEventListener('click', ()=>{

        for(let btn of carousel2Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel1Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[1].style.display = 'none';

        seccionesCarousel[0].style.display = 'block';

    })

    carousel2Buttons[1].addEventListener('click', ()=>{

        for(let btn of carousel2Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel3Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[1].style.display = 'none';

        seccionesCarousel[2].style.display = 'block';

    })

    carousel3Buttons[0].addEventListener('click', ()=>{

        for(let btn of carousel3Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel2Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[2].style.display = 'none';

        seccionesCarousel[1].style.display = 'block';

    })

    carousel3Buttons[1].addEventListener('click', ()=>{

        for(let btn of carousel3Buttons){

            btn.style.display = 'none';

        }

        for(let btn of carousel1Buttons){

            btn.style.display = 'block';

        }

        seccionesCarousel[2].style.display = 'none';

        seccionesCarousel[0].style.display = 'block';

    })

    $("#btn-sm-consultar-zonas").click(function() {
        modal.style.display = "block";
    })

    $("#btn-xl-consultar-zonas").click(function() {
        modal.style.display = "block";
    })

    $("#btn-close-modal").click(function() {
        modal.style.display = "none";
    })

    span.onclick = function() {
        modal.style.display = "none";
    }


    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }
}

function InitializeCarouselBanner(){

    let container = document.querySelector('.section-index-1__img');

    let pictures = [];

    let buttons = document.querySelectorAll('div.carButtons button');

    let headers = document.querySelectorAll('.section-index-1 h1');

    let anchor = document.querySelectorAll('.section-index-1 a');


    //Debería haber el mismo número de imágenes que de botones
    pictures[0] = './img/02.png';
    pictures[1] = "https://images.unsplash.com/photo-1549465220-1a8b9238cd48?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1324&q=80";
    pictures[2] = "https://images.unsplash.com/photo-1529244927325-b3ef2247b9fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80";
    let carousel = new CarouselBG(container, pictures, buttons, headers, anchor);

}

function InitializeCarouselNewer(){

    let itemArraySmall = document.querySelectorAll('.wrapper#carousel-small div.card')



    let buttons = document.querySelectorAll('.carousel-layout button')

    let carouselSmall = new CarouselNewer(document.querySelector('.wrapper#carousel-small'), itemArraySmall, buttons[0], buttons[1], 0)

    let itemArrayBig = document.querySelectorAll('.wrapper#carousel-big .products')

    let carouselBig = new CarouselNewer(document.querySelector('.wrapper#carousel-big'), itemArrayBig, buttons[0], buttons[1], 1)

}



//Inicializa los componentes necesarios para el desarrollo de la página. Se manda a llamar al final del documento
function IndexMain() {



    InitializeModalZonas();

    InitializeCarouselBanner();

    InitializeCarouselNewer();

    const containers = [];

    containers.push(document.querySelector("#carousel-small"));
    containers.push(document.querySelector("#big-1"));
    containers.push(document.querySelector("#big-2"));

    const template = document.querySelector("#scroll-card-container").innerHTML;

    const loadingSpinner = new LoadingSpinner(document.querySelector("body"), "loading-spinner");
    loadingSpinner.show();

    document.addEventListener("ultimosArticulos", () => {

        loadingSpinner.hide();

    });

    const llamadaHttpFinalizada = new CustomEvent("ultimosArticulos");

    const ultimosArticulos = new UltimosArticulos(containers, template, llamadaHttpFinalizada);

}



IndexMain();
