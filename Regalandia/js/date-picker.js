export function StartCalendar(){

  $(".calendar").datepicker({
    dateFormat: "dd/mm/yy",
    firstDay: 1
  });

  $(".calendar").hide();

  $(document).on("click", ".date-picker .input", function(e) {
    var $me = $(this),
      $parent = $me.parents(".date-picker");

    $parent.toggleClass("open");
    if ($parent.hasClass("open")) {
      $(".calendar").show();
    } else {
      $(".calendar").hide();
    }
  });

  $(".calendar").on("change", function() {
    var $me = $(this),
      $selected = $me.val(),
      $parent = $me.parents(".date-picker");
    $parent
      .find(".result")
      .children("span")
      .html($selected);
    $(".calendar").hide();
  });
}


export function UpdateCalendar($e){

  $($e).datepicker({
    dateFormat: "dd/mm/yy",
    firstDay: 1
  });

  $($e).hide();

  $(document).on("click", ".date-picker .input", function(e) {
    var $me = $(this),
        $parent = $me.parents(".date-picker");

    $parent.toggleClass("open");
    if ($parent.hasClass("open")) {
      $(".calendar").show();
    } else {
      $(".calendar").hide();
    }
  });

  $($e).on("change", function() {
    var $me = $(this),
        $selected = $me.val(),
        $parent = $me.parents(".date-picker");
    $parent
        .find(".result")
        .children("span")
        .html($selected);
    $(".calendar").hide();
  });
}
