﻿export class Producto {

    id; // unsigned int. PK de la base de datos 
    nombre;  // string.
    descripcion; //string
    precio; // float
    foto1; // string
    foto2;
    foto3;
    foto4;
    categoria; // string. API: nombre_Categoria
    tamano; // unsigned int. id del tamano en la bbdd

}