export class Regalo{

    nombre;
    tamano;
    direccion; //objeto de la clase Direccion declarado en ./direccion.js. Solo válido en recogida en casa
    fechaRecoleccion;  //Sólo válido en recogida en casa
    horaRecoleccion;   //Sólo válido en recogida en casa
    precio;
    tienda;   //Solo válido en recogida en tienda
    link;
    tempId; //Solo para front
    envoltura; // objeto de la clase envolturaRegalo
    id;


}
