﻿import { ModalProducto } from "../../modalProducto.js";
import { Color } from '../../opciones/radiosColores.js';

export class ModalProductoA extends ModalProducto{

    constructor(element, buttonAbrir) {
        super(element, buttonAbrir);

        this.add('selectFrase');

        this.add('combinarColores');

        this.add('cantidad');

        this.add('radiosGenero');

        const panelColores = this.add('radiosColor');

        const colores = [];

        let color = new Color();
        color.nombre = 'rojo';
        color.hexCode = '#FF6978'

        colores.push(color);

        panelColores.addRow('Fila 1', colores);

        color = new Color();
        color.nombre = 'rojo2';
        color.hexCode = 'red'

        colores.push(color);
        panelColores.addRow('Fila 2', colores);

        color = new Color();
        color.nombre = 'azul';
        color.hexCode = '#B1EDE8'

        colores.push(color);

        color = new Color();
        color.nombre = 'morado';
        color.hexCode = '#340068'

        colores.push(color);

        color = new Color();
        color.nombre = 'verde';
        color.hexCode = '#64F58D'

        colores.push(color);
        panelColores.addRow('Fila 3', colores);


    }

}