﻿import { ModalProducto } from "../../modalProducto.js";
import { Color } from '../../opciones/radiosColores.js';

export class ModalProductoB extends ModalProducto {

    constructor(element, buttonAbrir) {
        super(element, buttonAbrir);

        this.add('selectFrase');

        this.add('cantidad');



        const panelColores = this.add('radiosColor');

        const colores = [];

        let color = new Color();
        color.nombre = 'rojo';
        color.hexCode = '#FF6978'

        colores.push(color);

        panelColores.addRow('Fila 1', colores);

        color = new Color();
        color.nombre = 'rojo2';
        color.hexCode = 'red'

        colores.push(color);
        panelColores.addRow('Fila 2', colores);

        color = new Color();
        color.nombre = 'azul';
        color.hexCode = '#B1EDE8'

        colores.push(color);


    }

}