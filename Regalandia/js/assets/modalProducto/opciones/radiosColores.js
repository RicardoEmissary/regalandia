﻿import { idGenerator } from "../../id-generator.js";
import { TemplateHandler } from "../../templateHandlebars.js";

export class Color {

    nombre;
    hexCode;
    tempId;

}


/**
 * 
 * Clase para encapsular una sola fila del elemento de eleccion de color
 */
class RowColores {

      /**
 *  Crea el row con el numero de botones equivalente al numero de colores introducidos en el parametro data
 * @param  {Element} container elemento que contendra al row
 * @param  {Array<Color>} data objeto js organizado para su utilizacion con handlebars
 */
    constructor(container, data) {

        const tH = new TemplateHandler();

        // se obtiene un id string unico
        const rowId = idGenerator(Date.now() / 10000000000);

        // se agrega ese id a cada objeto de colores para nombrar los inputs radio de la misma manera
        for (const i of data.colores){

            i.tempId = rowId;
    
        }

            data.id = rowId;

        tH.desplegarSingleTemplate(container, document.querySelector('#panel-color-template').innerHTML, data);

        // una vez creado, se obtiene el elemento que contiene todo el row
        this.element = document.querySelector(`#color-row-${rowId}`);
        console.log(this.element);
       
        // se obtienen los elementos div que engloban el input oculto y el boton del color
        const options = this.element.querySelectorAll('.radio-genero-modal__opcion');

        for(const i of options){

            const input = i.querySelector('input');
            
            const button = i.querySelector('.radio-square-modal');

            for(const j of data.colores){


                if(input.value === j.nombre){
                    
                    button.style.background = j.hexCode;
                    

                }

            }

        }

        
    }
    
    element;
}


/**
 * Clase para controlar el contenedor de todo el elemento de eleccion de color
 */
export class PanelColores {

    constructor(container) {
  
        const tH = new TemplateHandler();

        this.id = idGenerator(155);

        tH.desplegarSingleTemplate(container, document.querySelector('#panel-color-container-template').innerHTML, { id: this.id });

        this.element = document.querySelector(`#panel-container-${this.id}`);

        this.row = [];

    }

    /**
 * Metodo para agregar una fila al element
 * @param  {Array<Color>} colores Array de objetos de la clase Color
 * @return {Number}  El indice del row agregado en el array rows
 */
    addRow(nombre, colores) {

        this.rows.push(new RowColores(this.element, {nombre: nombre, id: this.id,  colores: colores}));
        

    }

    id;
    element;
    rows = [];

}