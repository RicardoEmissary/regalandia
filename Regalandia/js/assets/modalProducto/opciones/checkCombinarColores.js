﻿import { TemplateHandler } from "../../templateHandlebars.js";
import {CustomCheckbox} from "../../custom-checkbox.js";

export class CheckCombinarColores {

    constructor(container) {

        const tH = new TemplateHandler();

        const template = document.querySelector('#check-combinar-colores-template').innerHTML;

        tH.desplegarSingleTemplate(container, template, {id: '45'});

        const intervalID = setInterval(() => {

            clearInterval(intervalID);
            const checkBox = new CustomCheckbox(document.querySelector('#container-check-combinar-45'));

        }, 200);


    }

}
