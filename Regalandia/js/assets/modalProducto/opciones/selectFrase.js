import { TemplateHandler } from "../../templateHandlebars.js";

export class SelectFrase {

    constructor(frases, container) {

        const template = document.querySelector('#select-frase-template').innerHTML;

        this.frases = [];

        const tH = new TemplateHandler();

        for (const i of frases) {

            this.frases.push({ frase: i });

        }

        tH.desplegarSingleTemplate(container, template, { frases: this.frases });

    }

    frases;
}