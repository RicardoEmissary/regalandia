import { InputNumberHandler } from "../CustomInputNumber.js";
import { TemplateHandler } from "../templateHandlebars.js";
import { ToggleVisibility } from "../toggleVisibility.js";
import { CheckCombinarColores } from "./opciones/checkCombinarColores.js";
import { PanelColores } from "./opciones/radiosColores.js";
import { RadiosGenero } from "./opciones/radiosGenero.js";
import { SelectFrase } from "./opciones/selectFrase.js";

/* todo agregar un objeto de producto a la clase
    Crear toda la ventana desde cero en handlebars
*/
export class ModalProducto{

    /**
    * Construye una ventana modal para el producto y le agrega la funcionalidad de toggle visibility
    * @param  {Element} element elemento HTML que contiene la ventana modal
    * @param {Producto} producto atributo para guardar la informacion del producto a mostrar
    */
    constructor(element, producto) {

        const template = document.querySelector('#modal-info-basica-template').innerHTML;

        const tH = new TemplateHandler();

        tH.desplegarTemplate(element, template, producto);
     
        this.componentsEnum = {

            selectFrase: 'selectFrase',
            cantidad: 'cantidad',
            radiosGenero: 'radiosGenero',
            combinarColores: 'combinarColores',
            radiosColor: 'radiosColor'

        };

        this.element = element;

        this.producto = producto;

        // aqui se crearan los componentes requeridos por medio de handlebars
        this.actionContainer = this.element.querySelector('.modal-producto__options-container');

        this.actionContainer.innerHTML = '';
    }


    /**
 * Agrega un componente a la ventana modal segun el identificador que se introduzca
 * @param  {string} identifier string que identifica al componente en componentsEnum
 * @return {void} Nada
 */
    add(identifier, details = null){

        switch (identifier){

            case this.componentsEnum.selectFrase: {

                const selectC = new SelectFrase(
                    ['hola', 'adios', 'bye'],

                    this.actionContainer

                );

                this.components.push(selectC);
                break;
            }

            case this.componentsEnum.cantidad: {

                const tH = new TemplateHandler();

                tH.desplegarSingleTemplate(
                    this.actionContainer,
                    document.querySelector('#cantidad-template').innerHTML,
                    { id: 'hola', label:'Cantidad'}
                );

                const intervalId = setInterval(() => {

                    clearInterval(intervalId);

                    this.components.push(new InputNumberHandler(

                        document.querySelector('#hola input[type="number"]'),
                        document.querySelectorAll('#hola button')[0],
                        document.querySelectorAll('#hola button')[1],
                        1,
                        10,
                        1


                    ));

                }, 200);
                

                break;
            }

            case this.componentsEnum.radiosGenero: {

                const radiosGenero = new RadiosGenero(this.actionContainer);

                this.components.push(radiosGenero);
                
                break;
            }

            case this.componentsEnum.combinarColores: {

                const cc = new CheckCombinarColores(this.actionContainer);

                this.components.push(cc);

                break;
            }

            case this.componentsEnum.radiosColor: {

                const cc = new PanelColores(this.actionContainer);

                this.components.push(cc);

                return cc;
            }
        }

    }

    restart() {

        this.element.innerHTML = '';

    }


    element;
    actionContainer;

    // Los componentes pueden no estar en orden de agregado
    components = [];

    producto;
}