﻿import { ModalProductoA } from "../tiposModales/modalProductoA/modalProductoA.js";
import { ModalProductoB } from "../tiposModales/modalProductoB/modalProductoB.js";
import { ModalProductoC } from "../tiposModales/modalProductoC/modalProductoC.js";


/**
 * 
 * Clase para encapsular un elemento de boton con una informacion especifica para la modal
 * 
 * */
export class DataButton{

    /**
    * Crea una abstraccion de un boton vinculada a informacion para el despliegue de la ventana modal
    * @param  {Element} buttonElement elemento HTML que contiene el boton
    * @param  {any} data informacion vinculada
    * @param  {string} type identificador del tipo de ventana modal a mostrar
    */
    constructor(buttonElement, data, type) {

        this.buttonElement = buttonElement;
        this.data = data;
        this.type = type;
        
    }

    buttonElement;
    data;
    type;

}


/**
 *
 * Clase para manejar la informacion de diferentes productos en la ventana modal de la pagina
 *
 * */
export class ModalProductInfoHandler {

    /**
    * Construye una ventana modal para el producto y le agrega la funcionalidad de toggle visibility
    * 
    * Agrega la funcionalidad de abrir y cerrar modal creando y destruyendo cada que se abre una nueva
    * 
    * @param  {Array<Element> | NodeListOf<Element>} openButtons encapsulacion de botones para abrir junto con la info que deben mostrar
    * @param  {Element} element elemento contenedor de la ventana modal
    */

    constructor(openButtons, element) {

        // funcionalidad de boton de cerrar

        element.querySelector('.modal-producto__close-button')
            .addEventListener('click', () => {

                element.style.display = 'none';


            });

        this.openButtons = openButtons;

        for (const button of this.openButtons) {

            button.buttonElement.addEventListener('click', () => {

                switch (button.type) {

                    case 'A': {

                        this.modalProduct = new ModalProductoA(element, button.buttonElement);

                        
                        break;

                    }

                    case 'B': {

                        this.modalProduct = new ModalProductoB(element, button.buttonElement);


                        break;

                    }

                    case 'C': {

                        this.modalProduct = new ModalProductoC(element, button.buttonElement);


                        break;

                    }

                    default: {

                        throw ('Opcion de modal no manejada');

                    }

                }

                
                // Al final se hace visible
                this.modalProduct.element.style.display = 'block';

            });

        }

    }



    openButtons;
    modalProduct;

}