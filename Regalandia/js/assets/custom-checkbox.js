﻿export class CustomCheckbox {

    constructor(element) {

        this.button = element.querySelector('.checkbox-big-square');

        this.inputElement = element.querySelector('input[type="checkbox"]');

        this.inputElement.checked = false;

        this.button.addEventListener('click', () => {

            this.inputElement.checked = !this.inputElement.checked;

            if (this.inputElement.checked) {

                this.button.classList.add('selected');

            }
            else {

                this.button.classList.remove('selected');
            }

        });

    }


    button;
    inputElement
}
