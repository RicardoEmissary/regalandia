export class TemplateHandler{


    desplegarTemplate($container, $template, $data){

        const compiled = Handlebars.compile($template);

        const dataFinalCompiled = compiled($data);

        $container.innerHTML = dataFinalCompiled;


    }

    desplegarSingleTemplate($container, $template, $data){

        const compiled = Handlebars.compile($template);

        const dataFinalCompiled = compiled($data);

        $container.innerHTML += dataFinalCompiled;


    }

    vaciarContainer($container){

        $container.innerHTML = "";

    }

}
