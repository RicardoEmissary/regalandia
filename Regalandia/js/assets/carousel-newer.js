export class CarouselNewer{


    constructor(containerElement, itemArray, buttonLeft, buttonRight, size){

        this.containerElement = containerElement
        this.itemArray = itemArray
        this.buttonLeft = buttonLeft
        this.buttonRight = buttonRight


        this.loopState = 0

        this.SetScrollInterval()

        this.containerElement.style.gridTemplateColumns = 'repeat( '+ this.itemArray.length.toString() +', 100%)'


        if(size===0) {

            if(window.innerWidth>=1200){
                this.containerElement.style.display = 'none'
            }

            window.addEventListener('resize', () => {

                if (window.innerWidth >= 1200) {
                    this.containerElement.style.display = 'none'

                } else {
                    this.containerElement.style.display = 'grid'


                }
            })
        }
        else{
            if(window.innerWidth<1200){
                this.containerElement.style.display = 'none'
            }

            window.addEventListener('resize', ()=>{

                this.loopState = 0;
                if(window.innerWidth<1200){
                    this.containerElement.style.display = 'none'

                }
                else{
                    this.containerElement.style.display = 'grid'


                }
            })
        }
        this.buttonLeft.addEventListener('click', ()=>{
            if(this.loopState>0) {
                --this.loopState
                this.containerElement.scrollTo(this.loopState * containerElement.offsetWidth, 0)

                clearInterval(this.intervalID)
                this.SetScrollInterval()
            }

        })

        this.buttonRight.addEventListener('click', ()=>{
            if(this.loopState<this.itemArray.length-1) {
                ++this.loopState
                this.containerElement.scrollTo(this.loopState * containerElement.offsetWidth, 0)

                clearInterval(this.intervalID)
                this.SetScrollInterval()

            }
        })


    }

    SetScrollInterval(){
        this.intervalID = setInterval(()=>{
            if(this.loopState<this.itemArray.length-1){
                this.containerElement.scrollTo(this.containerElement.offsetWidth,0)
                ++this.loopState
            }
            else{
                this.containerElement.scrollTo(-(this.containerElement*this.itemArray.length),0);
                this.loopState=0
            }
        },5000)

    }


    containerElement
    itemArray
    loopState
    intervalID
    buttonLeft
    buttonRight
}






