export class ToggleVisibility{

    constructor(htmlElement, showButtons, hideButtons, displayMode = "block"){

        this.displayMode = displayMode;


        if(showButtons.constructor === Array){

            this.showButtons = Array.from(showButtons);

        }
        else{

                this.showButtons.push(showButtons);

        }

        if(hideButtons.constructor === Array){

            this.hideButtons = Array.from(hideButtonsButtons);

        }
        else{


                this.hideButtons.push(hideButtons);


        }

        this.htmlElement = htmlElement;

        for(const button of this.showButtons){

            button.addEventListener("click", ()=>{

                this.htmlElement.style.display = this.displayMode;

            });

        }


        for(const button of this.hideButtons){

            button.addEventListener("click", ()=>{

                this.htmlElement.style.display = "none";

            });

        }



    }


    htmlElement;
    showButtons = [];
    hideButtons = [];
    displayMode;
}
