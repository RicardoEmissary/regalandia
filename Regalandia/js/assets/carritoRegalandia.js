import {Carrito} from './carrito.js';

/**
 * Implementación del carrito específico para su utilización en Regalandia
 */
export class CarritoRegalandia extends Carrito{

    constructor() {
        super();
    }

    setDataFormat() {
        super.setDataFormat();

        this.dataFormat = {entregas: [], productosVarios: []};

    }


    /**
     * Calcula el precio total del carrito sumando el atributo precio de todos los productos agregados.
     * @para
     * Todas las clases productos que se venden en regalandia deben tener el atributo precio.
     * @return {number} El precio total sumado de todos los productos del carrito
     */
    getPrecioTotal(){

        if(this.isNull()){

            return;

        }

        const carrito = this.get();

        this.precioTotal = 0;

        const keys = [];

        for(const key in carrito){

            keys.push(key);

        }


        for(let i = 0; i<2; ++i){

            for(const j of carrito[keys[i]]){

                this.precioTotal += j.precio;

            }

        }

    }

    precioTotal;

}
