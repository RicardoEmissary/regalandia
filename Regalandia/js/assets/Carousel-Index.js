

export class CarouselBG{

    constructor(container, pictures, buttons, headers, anchor){

        this.container = container
        this.pictures = pictures
        this.buttons = buttons
        this.status = 0
        this.container.style.background = 'url('+this.pictures[0]+')center no-repeat'
        this.container.style.backgroundSize = 'cover'
        this.headers = headers
        this.anchor = anchor


        this.headers[0].style.display = 'block'
        this.headers[1].style.display = 'none'
        this.headers[2].style.display = 'none'


        this.buttons[0].style.background = '#f1b144'
        this.buttons[0].style.border = '2px solid #802e58'

        for(let i = 0; i<this.buttons.length; ++i){

            this.buttons[i].addEventListener('click', ()=>{
                for(let btn of this.buttons){
                    btn.style.border = 'none'
                    btn.style.background = '#802e58'
                }
                this.headers[0].style.display = 'none'
                this.headers[1].style.display = 'none'
                this.headers[2].style.display = 'none'
                this.headers[i].style.display = 'block'

                if(i === 0) {
                    for (let a of anchor) {

                        a.style.display = 'block'

                    }
                }
                else{
                    for (let a of anchor) {
                        a.style.display = 'none'
                    }
                }


                this.buttons[i].style.background = '#f1b144'
                this.buttons[i].style.border = '2px solid #802e58'

                clearInterval(this.intervalID)
                this.status = i

                this.container.style.background = 'url("' + this.pictures[i] + '")center no-repeat'

                this.container.style.backgroundSize = 'cover'

                this.intervalID = setInterval(()=>{


                    if(this.status<=this.pictures.length-2) {
                        ++this.status

                        this.container.style.animation = 'cambioImagen'+(this.status).toString()+ ' 1.5s'

                        for(let btn of this.buttons){
                            btn.style.border = 'none'
                            btn.style.background = '#802e58'
                        }
                        this.buttons[this.status].style.background = '#f1b144'
                        this.buttons[this.status].style.border = '2px solid #802e58'


                        setTimeout(() => {
                            this.headers[0].style.display = 'none'
                            this.headers[1].style.display = 'none'
                            this.headers[2].style.display = 'none'
                            this.headers[this.status].style.display = 'block'

                            if(this.status === 0) {
                                for (let a of anchor) {

                                    a.style.display = 'block'

                                }
                            }
                            else{
                                for (let a of anchor) {
                                    a.style.display = 'none'
                                }
                            }

                            this.container.style.background = 'url("' + this.pictures[this.status] + '")center no-repeat'

                            this.container.style.backgroundSize = 'cover'



                        }, 500)
                    }
                    else{
                        this.status = -1
                    }



                },10000)
            })




        }

        this.intervalID = setInterval(()=>{

            if(this.status<=this.pictures.length-2) {
                ++this.status

                this.container.style.animation = 'cambioImagen'+(this.status).toString()+ ' 1.5s'

                for(let btn of this.buttons){
                    btn.style.border = 'none'
                    btn.style.background = '#802e58'
                }
                this.buttons[this.status].style.background = '#f1b144'
                this.buttons[this.status].style.border = '2px solid #802e58'


                setTimeout(() => {
                    this.headers[0].style.display = 'none'
                    this.headers[1].style.display = 'none'
                    this.headers[2].style.display = 'none'
                    this.headers[this.status].style.display = 'block'

                    if(this.status === 0) {
                        for (let a of anchor) {

                            a.style.display = 'block'

                        }
                    }
                    else{
                        for (let a of anchor) {
                            a.style.display = 'none'
                        }
                    }

                    this.container.style.background = 'url("' + this.pictures[this.status] + '")center no-repeat'

                    this.container.style.backgroundSize = 'cover'


                }, 500)
            }
            else{
                this.status = -1
            }



        },10000)

    }




    container
    pictures
    buttons
    status
    intervalID
    anchor
    headers
}


