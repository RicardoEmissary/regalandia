function InitializeNavbar() {

    $('#responsiveNavbar').click(function() {
        $('#myTopnav').toggleClass('responsive');
    });


    //Listener para el scroll de la ventana
    document.body.addEventListener('scroll', () => {
        scrollFunction();
    });

    //Utilizando JQuery,se consigue el navbar de la página
    const nav = $("#myTopnav");

    //Función a realizar en cuanto se produce el scroll
    function scrollFunction() {

        const bannerH = document.getElementById('header').getBoundingClientRect().height;


        if (document.body.scrollTop > bannerH || document.documentElement.scrollTop > bannerH) {
            nav.addClass("fixed-topnav");
        } else {
            nav.removeClass("fixed-topnav");
        }
    }

}

InitializeNavbar();
