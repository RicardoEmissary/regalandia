
/*
       * Controla el flujo de la SPA.
       *
       * Cambia de fase
       *
       * Muestra el resultado en los botones
       *
       * */
export class Pasos{

    /*
       * Inicializa los arrays de los elementos html de la fase y los botones de cada fase
       *
       * */
    constructor(){

        this.menu = document.querySelector("#menuPasos");

        this.pasos[0] = document.querySelector("#paso1");
        this.pasos[1] = document.querySelector("#paso2");
        this.pasos[2] = document.querySelector("#paso3");
        this.pasos[3] = document.querySelector("#paso4");
        this.pasos[4] = document.querySelector("#paso5C");
        this.pasos[5] = document.querySelector("#paso5T");

        for (const i of this.pasos){

            i.style.background = "none";

        }

        //Botones del menu superior de colores recoleccion, globos....
        this.stepButtons.push(document.querySelector("#step1"));
        this.stepButtons.push(document.querySelector("#step2"));
        this.stepButtons.push(document.querySelector("#step3"));
        this.stepButtons.push(document.querySelector("#step4"));
        this.stepButtons.push(document.querySelector("#step5"));


        //Listener del custom event
        document.addEventListener("chP", ($e)=>{

        //Le agrega el estado de inactivo a todos los botones del menu
           for(const i of this.stepButtons){

               i.classList.add("unactiveStep");


           }

           //Le quita estado inactivo al boton especifico
           this.stepButtons[$e.detail.paso].classList.remove("unactiveStep");

           //Le agrega el estado activo
           this.stepButtons[$e.detail.paso].classList.add("activeStep");

           //Se le activa su funcionalidad hover
           this.stepButtons[$e.detail.paso].classList.add("hoverActivate");

        });


        //Declaración del custom event y su detail
        this.changePasoEvent = new CustomEvent("chP", {'detail':{

                paso: 0

            }});


    }


  /*
  * Método para cambiar de fase/paso
  *
  * @return {type} Nada.
  *
  * */
    changePaso(option){

        //Esconde todos los htmls de fase/paso que hay
        for(const i of this.pasos){

            i.style.display = "none";

        }

        //Muestra el específico
        this.pasos[option].style.display = "block";

        //agrega al detail del custom event el número de página a ser mostrada
        this.changePasoEvent.detail.paso = option;

        //Se lanza el custom event
        document.dispatchEvent(this.changePasoEvent);

        //Listener once true para que al hacer clic en el boton de fase cambie al html de esa fase
        this.stepButtons[option].addEventListener("click",()=> {

            this.changePaso(option);

        },{once:true});
    }

    menu;
    pasos = [];
    stepButtons = [];
    changePasoEvent;
}
