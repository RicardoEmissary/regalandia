export class RegalosTable{

    constructor($regalos){

        for(const i of $regalos){

            const htmlElement = document.querySelector("#envolturaT-"+i.tempId.toString());

            const select = htmlElement.querySelector("select.tamano");

            switch(i.tamano){

                case 0:{

                    select.querySelector('[value="' + 0 + '"]').selected = true;

                    break;
                }

                case 1:{

                    select.querySelector('[value="' + 1 + '"]').selected = true;

                    break;
                }

                case 2:{

                    select.querySelector('[value="' + 2 + '"]').selected = true;

                    break;
                }

            }

        }

    }



}
