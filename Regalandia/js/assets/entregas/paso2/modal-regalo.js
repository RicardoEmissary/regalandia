import {TemplateHandler} from "../../templateHandlebars.js";

export class ModalRegalo{

    constructor(container, template, data, regalos, categoria){

        const tH = new TemplateHandler();

        const finalArray = [];

        for(const i of data.envolturas){

            if(i.categoria === categoria){

                finalArray.push(i);

            }

        }

        tH.vaciarContainer(container);  //Necesario para que no se repitan los mismos articulos al desplegar
        tH.desplegarSingleTemplate(container, template, {envolturas: finalArray});

        switch (categoria) {

            case 1:{

                for(const i of regalos){



                    const openButton = document.querySelector("#envolturaT-" + i.tempId.toString() + " .btn-envoltura-papel");
                    this.htmlElement = document.querySelector("#modal-papel");
                    this.closeButton = this.htmlElement.querySelector("span.close");
                    this.openButtonListener(openButton);

                }

                break;
            }

            case 2:{

                for(const i of regalos){

                    const openButton = document.querySelector("#envolturaT-" + i.tempId.toString() + " .btn-envoltura-bolsas");
                    this.htmlElement = document.querySelector("#modal-bolsa");
                    this.closeButton = this.htmlElement.querySelector("span.close");
                    this.openButtonListener(openButton);

                }

                break;
            }

        }






       this.closeButtonListener();


    }

    openButtonListener($e){

        $e.addEventListener("click", ()=>{

           this.htmlElement.style.display = "flex";

        });

    }

    closeButtonListener(){

        this.closeButton.addEventListener("click", ()=>{

            this.htmlElement.style.display = "none";

        });

    }

    grid = [];
    openButton;
    closeButton;
    htmlElement;

}

export class ModalRegaloHandler{

    constructor($categoria, $regalos) {


        this.template = document.querySelector("#modalEnvolturasTemplate").innerHTML;




        this.containers.push(document.querySelector("#modal-papel .papelBolsa .boxes-grid"));
        this.containers.push(document.querySelector("#modal-bolsa .papelBolsa .boxes-grid"));

        const xmlhttp = new XMLHttpRequest();

        const cont = this.containers[$categoria-1];

        const temp = this.template;

        xmlhttp.onreadystatechange = function() {

            if (this.readyState === 4 && this.status === 200) {

                const data = JSON.parse(this.responseText);

                this.modal = new ModalRegalo(cont, temp, data, $regalos, $categoria);

            }

        }

        xmlhttp.open("GET", "./JSON/envolturas.json", true);
        xmlhttp.send();


    }

    template;
    envolturasRegalos;
    containers = [];
    modal;
}
