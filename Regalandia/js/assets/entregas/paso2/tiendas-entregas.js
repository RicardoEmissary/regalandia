
/*
* todo:
 *
* */

import {Tiendas} from "../../tiendas.js";
import {EnvolturaRegalo} from "../../../models/envolturaRegalo.js";
import {Subcategoria} from "../../../models/subcategoria.js";
import {Producto} from "../../../models/producto.js";


export class TiendasEntregas{

    constructor(regalos){

       const envolturas = [];

       envolturas.push(document.querySelector('#modal-bolsas-tienda-especial'));  // 0
       envolturas.push(document.querySelector('#modal-bolsas-tienda-divertido')); // 1
       envolturas.push(document.querySelector('#modal-bolsas-tienda-enGrande')); // 2
       envolturas.push(document.querySelector('#modal-papel-tienda-especial')); // 3
       envolturas.push(document.querySelector('#modal-papel-tienda-divertido')); // 4
       envolturas.push(document.querySelector('#modal-papel-tienda-enGrande')); // 5

        // Se guardan los botones de abrir tienda de bolsas
        const bolsasButtons = document.querySelectorAll(".btn-envoltura-bolsas");

        for(const button of bolsasButtons){

            button.addEventListener('click', () => {

                // Se obtiene el valor del select del mismo tr (la plantilla no debe cambiar).
                const tamano = button.parentElement.parentElement.parentElement
                    .querySelector('select.tamano')
                    .value + 1;

                switch(tamano){

                    case '01':{

                        envolturas[0].style.display = 'block';

                        break;
                    }

                    case '11':{

                        envolturas[1].style.display = 'block';

                        break;
                    }

                    case '21':{

                        envolturas[2].style.display = 'block';

                        break;
                    }

                }

            });

        }

        // se guardan los botones de abrir tienda de papel
        const papelButtons = document.querySelectorAll(".btn-envoltura-papel");

        for(const button of papelButtons){

            button.addEventListener('click', () => {

                // valor del select del mismo tr
                const tamano = button.parentElement.parentElement.parentElement
                    .querySelector('select.tamano')
                    .value + 1;

                switch(tamano){

                    case '01':{

                        envolturas[3].style.display = 'block';

                        break;
                    }

                    case '11':{

                        envolturas[4].style.display = 'block';

                        break;
                    }

                    case '21':{

                        envolturas[5].style.display = 'block';

                        break;
                    }

                }

            });

        }

        // Se inicializan el botón de cerrar

        for(let i = 0; i<envolturas.length; ++i){

            envolturas[i].querySelector(".close")
                .addEventListener('click', () => {

                    envolturas[i].style.display = 'block';

                });

        }

        // Se inicializa la tecla ESC para cerrar el modal

        for(let i = 0; i < envolturas.length; ++i){

            document.addEventListener('keydown', ($event) => {

                if($event.key === 'Escape'){

                    envolturas[i].style.display = 'none';

                }

            });

        }

        const templateSubcategorias = document.querySelector("#articulos-envolturas");
        const templateArticulos = document.querySelector("#modalEnvolturasTemplate");
        const templateModal = document.querySelector("#modalProductTemplate");

        const subcategoriasBolsas = [];
        const bolsas = [];

        let responseBolsas = null;
        $.ajax({
            type: "POST",
            url: "entregas.aspx/getBolsas",
            dataType: "json",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            success: function (result) {
                responseBolsas = result;
            },
            error: function (err) {
                console.log("Error: ", err);
                responseBolsas = "Error";
            },
            complete: function (result) {

                const bolsasRaw = responseBolsas.d;

                // mapeo de datos a la clase del modelo de la vista

                for(const bolsa of bolsasRaw){

                    const envoltura = new Producto();

                    envoltura.id = bolsa.id_Producto;
                    envoltura.nombre = bolsa.nombre_Producto;
                    envoltura.descripcion = bolsa.descripcion_Producto;
                    envoltura.foto1 = bolsa.foto1_Produtcto;
                    envoltura.foto2 = bolsa.foto2_Producto;
                    envoltura.foto3 = bolsa.foto3_Producto;
                    envoltura.foto4 = bolsa.foto4_Producto;
                    envoltura.categoria = bolsa.id_Categoria;

                    envoltura.tamano = bolsa.id_Tamanio;
                    envoltura.precio = bolsa.precio_Producto;

                    bolsas.push(envoltura);

                    let subcategoriaAdded = false;

                    for(const subcategoria of subcategoriasBolsas){

                        if(subcategoria.id === envoltura.categoria){

                            subcategoriaAdded = true;

                        }

                    }

                    if(!subcategoriaAdded){

                        const subcategoria = new Subcategoria();

                        subcategoria.id = envoltura.categoria;
                        subcategoria.nombre = bolsa.nombre_Categoria;

                        subcategoriasBolsas.push(subcategoria);



                    }

                }

                console.log(subcategoriasBolsas)
                const bolsasEspecial = [];
                const bolsasDivertido = [];
                const bolsasEnGrande = [];

                for(const bolsa of bolsas){

                    switch(bolsa.tamano){

                        case 1:{

                            bolsasEspecial.push(bolsa);

                            break;
                        }

                        case 2:{

                            bolsasDivertido.push(bolsa);

                            break;
                        }

                        case 3:{

                            bolsasEnGrande.push(bolsa);

                            break;
                        }

                    }

                }

                const tiendaBolsasEspecial = new Tiendas(

                    1,
                    envolturas[0],
                    templateSubcategorias.innerHTML,
                    subcategoriasBolsas,
                    templateArticulos.innerHTML,
                    bolsasEspecial,
                    templateModal.innerHTML,
                    document.querySelector("#modal-product-container-bolsas-especial")

                );

                const tiendaBolsasDivertido = new Tiendas(

                    1,
                    envolturas[1],
                    templateSubcategorias.innerHTML,
                    subcategoriasBolsas,
                    templateArticulos.innerHTML,
                    bolsasDivertido,
                    templateModal.innerHTML,
                    document.querySelector("#modal-product-container-bolsas-divertido")

                );

                const tiendaBolsasEnGrande = new Tiendas(

                    1,
                    envolturas[2],
                    templateSubcategorias.innerHTML,
                    subcategoriasBolsas,
                    templateArticulos.innerHTML,
                    bolsasEnGrande,
                    templateModal.innerHTML,
                    document.querySelector("#modal-product-container-bolsas-enGrande")

                );




            }




        });


        const subcategoriasPapel = [];
        const envolturasPapel = [];
        /*
        let responsePapel = null;
        /*
        $.ajax({
            type: "POST",
            url: "entregas.aspx/getEnvolturas",
            dataType: "json",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            success: function (result) {
                responsePapel = result;
            },
            error: function (err) {
                console.log("Error: ", err);
                responsePapel = "Error";
            },
            complete: function (result) {

                const papelRaw = responsePapel.d;

                // mapeo de datos a la clase del modelo de la vista

                for(const papel of papelRaw){

                    const envoltura = new EnvolturaRegalo();

                    envoltura.id = papel.id_Regalo;
                    envoltura.nombre = papel.nombre_Regalo;
                    envoltura.descripcion = bolsa.descripcion_Regalo;
                    envoltura.foto1 = papel.foto1_Regalo;
                    envoltura.foto2 = papel.foto2_Regalo;
                    envoltura.foto3 = papel.foto3_Regalo;
                    envoltura.foto4 = papel.foto4_Regalo;
                    envoltura.categoria = papel.id_Categoria;
                    envoltura.subcategoria = papel.id_SubCategoria;
                    envoltura.tamano = papel.id_Tamanio;
                    envoltura.precio = papel.precio_Regalo;

                    envolturasPapel.push(envoltura);

                    let subcategoriaAdded = false;

                    for(const subcategoria of subcategoriasPapel){

                        if(subcategoria.id === envoltura.subcategoria){

                            subcategoriaAdded = true;

                        }

                    }

                    if(!subcategoriaAdded){

                        const subcategoria = new Subcategoria();

                        subcategoria.id = envoltura.subcategoria;
                        subcategoria.nombre = bolsa.nombre_SubCategoria;

                        subcategoriasPapel.push(subcategoria);



                    }

                }

                const papelEspecial = [];
                const papelDivertido = [];
                const papelEnGrande = [];

                for(const papel of envolturasPapel){

                    switch(papel.tamano){

                        case 1:{

                            papelEspecial.push(papel);

                            break;
                        }

                        case 2:{

                            papelDivertido.push(papel);

                            break;
                        }

                        case 3:{

                            papelEnGrande.push(papel);

                            break;
                        }

                    }

                }



                const tiendaPapelEspecial = new Tiendas(

                    1,
                    envolturas[3],
                    templateSubcategorias.innerHTML,
                    subcategoriasPapel,
                    templateArticulos.innerHTML,
                    papelEnGrande,
                    templateModal.innerHTML,
                    document.querySelector("#modal-product-container-bolsas-enGrande")

                );

                const tiendaPapelDivertido = new Tiendas(

                    1,
                    envolturas[4],
                    templateSubcategorias.innerHTML,
                    subcategoriasPapel,
                    templateArticulos.innerHTML,
                    papelEnGrande,
                    templateModal.innerHTML,
                    document.querySelector("#modal-product-container-bolsas-enGrande")

                );

                const tiendaPapelEnGrande = new Tiendas(

                    1,
                    envolturas[5],
                    templateSubcategorias.innerHTML,
                    subcategoriasPapel,
                    templateArticulos.innerHTML,
                    papelEnGrande,
                    templateModal.innerHTML,
                    document.querySelector("#modal-product-container-bolsas-enGrande"),


                );



            }




        });

*/


    }

}
