import {Direccion} from "../../../models/direccion.js";
import {Regalo} from "../../../models/regalo.js";


export class ConfirmarFase1{

    constructor(eventToDispatch){

        this.buttons.push(document.querySelector("#paso1CNext"));

        this.buttons.push(document.querySelector("#paso1Next1D"));

        this.buttons.push(document.querySelector("#paso1NextVD"));

        this.buttons.push(document.querySelector("#confirmarViaTienda1"));

        this.buttons.push(document.querySelector("#confirmarViaTienda2"));

        this.buttons.push(document.querySelector("#paso1TNext"));

        this.arrayRegalos = [];

        this.eventToDispatch = eventToDispatch;


        this.unRegaloUnaCasa();
        this.variosRegalosUnaCasa();
        this.variosRegalosVariasCasas();

        this.unRegaloUnaTienda();
        this.variosRegalosUnaTienda();
        this.variosRegalosVariasTiendas();


    }

    unRegaloUnaCasa(){

        const nombre = document.querySelector("#nombre1RegaloCasa");

        const tamanoRadios = document.querySelector("#tamano1RegaloCasa");

        const fechaRecoleccion = document.querySelector("#fechaCasa1Regalo .result span");

        const horaRecoleccion = document.querySelector("#horariosCasa1Regalo");

        const calle = document.querySelector("#calleCasa1Regalo");

        const numero = document.querySelector("#numeroCasa1Regalo");

        const colonia = document.querySelector("#coloniaCasa1Regalo");

        const ciudad = document.querySelector("#ciudadCasa1Regalo")

        const codigoPostal = document.querySelector("#cpCasa1Regalo");

        const estado = document.querySelector("#estadoCasa1Regalo");

        const referencia = document.querySelector("#refCasa1Regalo");

        this.buttons[0].addEventListener("click", ()=>{

            this.arrayRegalos = [];

            const direccion = new Direccion();

            direccion.calle = calle.value;
            direccion.numero = numero.value;
            direccion.colonia = colonia.value;
            direccion.ciudad = ciudad.value;
            direccion.codigoPostal = codigoPostal.value;
            direccion.estado = estado.value;
            direccion.puntoReferencia = referencia.value;


            const regalo = new Regalo();

            regalo.direccion = direccion;
            regalo.nombre = nombre.value;
            regalo.tempId = 1;

            const radios = tamanoRadios.querySelectorAll("input[type='radio']");

            let index = 0;

            for(let i = 0; i<3; ++i){

                if(radios[i].checked){

                    index = i;
                    break;

                }

            }

            regalo.tamano = index;

            regalo.fechaRecoleccion = fechaRecoleccion.textContent;

            regalo.horaRecoleccion = horaRecoleccion[horaRecoleccion.selectedIndex].textContent;

            this.arrayRegalos.push(regalo);



            document.querySelectorAll(".pasosCasa")[0].style.display = "none";

            this.eventToDispatch.detail.regalosArray = this.arrayRegalos;

            document.dispatchEvent(this.eventToDispatch);

        });


    }

    variosRegalosUnaCasa(){



        const section = document.querySelector("#paso1CasaV1D");

        this.buttons[1].addEventListener("click", ()=>{

            this.arrayRegalos = [];

            //Objeto que guarda la dirección única de los regalos
            const direccion = new Direccion();

            direccion.calle = document.querySelector(".inputCalle").value;
            direccion.numero = document.querySelector(".inputNumero").value;
            direccion.colonia = document.querySelector(".inputColonia").value;
            direccion.puntoReferencia = document.querySelector(".inputReferencia").value;
            direccion.codigoPostal = document.querySelector(".inputCP").value;
            direccion.estado = document.querySelector(".inputEstado").value;

            const cardRegaloElements = section.querySelectorAll(".card-entregas");

            let index = 0;

            let counter = 0;
            for (const i of cardRegaloElements){

                const tamanoRadios = i.querySelector(".inputTamano")
                    .querySelectorAll("input[type='radio']");

                for(let j = 0; j<tamanoRadios.length; ++j){

                    if(tamanoRadios[j].checked){

                        index = j;

                        break;

                    }

                }

                const regalo = new Regalo();

                const selectHora = section.querySelector(".input-hora");

                regalo.nombre = i.querySelector(".inputNombre").value;
                regalo.tamano = index;
                regalo.direccion = direccion;
                regalo.fechaRecoleccion = section.querySelector(".date-picker .result span").textContent;
                regalo.horaRecoleccion = selectHora[selectHora.selectedIndex].textContent
                regalo.tempId = counter++;

                this.arrayRegalos.push(regalo);
            }
            this.eventToDispatch.detail.regalosArray = this.arrayRegalos;

            document.dispatchEvent(this.eventToDispatch);

        });

    }

    variosRegalosVariasCasas(){



        const section = document.querySelector("#paso1CasaVVD");

        this.buttons[2].addEventListener("click", ()=>{

            this.arrayRegalos = [];

            const cardRegaloElements = section.querySelectorAll(".card-entregas");

            let counter = 0;
            for(const i of cardRegaloElements){

                const direccion = new Direccion();

                direccion.calle = i.querySelector(".inputCalle").value;
                direccion.numero = i.querySelector(".inputNumero").value;
                direccion.colonia = i.querySelector(".inputColonia").value;
                direccion.codigoPostal = i.querySelector(".inputCP").value;
                direccion.ciudad = i.querySelector(".inputCiudad").value;
                direccion.estado = i.querySelector(".inputEstado").value;
                direccion.puntoReferencia = i.querySelector(".inputReferencia").value;

                const regalo = new Regalo();

                const selectHora = section.querySelector(".input-hora");

                let index = 0;

                const tamanoRadios = i.querySelector(".inputTamano")
                    .querySelectorAll("input[type='radio']");

                for(let j = 0; j<tamanoRadios.length; ++j){

                    if(tamanoRadios[j].checked){

                        index = j;

                        break;

                    }

                }

                regalo.nombre = i.querySelector(".inputNombre").value;
                regalo.direccion = direccion;
                regalo.fechaRecoleccion = i.querySelector(".date-picker .result span").textContent;
                regalo.horaRecoleccion = selectHora[selectHora.selectedIndex].textContent;
                regalo.tamano = index;
                regalo.tempId = counter++;

                this.arrayRegalos.push(regalo);

            }

            this.eventToDispatch.detail.regalosArray = this.arrayRegalos;

            document.dispatchEvent(this.eventToDispatch);

        });
    }

    unRegaloUnaTienda(){



        const section = document.querySelector("#paso1Tienda1");

        this.buttons[3].addEventListener("click", ()=>{

            this.arrayRegalos = [];

            const regalo = new Regalo();


            const tamanoRadios = section.querySelector(".inputTamano")
                .querySelectorAll("input[type='radio']");

            let index = 0;

            for(let j = 0; j<tamanoRadios.length; ++j){

                if(tamanoRadios[j].checked){

                    index = j;

                    break;

                }

            }

            const tiendaSelect = section.querySelector("#tiendas-sugeridas-select");



            regalo.nombre = section.querySelector(".inputNombre").value;
            regalo.tamano = index;
            regalo.tienda = tiendaSelect[tiendaSelect.selectedIndex].textContent;
            regalo.link = section.querySelector(".inputLink").value;
            regalo.precio = section.querySelector(".inputPrecio").value;
            regalo.tempId = 0;

            this.arrayRegalos.push(regalo);

            this.eventToDispatch.detail.regalosArray = this.arrayRegalos;

            document.dispatchEvent(this.eventToDispatch);

        });



    }

    variosRegalosUnaTienda(){



        const section = document.querySelector("#paso1TiendaV1D");
        const tiendaSelect = document.querySelector("#tiendas-sugeridas-V");



        this.buttons[4].addEventListener("click", ()=>{

            const cardRegaloElements = section.querySelectorAll(".card-entregas");

            this.arrayRegalos = [];

            let counter = 0;
            for (let i of cardRegaloElements){

                const tamanoRadios = i.querySelector(".inputTamano")
                    .querySelectorAll("input[type='radio']");

                let index = 0;


                for(let j = 0; j<tamanoRadios.length; ++j){

                    if(tamanoRadios[j].checked){

                        index = j;

                        break;

                    }

                }

                const regalo = new Regalo();
                regalo.nombre = i.querySelector(".inputNombre").value;
                regalo.link = i.querySelector(".inputLink").value;
                regalo.precio = i.querySelector(".inputPrecio").value;
                regalo.tienda = tiendaSelect[tiendaSelect.selectedIndex].textContent;
                regalo.tamano = index;
                regalo.tempId = counter++;

                this.arrayRegalos.push(regalo);

            }

            this.eventToDispatch.detail.regalosArray = this.arrayRegalos;

            document.dispatchEvent(this.eventToDispatch);


        });

    }

    variosRegalosVariasTiendas(){

        const section = document.querySelector("#paso1TiendaVVD");

        this.buttons[5].addEventListener("click", ()=>{

            this.arrayRegalos = [];

            const cardRegaloElements = section.querySelectorAll(".card-entregas");

            let counter = 0;

            for(const i of cardRegaloElements){

                const tamanoRadios = i.querySelector(".inputTamano")
                    .querySelectorAll("input[type='radio']");

                let index = 0;

                for(let j = 0; j<tamanoRadios.length; ++j){

                    if(tamanoRadios[j].checked){

                        index = j;

                        break;

                    }

                }

                const tiendaSelect = i.querySelector(".tiendas-sugeridas");

                const regalo = new Regalo();

                regalo.nombre = i.querySelector(".inputNombre").value;
                regalo.link = i.querySelector(".inputLink").value;
                regalo.precio = i.querySelector(".inputPrecio").value;
                regalo.tienda = tiendaSelect[tiendaSelect.selectedIndex].textContent;
                regalo.tamano = index;
                regalo.tempId = counter++;

                this.arrayRegalos.push(regalo);

            }
            this.eventToDispatch.detail.regalosArray = this.arrayRegalos;

            document.dispatchEvent(this.eventToDispatch);

        });


    }


    buttons = [];

    arrayRegalos;
    eventToDispatch;
}
