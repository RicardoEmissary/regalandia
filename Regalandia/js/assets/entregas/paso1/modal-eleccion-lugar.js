
import {CardRegalo} from "./card-regalo.js";

import {StartCalendar} from "../../../date-picker.js";
import {InputNumberHandler} from "../../CustomInputNumber.js";


class ModalLugar{

        /*
        * Para ser usado por su manejador en la primera fase. Representa cada una de las modales de elección de vía.
        *
        * @param {object} Contenido HTML a mostrar o esconder.
        *
        * @param {object} Botón para abrir el modal.
        *
        * @param {object} Botón para cerrar el modal.
        *
        * @param {Event} Evento que se dispara al confirmar la elección de la vía
        *
        *
        * */
    constructor(htmlContent, openButton, closeButton, ventanaElegida){

        //Inicializacion de los atributos de los elementos html
        this.htmlContent = htmlContent;

        this.openButton = openButton;

        this.closeButton = closeButton;

        this.radioDireccion = this.htmlContent.querySelector(".selectAddress");



        const inputElement = this.htmlContent.querySelector("input[type='number']");

        const buttonUp = this.htmlContent.querySelector(".plus-sign");

        const buttonDown = this.htmlContent.querySelector(".minus-sign");

        this.inputNumberHandler = new InputNumberHandler(inputElement,
                                                            buttonUp,
                                                            buttonDown,
                                                            1,
                                                            10,
                                                            1);


        this.confirmarButton = this.htmlContent.querySelector(".confirmar-modal");

        //Inicialización de los listener de la ventana modal
        this.showModalListener();

        this.closeModalListener();

        this.confirmarListener(ventanaElegida);

        //Interval que verifica si debe mostrar la opcion de varias direcciones
        const intervalRadioDireccion = setInterval(()=>{

            if(this.inputNumberHandler.amount > 1){

                this.radioDireccion.style.display = "flex";

            }
            else{

                this.radioDireccion.style.display = "none";

            }


        },100);



    }


    /*
    * Listener de los botones para mostrar las modales al hacer click.
    *
    * @return {type} Nada.
    *
    * */
    showModalListener(){

        this.openButton.addEventListener("click", ()=>{

            this.htmlContent.style.display = "flex";


        });

    }

    /*
   * Listeners
   *
   * Para el boton que cierra el modal al hacer click.
   *
   * Para cuando se da click afuera del modal.
   *
   * Para cuando se presiona la tecla de Escape.
   *
   * @return {type} Nada.
   *
   * */
    closeModalListener(){

        this.closeButton.addEventListener("click", ()=>{

            this.htmlContent.style.display = "none";

        });

        this.htmlContent.addEventListener("click", ($e)=>{

            if($e.target === this.htmlContent){

                this.htmlContent.style.display = "none";

            }

        });

        document.addEventListener("keydown", ($e)=>{

            if($e.key === "Escape"){

                this.htmlContent.style.display = "none";

            }

        });

    }

    /*
   * Listener para el boton que confirma la vía
   *
   * @return {type} Nada.
   *
   * */
    confirmarListener(ventanaElegida){

        this.confirmarButton.addEventListener("click", ()=>{

            //constante que guarda el elemento html del radio de varias direcciones
            const opciones = this.radioDireccion.querySelectorAll("input[type='radio']");

            //Se valida el id y se llenan los datos del detail del event. Al finalizar, se lanza el event
            if(this.openButton.id === "button-casa"){

                ventanaElegida.detail.lugar = false;
                ventanaElegida.detail.opcion = opciones[1].checked;
                ventanaElegida.detail.cantidad = this.inputNumberHandler.amount;
                document.dispatchEvent(ventanaElegida);

            }
            else{

                ventanaElegida.detail.lugar = true;
                ventanaElegida.detail.opcion = opciones[1].checked;
                ventanaElegida.detail.cantidad = this.inputNumberHandler.amount;
                document.dispatchEvent(ventanaElegida);
            }

            //Desaparece el modal
            this.htmlContent.style.display = "none";

        });

    }


    confirmarButton;
    openButton;
    closeButton;
    inputNumberHandler;
    radioDireccion;


    htmlContent;



}

export class ModalLugarHandler{

    /*

    * A ser llamado al ser creada la página. Inicializa los elementos HTML que serán utilizados para la elección
    *  de vía.
    *
    */
    constructor(buttonCasa, buttonTienda, modalCasa, modalTienda, cerrarCasa, cerrarTienda) {

        //Se inicializa el listener del customevent
        this.customEventListener();


        //Se crea un custom event para cuando se da al boton de aceptar de una ventana modal.
        //Detail guarda el objeto js que lanza el event
        const ventanaElegida = new CustomEvent('ventana', {'detail': {

                lugar:'',
                opcion:false,
                cantidad:0

            }});

        //Se inicializan los dos modales necesarios
        this.modalCasa = new ModalLugar(modalCasa, buttonCasa, cerrarCasa, ventanaElegida);

        this.modalTienda = new ModalLugar(modalTienda, buttonTienda, cerrarTienda, ventanaElegida);

        this.opciones.push(document.querySelectorAll(".pasosCasa"));
        this.opciones.push(document.querySelectorAll(".pasosTienda"));

        for(const i of this.opciones){

            for(const j of i){

                j.style.display = "none";

            }

        }


    }

    /*
        * Listener del custom event. Muestra el template necesario según las opciones introducidas en el modal
        *
        * @return {type} Nada.
        *
        * */
    customEventListener(){

// Listener para el custom event
        document.addEventListener('ventana', ($e)=> {

            //Se actualizan los valores de la clase
            this.lugar = $e.detail.lugar;
            this.opcion = $e.detail.opcion;
            this.cantidad = $e.detail.cantidad;

            if(this.cantidad === 1){

                document.querySelector("#paso1Casa1").scrollIntoView();

            }
            else{

                    if(!this.opcion){

                        document.querySelector("#paso1CasaV1D").scrollIntoView();

                    }
                    else{

                        document.querySelector("#paso1CasaVVD").scrollIntoView();

                    }

                }









            //Se esconden los htmls de opciones que pudieran estar visibles
            for(const i of this.opciones){

                for(const j of i){

                    j.style.display = "none";

                }

            }

            /*Se valida cual es el template a mostrar.
            *
            * Al encontrar la opción, se crean cards en el html segun la cantidad señalada
            * Los templates html se sacan de la clase CardRegalo : ref: ./card-regalo.js
            *
            * */
            if(!this.lugar){

                if(this.cantidad === 1){

                    this.opciones[0][0].style.display = "block";

                }
                else{

                    const cardRegalo = new CardRegalo();

                    if(!this.opcion){

                        this.opciones[0][1].style.display = "block";

                        const container = document.querySelector("#contenedorRegalosMD-casa");



                        for(let i = 0; i<this.cantidad; ++i){

                            let compiled = Handlebars.compile(cardRegalo.templates[0]);

                            let dataFinal = {
                                indice: i+1
                            };

                            let dataFinalCompiled = compiled(dataFinal);

                            container.innerHTML += dataFinalCompiled;


                        }

                    }
                    else{

                        this.opciones[0][2].style.display = "block";

                        const container = document.querySelector("#contenedorRegalosDD-casa");

                        container.innerHTML = "" ;



                            for(let i = 0; i<this.cantidad; ++i){

                                let compiled = Handlebars.compile(cardRegalo.templates[1]);

                                let dataFinal = {
                                    indice: i+1
                                };

                                let dataFinalCompiled = compiled(dataFinal);

                                container.innerHTML += dataFinalCompiled;

                                StartCalendar();
                            }



                    }
                }

            }
            else{

                if(this.cantidad === 1){

                    this.opciones[1][0].style.display = "block";

                }
                else{

                    const cardRegalo = new CardRegalo();

                    if(!this.opcion){

                        this.opciones[1][1].style.display = "block";

                        const container = document.querySelector("#contenedorRegalosMD-tienda");

                        container.innerHTML = "";

                        for(let i = 0; i<this.cantidad; ++i){

                            let compiled = Handlebars.compile(cardRegalo.templates[2]);

                            let dataFinal = {
                                indice: i+1
                            };

                            let dataFinalCompiled = compiled(dataFinal);

                            container.innerHTML += dataFinalCompiled;

                        }


                    }
                    else{

                        this.opciones[1][2].style.display = "block";

                        const container = document.querySelector("#contenedorRegalosDD-tienda");

                        container.innerHTML = "";

                        for(let i = 0; i<this.cantidad; ++i){

                            let compiled = Handlebars.compile(cardRegalo.templates[3]);

                            let dataFinal = {
                                indice: i+1
                            };

                            let dataFinalCompiled = compiled(dataFinal);

                            container.innerHTML += dataFinalCompiled;

                        }


                    }
                }
            }

        });

    }

    /*
    * Listener de los botones para mostrar las modales al hacer click.
    *
    * @return {type} Nada.
    *
    * */


    //ModalLugar. Objeto que representa el modal de casa
    modalCasa;

    //ModalLugar. Objeto que representa el modal de tienda
    modalTienda;



    /*boolean. Identifica si se elige casa o tienda.
    *
    * false: casa.
    *
    * true: tienda
    */
    lugar;

    /*boolean. Identifica la opción que se eligió entre misma direccion y varias direcciones.
    *
    * No tiene validez cuando la cantidad es 1
    *
    * false: mismaDireccion.
    *
    * true: variasDirecciones
    *
    * default: otro valor lanzará un error
    * */
    opcion;

    /*number. Cantidad de regalos.
    *
    * 0: un solo regalo.
    *
    * Solo número enteros positivos
    * */
    cantidad;

    /*Array<Array<object>>. Matriz de los HTMLS de las distintas opciones de la primera fase.
    *
    * En cada fila se ordena por:
    *
    *   (1 regalo, n regalos 1 direccion, n regalos n direcciones)
    *
    * opciones[0]-> HTMLS de casa
    *
    * opciones[0]-> HTMLS de tienda
    *
    * */
    opciones = [];
}
