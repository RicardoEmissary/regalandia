export class CardRegalo{

    constructor(){

    this.initializeTemplates();

    }

        /*
      * Llena el arreglo templates.
      *
      * (1casa, varias casas, 1 tienda, varias tiendas).
      *
      * @return {type} Nada.
      *
      * */
    initializeTemplates(){

        //template de card de misma casa
        this.templates.push(document.querySelector("#templateRegalosCasa1Direccion").innerHTML);

        //template de card de varias casas
        this.templates.push(document.querySelector("#templateRegalosCasaVDireccion").innerHTML);

        //template para una tienda
        this.templates.push(document.querySelector("#templateRegalosTienda1Direccion").innerHTML);

        //template para varias tiendas
        this.templates.push(document.querySelector("#templateRegalosTiendaVDireccion").innerHTML);

    }


    /*Array<string>
    *
    * Array de templates para las cards
    * */
    templates = [];
}
