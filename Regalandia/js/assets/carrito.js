/**
 * Clase abstracta que declara lo básico del manejo del localStorage específico para un shopping cart o carrito
 * @para
 * Cada clase que herede de esta debe implementar el método setDataFormat, el cual le da el formato al carrito en un
 * objeto de tipo {} que guarda arrays
 * @para
 * Ejemplo: this.dataFormat = {productos: [], servicios: []}
 */
export class Carrito{

    /**
     * Llama al método setDataFormat implementado en las herencias de esta clase.
     */
    constructor(){

        this.storage = localStorage;

        this.storageKey = 'carritoRegalandia';

        this.setDataFormat();

        this.create();

    }

    setDataFormat(){} // método abstracto

    /**
     * Llama a localStorage.getItem y revisa si existe el carrito o es null
     * @return {boolean}      Si existe o no el carrito
     */
    isNull(){

        const carrito = this.storage.getItem(this.storageKey);

        return carrito === null;

    }

    /**
     * Si el carrito es null, crea un carrito vacío
     * @return {void}      Nada
     */
    create(){

        if(this.isNull()){

            this.storage.setItem(this.storageKey, JSON.stringify(this.dataFormat));

        }

    }

    /**
     * Captura el carrito del localStorage y lo convierte a su dataFormat
     * @return {any}      El carrito en el dataFormat específico
     */
    get(){

        if(!this.isNull()){

            return JSON.parse(this.storage.getItem(this.storageKey));

        }

    }

    /**
     * Guarda el carrito en el localStorage
     * @param  {any} carrito El carrito a guardar
     * @return {void}      Nada
     */
    save(carrito){

        if(!this.isNull()){

            this.storage.setItem(this.storageKey, JSON.stringify(carrito));

        }

    }

    /**
     * Agrega un dato a un campo específico del carrito
     * @param  {any} item El dato a guardar
     * @param  {string} key La clave del campo en el carrito
     * @return {void}      Nada
     */
    add(item, key){

        this.create();

        const carrito = this.get();

        for(const $key in carrito){

            if(key === $key){

                carrito[key].push(item);
                break;

            }

        }

        this.save(carrito);

    }

    /**
     * Elimina un dato de un campo específico del carrito
     * @param  {any} item El dato a eliminar
     * @param  {string} key La clave del campo en el carrito
     * @return {void}      Nada
     */
    remove(item, key){

        if(this.isNull()){

            return;

        }

        const carrito = this.get();

        for(const $key in carrito){

            if(key === $key){

                for(let i = 0; i<carrito[key].length; ++i){
                    console.log('item' + item.toString());
                    console.log('carritoV' + carrito[key][i].toString());
                    if(item === carrito[key][i]){

                        carrito[key][i] = carrito[key].pop();

                        break;

                    }


                }
                break;

            }

        }

        this.save(carrito);

    }


    /**
     * Itera por los campos del carrito y devuelve el número total del producto
     * @return {number}      El número de productos que tiene el carrito
     */
    getNumeroArticulos(){

        if(this.isNull()){

            return;

        }

        const carrito = this.get();

        let counter = 0;

        const keys = [];

        for(const key in carrito){

            keys.push(key);

        }


        for(let i = 0; i<2; ++i){

            for(const j of carrito[keys[i]]){

                ++counter;

            }

        }

    }



    storage; // puntero a local storage
    storageKey; // string que guarda el nombre del slot utilizado para el carrito en el localStorage
    entregas = [];
    productos = [];
    numeroArticulos;
    dataFormat;
}
