﻿import { EnvolturaRegalo } from "../models/envolturaRegalo.js";
import { TemplateHandler } from "../assets/templateHandlebars.js";
import { ModalProduct } from "./modalProduct.js";
import { Producto } from "../models/producto.js";

export class UltimosArticulos {



    initializeModals(data) {

        const tH = new TemplateHandler();

        tH.desplegarTemplate(document.querySelector("#container-modals"), document.querySelector("#modal-product-template").innerHTML, { articulos: data });

        const cardButtons = document.querySelectorAll(".card-button");

        const modals = document.querySelectorAll(".modal");

        for (const card of cardButtons) {

            let modalProduct = null; //a obtener y agregar a la página
          
            for (const modal of modals) {

                const id = modal.id.slice(("mp-").length);

                if (card.classList.contains(`card-${id}`)) {

                    const closeButton = modal.querySelector(".close");

                    let precio = 0;

                    for (const i of data) {

                        if (i.id === parseInt(id)) {

                            precio = i.precio;
                            
                            break;

                        }

                    }

                    modalProduct = new ModalProduct(precio, modal, card, closeButton);

                    break;

                }

            }

            

            this.modalProducts.push(modalProduct);
        }


    }

    constructor(containers, template, eventToDispatch) {

        let response = null;
        const objectRef = this;
        $.ajax({
            type: "POST",
            url: "index.aspx/getNuevosProductos",
            dataType: "json",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            success: function (result) {
                response = result;
            },
            error: function (err) {
                console.log("Error: ", err);
                response = "Error";
            },
            complete: function (result) {

                const regalosRaw = response.d;

                console.log(regalosRaw);

                const regalos = [];

                for (const i of regalosRaw) {

                    const regalo = new Producto();

                    regalo.id = i.id_Producto;
                    regalo.nombre = i.nombre_Producto;
                    regalo.precio = i.precio_Producto;
                    

                    if (i.foto1_Regalo == null) {

                        regalo.foto1 = "img/Fotos/Regalandia_1575.jpg"

                    }
                    else {
                        regalo.foto1 = i.foto1_Producto;
                        regalo.foto2 = i.foto2_Producto;
                        regalo.foto3 = i.foto3_Producto;
                        regalo.foto4 = i.foto4_Producto;
                    }

                    regalos.push(regalo);

                }

                let tH = new TemplateHandler();
            
                for (let i = 0; i < containers.length; ++i){


                    switch (i) {

                        case 0: {

                            tH.desplegarTemplate(containers[i], template, { regalos: regalos });

                            break;
                        }


                        case 1: {

                            tH.desplegarTemplate(containers[i], template, { regalos: regalos.slice(0,3)});

                            break;
                        }

                        case 2: {

                            tH.desplegarTemplate(containers[i], template, { regalos: regalos.slice(3, 6)});

                            break;
                        }

                    }

                }

                objectRef.initializeModals(regalos);

                if (eventToDispatch != null) {

                    document.dispatchEvent(eventToDispatch);
                }

            }


         

        });

    }

    regalos = [];
    modalProducts = [];
}