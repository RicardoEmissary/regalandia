﻿class RadioData {

    button;
    inputElement;

}

export class CustomRadio {

    constructor(element) {

        const optionContainers = element.querySelectorAll('.radio-genero-modal__opcion');

        for (const option of optionContainers) {

            const radio = new RadioData();

            radio.button = option.querySelector('div.radio-square-modal');
            radio.inputElement = option.querySelector('input[type="radio"]');




            this.options.push(radio);

        }

        for (const radio of this.options){

            radio.button.addEventListener('click', () => {

                radio.inputElement.checked = true;


                for (const i of this.options) {

                    i.button.classList.remove('selected');

                }

                radio.button.classList.add('selected');

            });

        }



    }

    element;
    options = [];
}
