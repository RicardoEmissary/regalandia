﻿export class LoadingSpinner{


    constructor(container, cssClass){

        this.container = container;
        this.cssClass = cssClass;

        this.createSpinner();


    }

    createSpinner(){

        this.spinner = document.createElement("div");
        this.spinner.classList.add(this.cssClass);
        console.log(this.spinner);
        this.container.appendChild(this.spinner);

    }

    show(){

        this.spinner.style.display = "block";

    }

    hide(){

        this.spinner.style.display = "none";

    }

    container;
    cssClass; //clase css a agregar al contenedor
    spinner;  //Element del spinner
}
