export class InputNumberHandler {

    constructor(inputElement, buttonUp, buttonDown, minAmount, maxAmount, initialAmount) {

        this.upListener = true;

        this.amount = initialAmount;

        this.minAmount = minAmount;

        this.maxAmount = maxAmount;

        this.inputElement = inputElement;

        this.inputElement.value = this.amount;

        this.buttonUp = buttonUp;

        this.buttonDown = buttonDown;

        this.buttonUp.addEventListener('click', () => {

            if (this.upListener) {

                if (this.amount < this.maxAmount) {

                    this.inputElement.value = ++this.amount;

                }
            }
        })

        this.buttonDown.addEventListener('click', () => {

            if (this.amount > this.minAmount) {
                this.inputElement.value = --this.amount;

            }
        })
    }

    upListener;
    inputElement;
    buttonUp;
    buttonDown;
    amount;
    maxAmount;
    minAmount;
}
