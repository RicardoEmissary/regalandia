export function IconCarritoMain(){

    let iconCarritoButton =  document.querySelector('.banner-cart');

    let popupWindow = document.querySelector('div.banner-right__popup');

    let popupShown = false

    iconCarritoButton.addEventListener('click', ()=>{

    if(popupShown){

        popupWindow.style.display = 'none'
        popupShown = false
    }

    else{

        popupWindow.style.display = 'flex'
        popupWindow.style.flexDirection = 'column'
        popupShown = true
    }

    });



}


