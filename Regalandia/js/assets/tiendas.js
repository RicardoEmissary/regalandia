import {TemplateHandler} from "./templateHandlebars.js";
import { Pagination } from "./pagination.js";
import { ModalProduct } from "./modalProduct.js";

class GridCard {

    constructor(cards) {

        this.cards = cards;

    }

    display(displayMode) {

        for (const i of this.cards) {

            i.style.display = displayMode;

        }

    }

    cards = []; //<=6 cards

}

class GridAdapter {

    order(subcategoria = 0, tamano = 0) {

        const cardsToShow = [];

        console.log(this.cards);

        for (const i of this.cards) {

            let subcategoriaFlag = false;
            let tamanoFlag = false;

            if (subcategoria != 0) {

                if (i.classList.contains(`sub-${subcategoria}`)) {

                    subcategoriaFlag = true;
                    console.log('hola')

                }


            }
            else {

                subcategoriaFlag = true;

            }

            if (tamano != 0) {
                console.log(tamano);
                if (i.classList.contains(`tam-${tamano}`)) {

                    tamanoFlag = true;

                }

            }
            else {

                tamanoFlag = true;

            }



            if (subcategoriaFlag && tamanoFlag) {

                 cardsToShow.push(i);

            }

        }

        //Aumenta de 6 en 6 para cumplir que el grid sea 3x2
        for (let i = 0; i < cardsToShow.length; i += 6) {

            const auxGridCards = [];

            //Si ya no quedan otros 6...
            if (i + 6 >= cardsToShow.length) {

                let aux = i;

                //Que agregue al ultimo arreglo el n�mero que queda menor a 6
                while (aux < cardsToShow.length) {

                    auxGridCards.push(cardsToShow[aux]);

                    ++aux;

                }

                this.gridCards.push(new GridCard(auxGridCards));

                break; // y salga del bucle

            }

            for (let j = i; j < i + 6; ++j) {

                auxGridCards.push(cardsToShow[j]);

            }

            this.gridCards.push(new GridCard(auxGridCards));
            

        }

    }


    show(subcategoria = 0, tamano = 0) {

        for (const i of this.gridCards) {

            i.display("none");

        }

        this.gridCards = [];

        this.order(subcategoria, tamano);


    }

    constructor(cards) {

        this.cards = cards;
        this.show();

    }

    gridCards = []; //GridCard[]
    cards = []; //NodeListOf<Element>

}

export class Tiendas{


    /**
     * Constructor of tiendas class
     * @param  {number} categoria Categoria de las envolturas que va a mostrar.
     * @param  {Element} mainContainer Contenedor HTML principal. El <section.cajas> de la plantilla HTML.
     *                   Con este se busca a los contenedores de los datos de subcategorías y envolturas.
     * @param  {string} templateSubcategorias Template handlebars para el llenado de las subcategorías.
     * @param  {Array<Subcategoria>} dataSubcategorias Arreglo para el despliegue de subcategorías.
     * @param  {string} templateArticulos Template handlebars para el llenado de las envolturas a mostrar.
     * @param  {Array<EnvolturaRegalo>} dataArticulos Arreglo para el despliegue de envolturas.
     * @param  {string} templateModal Template handlebars para llenar la ventana modal de cada envoltura.
     * @param  {Element} containerModal Contenedor HTML para las ventanas modales.
     * @param  {number} tamanoDefault Id del tamano default a mostrar en el filtro. Si no se añade o es null, se muestra la
 *                                    opción de sidebar de tamaños.
     */

    constructor(categoria,
                mainContainer,
                templateSubcategorias,
                dataSubcategorias,
                templateArticulos,
                dataArticulos,
                templateModal,
                containerModal,
                tamanoDefault = -1) {

      

        this.tamanoDefault = tamanoDefault;

        this.containerModal = containerModal;

        this.mainContainer = mainContainer;

        dataSubcategorias = {subcategorias: dataSubcategorias};
        dataArticulos = { articulos: dataArticulos }
        this.categoria = categoria;

        const containerSubcategorias = mainContainer.querySelector(".sidebar");
        const containerArticulos = mainContainer.querySelector(".boxes-grid");

        this.initializeSidebar(templateSubcategorias, containerSubcategorias, dataSubcategorias);

        this.initializeArticulos(containerArticulos, templateArticulos, dataArticulos);

        this.initializeModalProducts(containerModal, templateModal, dataArticulos);

        this.orderAllGridCards();

        this.pagination = new Pagination(

            containerArticulos,
            this.gridAdapter.gridCards,
            mainContainer.querySelector(".buttons-container"),
            mainContainer.querySelector(".pag-left-button"),
            mainContainer.querySelector(".pag-right-button")

        );

    }

    initializeSidebar(template, container, data){

        const tH = new TemplateHandler();

        tH.desplegarTemplate(container, template, data);

        this.radiosSubcategorias =
            container.querySelectorAll("input[name='sidebar-radio-sub']");

        this.radiosTamano =
            container.querySelectorAll("input[name='sidebar-radio-tamano']");

        if(this.tamanoDefault < 0){

                this.mainContainer.querySelector(".sidebar-tamanos").style.display = "none";

        }


    }

    initializeArticulos(container, template, data){

        const tH = new TemplateHandler();

        const finalData = [];

        for(const i of data.articulos){

                finalData.push(i);

        }

        this.articulos = { envolturas: finalData };

        tH.desplegarTemplate(container, template, this.articulos);

    }

    initializeModalProducts(container, template, data){

        const tH = new TemplateHandler();

       

        tH.desplegarTemplate(container, template, this.articulos);

        const modals = this.containerModal.querySelectorAll(".modal");

        const openButtons = this.mainContainer.querySelectorAll(".box-product");

        

        for(const modal of modals){

            const id = modal.id.slice(("mp-").length);

            //obtener el openButton
            let openButton = null;

            for(const button of openButtons){

                const idButton = button.id.slice(("bp-").length);

                if(idButton === id){

                    openButton = button.querySelector(".box-product__image");
                    break;

                }

            }
            //obtener el precio
            let precio = 0;

            for(const i of this.articulos.envolturas){

                if(i.id === id){

                    precio = i.precio;

                    break;

                }

            }

            const closeButton = modal.querySelector(".close");

            const modalProduct = new ModalProduct(

                precio,
                modal,
                openButton,
                closeButton

            );

            this.modalProducts.push(modalProduct);

        }



    }

    orderAllGridCards() {

        const cards = this.mainContainer.querySelectorAll(".box-product");

        this.gridAdapter = new GridAdapter(cards);


        for (const i of this.radiosSubcategorias) {

            i.addEventListener("change", () => {

                let subcategoriaValue = 0;
                let tamanoValue = 0;

                for (const j of this.radiosSubcategorias) {

                    if (j.checked) {

                        subcategoriaValue = j.value;

                    }

                }

                for (const j of this.radiosTamano) {

                    if (j.checked) {

                        tamanoValue = j.value;

                    }

                }

                this.gridAdapter.show(subcategoriaValue, tamanoValue);
                this.pagination.reload(this.gridAdapter.gridCards);

            });

        }

        for (const i of this.radiosTamano) {

            i.addEventListener("change", () => {

                let subcategoriaValue = 0;
                let tamanoValue = 0;

                for (const j of this.radiosSubcategorias) {

                    if (j.checked) {

                        subcategoriaValue = j.value;

                    }

                }

                for (const j of this.radiosTamano) {

                    if (j.checked) {

                        tamanoValue = j.value;

                    }

                }

                this.gridAdapter.show(subcategoriaValue, tamanoValue);
                this.pagination.reload(this.gridAdapter.gridCards);

            });

        }


    }


    articulos = [];

    categoria;

    radiosSubcategorias;

    radiosTamano;

    gridAdapter;

    pagination;

    modalProducts = [];

    tamanoDefault;

    mainContainer;

    containerModal;
}
