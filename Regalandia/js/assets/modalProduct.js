import {ToggleVisibility} from "./toggleVisibility.js";
import {InputNumberHandler} from "./CustomInputNumber.js";

export class ModalProduct extends ToggleVisibility{

    precioListener() {

        this.inputPrecio.buttonUp.addEventListener("click", () => {

            const precioToShow = this.inputPrecio.inputElement.value * this.precio;

            this.precioDisplay.textContent = precioToShow.toString();

        });

        this.inputPrecio.buttonDown.addEventListener("click", () => {

            const precioToShow = this.inputPrecio.inputElement.value * this.precio;

            this.precioDisplay.textContent = precioToShow.toString();

        });



    }

    constructor(precio, htmlElement, openButton, closeButton, displayMode = "block"){
        super(htmlElement, openButton, closeButton, displayMode);

        const inputContainer = this.htmlElement.querySelector(".fixedInputNumber");
        const buttons = inputContainer.querySelectorAll("button");


        this.precio = precio;

        this.precioDisplay = this.htmlElement.querySelector(".costo-product");

        this.precioDisplay.textContent = precio.toString();

        this.inputPrecio = new InputNumberHandler(

            inputContainer.querySelector("input[type='number']"),
            buttons[0],
            buttons[1],
            1,
            10,
            1

        );

       this.precioListener();

    }

    
    inputPrecio; //InputNumberHandler
    precioDisplay;
    precio;

}
