export class Pagination {

    /**
     * Constructor of pagination class
     * @param  {Element} displayContainer The container where cards will be displayed
     * @param  {GridCard []} cards Cards that will be displayed
     * @param  {Element} buttonsContainer The container where buttons will be added
     * @param  {Element} leftButton The container where buttons will be added
     * @param  {Element} rightButton The container where buttons will be added
     */
    constructor(displayContainer, cards, buttonsContainer, leftButton, rightButton) {

        this.buttonsContainer = buttonsContainer;
        
        this.cards = cards;

        this.leftButton = leftButton;
        this.rightButton = rightButton;

        this.displayContainer = displayContainer;

        for(let i = 1; i<this.cards.length; ++i){

            this.cards[i].display("none");

        }
        this.shown = 0;

        this.InitializeButtons();

        this.displayFirst();

        this.initializeLeftRightButtons();

        this.shown = 0;

        this.focusButton();

        this.ShowHideButtons();

    }

    /**
     * Method to add buttons to container and add them their functionality.
     * @return {void}      Nothing
     */
    InitializeButtons(){

        //Por cada card hay un botón
        for(let i = 0; i<this.cards.length; ++i){

            //Se crea el elemento
            const button = document.createElement("button");

            button.id = `btn-${i}`;
            button.textContent = (i+1).toString();

            this.buttonsContainer.appendChild(button);

            this.buttons.push(button);


            //Se añade la funcionalidad de mostrar el card al que está relacionado
            button.addEventListener("click",()=>{

                for(const j of this.cards){

                    j.display("none");

                }


                this.cards[i].display("block");
                console.log(this.cards[i]);
                this.shown = i;

                for (const j of this.buttons) {

                    j.classList.remove("selected");

                }

                button.classList.add("selected");



                this.ShowHideButtons();


            });

        }

    }

    /**
     * Adds buttons the behaviour to show/hide depending on the shown index.
     * Must be called only inside InitializeButtons function
     * @return {void} Nothing
     */
    ShowHideButtons(){

        for(let i = 0; i<this.buttons.length; ++i){

            if(i === 0 || i === this.buttons.length-1 || i === this.shown){

                this.buttons[i].style.display = "block";

            }
            else{
                this.buttons[i].style.display = "none";

            }

        }


        if (this.shown === 0){

            if(this.buttons.length >= 4){

                for(let i = 1; i < 4; ++i){

                    this.buttons[i].style.display = "block";

                }

            }

        }
        else{

            if(this.shown +3 > this.buttons.length){

                if(this.shown === this.buttons.length-1){

                    if(this.shown-3>=0){

                        this.buttons[this.shown-3].style.display = "block";

                    }

                    if(this.shown-2>=0){

                        this.buttons[this.shown-2].style.display = "block";

                    }

                    if(this.shown-1>=0){

                        this.buttons[this.shown-1].style.display = "block";

                    }

                }
                else{

                    if(this.shown-2>=0){

                        this.buttons[this.shown-2].style.display = "block";

                    }

                    if(this.shown-1>=0){

                        this.buttons[this.shown-1].style.display = "block";

                    }

                }




            }
            else{

                if(this.shown === 1){

                    for(let i = this.shown-1; i<(this.shown+3); ++i){

                        this.buttons[i].style.display = "block";

                    }

                }

                else{


                        for(let i = this.shown-1; i<(this.shown+2); ++i){

                            this.buttons[i].style.display = "block";

                        }

                }

            }


        }

    }

    /**
     * Removes the old number buttons and load new ones to show those which correspond to the shown cards
     * Ideally the method the user should use.
     *
     * @param  {Element[]/NodeListOf<Element>} newCards Cards to be shown
     *
     * @return {void} Nothing
     */

    reload(newCards) {

        this.cards = newCards;

        this.reloadButtons();

        this.displayFirst();

        this.ShowHideButtons();

        this.focusButton();

    }

    /**
     * Removes the old number buttons and load new ones to show those which correspond to the shown cards
     * Not for user's usage
     *
     * @return {void} Nothing
     */
    reloadButtons() {

        for (let i of this.buttons) {

            i.remove();

            i = null;

        }

        this.buttons = [];

        this.shown = 0;

        this.focusButton();

        this.InitializeButtons();

    }


    /**
     * Displays the first gridCard
     *
     * @return {void} Nothing
     */
    displayFirst() {
        console.log(this.cards)
        this.cards[0].display("flex");

    }

    focusButton(){

        for(let j = 0; j<this.buttons.length; ++j){

            if(j!==this.shown){

                this.buttons[j].classList.remove("selected");

            }
            else{

                this.buttons[j].classList.add("selected");

            }


        }

        this.ShowHideButtons();

    }

    initializeLeftRightButtons(){

        this.leftButton.addEventListener("click", ()=>{

            if(this.shown>0){
                let subtractedFlag = false;

                for(let i = 0; i<this.cards.length; ++i){

                    if(i === this.shown-1){

                        subtractedFlag = true;
                        this.cards[i].display("block");

                    }
                    else{

                        this.cards[i].display("none");


                    }

                }

                if(subtractedFlag){

                    --this.shown;

                    this.focusButton();

                }

            }

        });

        this.rightButton.addEventListener("click", ()=>{

            if(this.shown<this.cards.length-1){
                let addedFlag = false;

                for(let i = 0; i<this.cards.length; ++i){

                    if(i === this.shown+1){

                        addedFlag = true;
                        this.cards[i].display("block");

                    }
                    else{

                        this.cards[i].display("none");


                    }

                }

                if(addedFlag){

                    ++this.shown;

                    this.focusButton();

                }

            }

        });


    }

    displayContainer;
    buttons = [];
    cards;
    buttonsContainer;
    shown; //Stores the shown card's id
    leftButton;
    rightButton;
}
