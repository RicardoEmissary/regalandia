//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Regalandia
{
    using System;
    using System.Collections.Generic;
    
    public partial class Frase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Frase()
        {
            this.ProductoSeleccionado = new HashSet<ProductoSeleccionado>();
        }
    
        public int id_Frase { get; set; }
        public string descripcion_Frase { get; set; }
        public Nullable<bool> habilitado { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductoSeleccionado> ProductoSeleccionado { get; set; }
    }
}
