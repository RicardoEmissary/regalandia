﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bolsas.aspx.cs" Inherits="Regalandia.bolsas" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bolsas</title>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="icon" type="image/png" href="img/favico.png">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@200;300;500;800&display=swap" rel="stylesheet">

    <script src="js/handlebars.min-v4.7.6.js"></script>

    <!--template para el sidebar-->
    <script id="articulos-envolturas" type="text/x-handlebars-template">



                        <div class="sidebar__title">
                            <div class="image">
                                <img src="img/icon/paper-white.svg" alt="bolsas especiales">
                            </div>
                            bolsas especiales
                        </div>
                        <div class="sidebar__filter">
                            <div class="sidebar-filter sidebar-filter__categoria">
                                <p class="text-purple">Elige por categoría</p>
                                <form>
                                    <label class="radio">ver todo
                                        <input type="radio" value="0" name="sidebar-radio-sub" checked="checked">
                                        <span class="checkmark"></span>
                                    </label>

                                    {{#each subcategorias}}
                                        <label class="radio">{{nombre}}
                                            <input type="radio" value="{{id}}" name="sidebar-radio-sub">
                                            <span class="checkmark"></span>
                                        </label>
                                    {{/each}}
                                </form>
                            </div>

                        </div>
                        <div class="sidebar__filter">
                            <div class="sidebar-filter sidebar-filter__categoria sidebar-tamanos">
                                <p class="text-purple">Elige por tamaño</p>
                                <form>
                                    <label class="radio">ver todo
                                        <input type="radio" value="0" name="sidebar-radio-tamano" checked="checked">
                                        <span class="checkmark"></span>
                                    </label>

                                    <label class="radio">Especial
                                        <input type="radio" value="1" name="sidebar-radio-tamano">
                                        <span class="checkmark"></span>
                                    </label>

                                    <label class="radio">Divertido
                                        <input type="radio" value="2" name="sidebar-radio-tamano">
                                        <span class="checkmark"></span>
                                    </label>

                                    <label class="radio">En grande
                                        <input type="radio" value="3" name="sidebar-radio-tamano">
                                        <span class="checkmark"></span>
                                    </label>

                                </form>
                            </div>

                        </div>






    </script>

    <!--template para el card de eleccion de envoltura-->
    <script id="modalEnvolturasTemplate" type="text/x-handlebars-template">
        {{#each envolturas}}
            <div class="box-product sub-{{categoria}} tam-{{tamano}}" id="bp-{{id}}">
                <div class="box-product__image">
                    <div class="image frame-container">
                        <div class="frame frame--blue"></div>

                        <img src="{{foto1}}" alt="{{nombre}}">
                        <img src="{{foto2}}" alt="{{nombre}}">
                    </div>
                </div>
                <div class="box-product__description text-purple">
                    <p>{{nombre}}</p>$ {{precio}}
                    <button class="btn-sm-blue add-to-cart">agregar</button>
                    <a href="#" class="cart"><span></span></a>
                </div>
            </div>
        {{/each}}
    </script>

    <!--template para el modal-product -->
    <script id="modalProductTemplate" type="text/x-handlebars-template">

        {{#each envolturas}}
            <div id="mp-{{id}}" class="modal modal-product">

                <div class="modal-content">
                    <span class="close">&times;</span>
                    <section>
                        <div class="mainTitle mainTitle--balloons">
                            <h3>{{nombre}}</h3>
                        </div>
                        <article class="productCard">
                            <div class="productCard__images">
                                <figure class="productCard__images__mainFigure">
                                    <img src="{{foto1}}" alt="Globos">
                                </figure>
                                <figure class="productCard__images__rowFigure">
                                    <img src="{{foto2}}" alt="Globos">
                                    <img src="{{foto3}}" alt="Globos">
                                    <img src="{{foto4}}" alt="Globos">
                                </figure>
                            </div>

                            <div class="productCard__info">
                                <p class="productCard__info__text">{{nombre}}</p>
                                <p class="productCard__info__productDesc">
                                    {{descripcion}}
                                </p>
                                <p class="productCard__info__text product__costo">Costo <span class="costo-product"></span></p>
                                <div class="colorCardGrid">
                                    <div class="colorCard ">
                                        <div class="fixedInputNumber fixedInputNumber--md balloonsAmount">
                                            <p>Cantidad</p>
                                            <input type="number" readonly>
                                            <div class="colorCard__buttons">
                                                <button></button>
                                                <button></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="productCard__info__productColor">
                                    <div class="productColor__rowColor">
                                        <p>Selecciona color</p>
                                        <div class="colorCardGrid">
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--violet"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color1">
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--blue"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color2" >
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--orange"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color3" >
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="colorCard">
                                                <div class="colorCard__circle colorCard__circle--red"></div>
                                                <div class="fixedInputNumber fixedInputNumber--md color4" >
                                                    <input type="number" readonly>
                                                    <div class="colorCard__buttons">
                                                        <button></button>
                                                        <button></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="productCard__info__productFrase">
                                    <p>Selecciona frase</p>
                                    <div class="productFrase__selectContainer">
                                        <select name="frases" id="frases">
                                            <option value="0">Buen día</option>
                                            <option value="1">Feliz cumpleaños</option>
                                            <option value="2">Felices fiestas</option>
                                        </select>
                                    </div>
                                </div>
                                <button id="btn-mp-{{id}}" class="btn-md-purple fit-content ">Agregar</button>
                            </div>
                        </article>
                    </section>
                </div>
            </div>
        {{/each}}

    </script>

</head>




<body>
    
    <!-- Container de Modal Ver Producto -->
    <div id="modal-product-container">

    </div>

    <section class="banner" id="header">
        <div class="banner-left">
            <div class="media">
                <p>¡Únete!</p>
                <div class="icon">
                    <a href="index.html"><img src="img/icon/icon-instagram.svg" alt="Regalandia Instagram"></a>
                </div>
                <div class="icon">
                    <a href=""><img src="img/icon/icon-facebook.svg" alt="Regalandia Facebook"></a>
                </div>
            </div>
        </div>
        <div class="logo">
            <a href="index.html"><img src="img/Rmono/6.svg" alt="Regalandia"></a>
        </div>
        <div class="banner-right">
            <a href="carrito.html" class="banner-cart">
                <div class="icon cart">
                    <img src="img/icon/shopping.svg" alt="carrito">
                    <span class="cart-items">0</span>
                </div>
                <p><span>$0.00</span></p>
            </a>
        </div>
    </section>

    <div class="topnav " id="myTopnav">
        <a href="index.html"></a>
        <a href="tienda.html">Tienda</a>
        <a href="entregas.html">Entregas</a>
        <a href="tracking.html">Rastreo</a>
        <a href="contacto.html">Contacto</a>
        <a href="nosotros.html">Nosotros</a>
        <a id="responsiveNavbar" href="javascript:void(0);" class="icon">&#9776;</a>
    </div>

    <div class="title title--blue">Bolsas</div>
    <p class="text-primary">Selecciona las bolsas que más te gusten</p>
    <main class="cajas" id="main-tienda-container">
        <div class="sidebar" id="sidebar"></div>

        <div class="right-cajas">
        <div class="boxes-grid" id="boxes-grid"></div>
            <div  class="pagination-buttons-container">

            <button class="pag-left-button"><</button>
            <div class=" buttons-container number-buttons-container"></div>
            <button class="pag-right-button">></button>
        </div>

        </div>
    </main>


    <footer>
        <div class="left">
            <div class="join">
                <p>¡Únete!</p>
                <div class="icon">
                    <a href=""><img src="img/icon/icon-instagram.svg" alt="Regalandia Instagram"></a>
                </div>
                <div class="icon">
                    <a href=""><img src="img/icon/icon-facebook.svg" alt="Regalandia Facebook"></a>
                </div>
            </div>
            <p>Monterrey, N.L. | correo@electronico.com</p>
            <div class="links">
                <a href="">Nosotros</a>
                <a href="">Entregas</a>
                <a href="">Globos</a>
                <a href="">Contacto</a>
                <a href="">Tienda</a>
            </div>
        </div>
        <div class="right">
            <div class="logo">
                <img src="img/Logotipo/morado.svg" alt="">
            </div>
        </div>
    </footer>
    <script src="vendor/jquery/jquery.js"></script>
    <script src="js/assets/navbar.js"></script>
    <script src="js/add-to-cart.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery/jquery.min.js "></script>

    <script>
        document.body.addEventListener('scroll', () => {
            scrollFunction()
        });
        window.onscroll = function() {
            scrollFunction()
        };
        var nav = $("#myTopnav");

        function scrollFunction() {
            var bannerH = document.getElementById('header').getBoundingClientRect().height;
            if (document.body.scrollTop > bannerH || document.documentElement.scrollTop > bannerH) {
                nav.addClass("fixed-topnav");
            } else {
                nav.removeClass("fixed-topnav");
            }
        }
    </script>

    <script type="module" src="js/pages/bolsasTiendas.js"></script>
</body>

</html>
