﻿using Regalandia.app.controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Regalandia
{
    public partial class bolsas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //Obtenemos todas las BOLSAS habilitadas
        [WebMethod(EnableSession = true)]
        public static Object getBolsas()
        {
            using (productoController controller = new productoController())
            {
                try
                {
                    return controller.getAllProductosByTipo(2);
                }
                catch(Exception E)
                {
                    return E;
                }
            }
        }

        //Obtenemos los colores registrados para las bolsas
        [WebMethod(EnableSession = true)]
        public static Object getColoresProductos()
        {
            using (productoColorController controller = new productoColorController())
            {
                try
                {
                    return controller.getColoresByTipo(2);
                }
                catch (Exception E)
                {
                    return E;
                }
            }
        }

        //Obtenemos todas las frases registradas
        [WebMethod(EnableSession = true)]
        public static Object getFrases()
        {
            using(frasesController controller = new frasesController())
            {
                try
                {
                    return controller.GetFrasesEnabled();
                }
                catch(Exception E)
                {
                    return E;
                }
            }
        }
    }
}